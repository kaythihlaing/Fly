//
//  CalendarViewController.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/10/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import FSCalendar
import SkyFloatingLabelTextField

enum TripType {
    case RoundTrip
    case Departure
    case Return
}

class CalendarViewController: UIViewController {
    
    //MARK: - View Components
    lazy private var navBarView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().HexToColor(hexString: AppConstants.Color.secondaryBlue)
        return view
    }()
    
    lazy private var btnClose: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Cancel"), for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnClose), for: .touchUpInside)
        return btn
    }()
    
    lazy private var lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.text = "Departure Date"
        lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lbl.font = UIFont(name: AppConstants.Font.AvenirNext.DemiBold, size: 20)
        return lbl
    }()
    
    lazy private var dateBgStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fillEqually
        view.alignment = .center
        view.spacing = 16
        view.addBackground(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        return view
    }()
    
    lazy private var departureDateBgView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy private var ivDepartureDate: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Date")
        iv.contentMode = UIView.ContentMode.scaleAspectFit
        return iv
    }()
    
    lazy private var tfDepartureDate: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "Departure Date"
        tf.lineColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        tf.selectedLineColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        tf.tintColor = UIColor.clear
        tf.isUserInteractionEnabled = false
        tf.text = ""
        tf.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 17)
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Regular, size: 17)
        tf.titleFormatter = { $0 }
        return tf
    }()
    
    lazy private var returnDateBgView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy private var ivReturnDate: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Date")
        iv.contentMode = UIView.ContentMode.scaleAspectFit
        return iv
    }()
    
    lazy private var tfReturnDate: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "Return Date"
        tf.lineColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        tf.selectedLineColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        tf.tintColor = UIColor.clear
        tf.isUserInteractionEnabled = false
        tf.text = ""
        tf.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 17)
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Regular, size: 17)
        tf.titleFormatter = { $0 }
        return tf
    }()
    
    lazy private var btnPreviousMonth: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Left Arrow Triangle"), for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnPreviousMonth), for: .touchUpInside)
        return btn
    }()

    lazy private var btnNextMonth: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Right Arrow Triangle"), for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnNextMonth), for: .touchUpInside)
        return btn
    }()
    
    lazy private var calendar: FSCalendar = {
        let fs = FSCalendar()
        fs.allowsMultipleSelection = true
        fs.delegate = self
        fs.dataSource = self
        fs.backgroundColor = UIColor.groupTableViewBackground
        fs.today = nil
        fs.appearance.headerMinimumDissolvedAlpha = 0
        fs.appearance.headerTitleColor = UIColor.black
        fs.appearance.headerTitleFont = UIFont(name: AppConstants.Font.Roboto.Bold, size: 20)
        fs.appearance.weekdayTextColor = UIColor.black
        fs.placeholderType = .none
        fs.appearance.titleFont = UIFont(name: AppConstants.Font.Roboto.Regular, size: 17)
        fs.register(CalendarCustomCell.self, forCellReuseIdentifier: self.cellId)
        return fs
    }()
    
    lazy private var btnSelectDate: UIButton = {
        let btn = UIButton()
        btn.setTitle("Select Date", for: .normal)
        btn.backgroundColor = UIColor.primaryBlue
        btn.layer.cornerRadius = 5
        btn.layer.masksToBounds = true
        btn.titleLabel?.font =  UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        btn.addTarget(self, action: #selector(didTapBtnSelectDate), for: .touchUpInside)
        return btn
    }()
    
    //MARK: - Class Attributes
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    
//    fileprivate let formatter: DateFormatter = {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "EE, dd MMM yyyy"
//        return formatter
//    }()
    
    private var firstDate: Date?
    
    private var lastDate: Date?
    
    private var datesRange: [Date] = [] {
        didSet {
            //TODO: Need to remove REDUANDENT code
            if datesRange.isEmpty {
                self.lblTitle.text = "Departure Date"
                self.tfDepartureDate.text = ""
                self.tfDepartureDate.snp.updateConstraints { (make) in
                    make.centerY.equalTo(ivDepartureDate.snp.centerY).offset(-8)
                }
                
                self.tfReturnDate.text = ""
                self.tfReturnDate.snp.updateConstraints { (make) in
                    make.centerY.equalTo(ivReturnDate.snp.centerY).offset(-8)
                }
            } else {
                if tripType != .Departure {
                    self.lblTitle.text = "Return Date"
                }
                
                switch tripType {
                case .RoundTrip :
                    self.tfDepartureDate.text = datesRange.first?.showDate
                    self.tfDepartureDate.snp.updateConstraints { (make) in
                        make.centerY.equalTo(ivDepartureDate.snp.centerY).offset(0)
                    }
                    self.firstDate = datesRange.first!
                    
                    if datesRange.count > 1 {
                        self.tfReturnDate.text = datesRange.last?.showDate
                        self.tfReturnDate.snp.updateConstraints { (make) in
                            make.centerY.equalTo(ivReturnDate.snp.centerY).offset(0)
                        }
                        self.lastDate = datesRange.last!
                    }
                    break
                    
                case .Departure:
                    self.tfDepartureDate.text = datesRange.last?.showDate
                    self.tfDepartureDate.snp.updateConstraints { (make) in
                        make.centerY.equalTo(ivDepartureDate.snp.centerY).offset(0)
                    }
                    self.firstDate = datesRange.last!
//                    self.lastDate = nil
                    break
                    
                case .Return:
                    self.tfDepartureDate.text = datesRange.first?.showDate
                    self.tfDepartureDate.snp.updateConstraints { (make) in
                        make.centerY.equalTo(ivDepartureDate.snp.centerY).offset(0)
                    }
                    self.firstDate = datesRange.first!
                    
                    if datesRange.count > 1 {
                        self.tfReturnDate.text = datesRange.last?.showDate
                        self.tfReturnDate.snp.updateConstraints { (make) in
                            make.centerY.equalTo(ivReturnDate.snp.centerY).offset(0)
                        }
                        self.lastDate = datesRange.last!
                    }
                    break
                }
                
            }
        }
    }
    
    private var tripType : TripType = .RoundTrip
    
    private var calendarProtocol: calProtocol?
    
    private let cellId = "CalendarCustomCell"
    
    //MARK: - Life Cycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNeedsStatusBarAppearanceUpdate()
        
        self.setupUI()
        
        if firstDate != nil && lastDate != nil {
            switch tripType {
            case .RoundTrip:
                var i = firstDate!
                var range: [Date] = []
                
                while i <= lastDate! {
                    range.append(i)
                    i = Calendar.current.date(byAdding: .day, value: 1, to: i)!
                }
                datesRange = range
                
            case .Departure:
                datesRange.append(firstDate!)
                
            case .Return:
                var i = firstDate!
                var range: [Date] = []
                
                while i <= lastDate! {
                    range.append(i)
                    i = Calendar.current.date(byAdding: .day, value: 1, to: i)!
                }
                datesRange = range
            }
            
            for d in datesRange {
                calendar.select(d)
            }
            self.configureVisibleCells()
        }
    }
    
    //MARK: - Public Functions
    
    /// Set data to calendar view
    ///
    /// - Parameters:
    ///   - type: Trip Type
    ///   - fromDate: Departure Date
    ///   - toDate: Return Date
    func setTripType(type: TripType, fromDate: Date?, toDate: Date?) {
        self.tripType = type
        
        self.firstDate = fromDate
        self.lastDate = toDate
    }
    
    func setProtocol(calendar: calProtocol) {
        self.calendarProtocol = calendar
    }
    
    //MARK: - Private Functions
    
    
    /// Set up UI to view
    private func setupUI() {
        if tripType == .Return {
            lblTitle.text = "Return Date"
        }
        
        //Navigation Bar
        view.addSubview(navBarView)
        navBarView.addSubview(lblTitle)
        navBarView.addSubview(btnClose)
        
        //Date View
        view.addSubview(dateBgStackView)
        //Departure Date
        departureDateBgView.addSubview(ivDepartureDate)
        departureDateBgView.addSubview(tfDepartureDate)
        //Return Date
        returnDateBgView.addSubview(ivReturnDate)
        returnDateBgView.addSubview(tfReturnDate)
        if tripType == .RoundTrip {
            dateBgStackView.addArrangedSubview(departureDateBgView)
            dateBgStackView.addArrangedSubview(returnDateBgView)
            calendar.allowsMultipleSelection = true
        } else if tripType == .Departure {
            dateBgStackView.addArrangedSubview(departureDateBgView)
            calendar.allowsMultipleSelection = false
        } else if tripType == .Return {
            dateBgStackView.addArrangedSubview(departureDateBgView)
            dateBgStackView.addArrangedSubview(returnDateBgView)
            calendar.allowsMultipleSelection = true
        }
        
        //Calendar View
        view.addSubview(calendar)
        view.addSubview(btnPreviousMonth)
        view.addSubview(btnNextMonth)
        
        view.addSubview(btnSelectDate)
        
        view.backgroundColor = UIColor.groupTableViewBackground
        
        self.setConstraints()
    }
    
    
    /// Set up constraint to UI Components
    private func setConstraints() {
        //Navigation Bar
        navBarView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(statusBarHeight + 44)
        }
        
        btnClose.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.topMargin).offset(8)
            make.left.equalToSuperview().offset(8)
            make.width.height.equalTo(32)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.centerY.equalTo(btnClose.snp.centerY)
            make.centerX.equalToSuperview()
        }
        
        //Date View
        dateBgStackView.snp.makeConstraints { (make) in
            make.top.equalTo(navBarView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(80)
        }
        //Departure Date
        ivDepartureDate.snp.makeConstraints { (make) in
            make.width.height.equalTo(24)
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(8)
        }
        tfDepartureDate.snp.makeConstraints { (make) in
            make.left.equalTo(ivDepartureDate.snp.right).offset(8)
            make.centerY.equalTo(ivDepartureDate.snp.centerY).offset(-8)
            make.right.equalToSuperview()
        }
        //Return Date
        ivReturnDate.snp.makeConstraints { (make) in
            make.width.height.equalTo(24)
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(8)
        }
        tfReturnDate.snp.makeConstraints { (make) in
            make.left.equalTo(ivReturnDate.snp.right).offset(8)
            make.centerY.equalTo(ivReturnDate.snp.centerY).offset(-8)
            make.right.equalToSuperview()
        }
        
        //Calendar View
        calendar.snp.makeConstraints { (make) in
            make.top.equalTo(dateBgStackView.snp.bottom)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(350)
        }
        
        btnPreviousMonth.snp.makeConstraints { (make) in
            make.top.left.equalTo(calendar).offset(8)
            make.width.height.equalTo(32)
        }
        
        btnNextMonth.snp.makeConstraints { (make) in
            make.top.equalTo(calendar).offset(8)
            make.right.equalTo(calendar).offset(-8)
            make.width.height.equalTo(32)
        }
        
        btnSelectDate.snp.makeConstraints { (make) in
            make.height.equalTo(48)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalTo(view.snp.bottomMargin).offset(-8)
        }
    }

    
    /// Calculate date range between two input dats
    ///
    /// - Parameters:
    ///   - from: From Date
    ///   - to: To Date
    /// - Returns: Date range array
    private func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    
    //Need to configure cell for date range selection
    fileprivate func configureVisibleCells() {
        calendar.visibleCells().forEach { (cell) in
            let date = calendar.date(for: cell)
            let position = calendar.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    //Configuring a cell
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        let diyCell = (cell as! CalendarCustomCell)
        // Configure selection layer
        if position == .current {
            
            var selectionType = SelectionType.none
            
            if calendar.selectedDates.contains(date) {
                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
                if calendar.selectedDates.contains(date) {
                    if calendar.selectedDates.contains(previousDate) && calendar.selectedDates.contains(nextDate) {
                        selectionType = .middle
                    }
                    else if calendar.selectedDates.contains(previousDate) && calendar.selectedDates.contains(date) {
                        selectionType = .rightBorder
                    }
                    else if calendar.selectedDates.contains(nextDate) {
                        selectionType = .leftBorder
                    }
                    else {
                        selectionType = .single
                    }
                }
            }
            else {
                selectionType = .none
            }
            
            if selectionType == .none {
                diyCell.selectionLayer.isHidden = true
                return
            }
            diyCell.selectionLayer.isHidden = false
            diyCell.selectionType = selectionType
            
        } else {
            diyCell.selectionLayer.isHidden = true
        }
    }
    
    @objc private func didTapBtnNextMonth() {
        print("Next tapped!")
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        var currentPage = calendar.currentPage
        dateComponents.month = 1
        currentPage = _calendar.date(byAdding: dateComponents, to: currentPage)!
        self.calendar.setCurrentPage(currentPage, animated: true)
    }
    
    @objc private func didTapBtnPreviousMonth() {
        print("Previous tapped!")
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        var currentPage = calendar.currentPage
        dateComponents.month = -1
        currentPage = _calendar.date(byAdding: dateComponents, to: currentPage)!
        self.calendar.setCurrentPage(currentPage, animated: true)
    }
    
    @objc private func didTapBtnClose() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func didTapBtnSelectDate() {
        if (firstDate == nil || lastDate == nil) && tripType != .Departure {
            let alert = UIAlertController(title: "Sorry", message: "Please select both departure date and return date.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            FlightModel.shared().flightReq.depDate = firstDate
            FlightModel.shared().flightReq.retDate = lastDate
            calendarProtocol?.getTravelDate(dep: firstDate, ret: lastDate)
            dismiss(animated: true, completion: nil)
        }
    }
}

//MARK: -
extension CalendarViewController: FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: self.cellId, for: date, at: position)
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        self.configure(cell: cell, for: date, at: position)
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        // nothing selected:
        if firstDate == nil {
            firstDate = date
            
            datesRange = [firstDate!]
            
            print("datesRange contains: \(datesRange)")
            self.configureVisibleCells()
            return
        }
        
        // only first date is selected:
        if firstDate != nil && lastDate == nil {
            // handle the case of if the last date is less than the first date:
            if date <= firstDate! {
                calendar.deselect(firstDate!)
                firstDate = date
                datesRange = [firstDate!]
                
                print("datesRange contains: \(datesRange)")
                self.configureVisibleCells()
                return
            }
            
            let range = datesRange(from: firstDate!, to: date)
            
            lastDate = range.last
            
            for d in range {
                calendar.select(d)
            }
            
            datesRange = range
            
            print("datesRange contains: \(datesRange)")
            self.configureVisibleCells()
            return
        }
        
        // both are selected:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            lastDate = nil
            firstDate = nil
            
            datesRange = []
            
            print("datesRange contains: \(datesRange)")
            
            self.configureVisibleCells()
        }
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        // both are selected:
        
        // NOTE: the is a REDUANDENT CODE:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            
            if tripType == .RoundTrip {
                lastDate = nil
            }
            firstDate = nil
            
            datesRange = []
            print("datesRange contains: \(datesRange)")
        }
        else if firstDate != nil && lastDate == nil && tripType == .RoundTrip {
            calendar.select(date)
        }
        
        self.configureVisibleCells()
    }
    
}

