//
//  FlightSearchViewController.swift
//  Flymya
//
//  Created by Kay Kay on 6/6/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import SnapKit
import SwiftyJSON
import Apollo

class FlightSearchViewController: UIViewController {
    
    lazy var naviBgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var ivMenu: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Menu")
        //button.titleLabel?.font = UIFont(name: AppConstants.Font.Poppins.SemiBold, size: 27)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isHidden = true
        return imageView
    }()
    
    lazy var ivIcon: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "flymya original white"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        return imageView
    }()
    
    lazy var btnLogin: UIButton = {
        let button = UIButton()
        button.setTitle("LOG IN", for: .normal)
        button.titleLabel?.font =  UIFont(name: AppConstants.Font.Roboto.Regular, size: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isHidden = true
        return button
    }()
    
    //Discover Myanmar
    lazy var textBgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var lblDiscover: UILabel = {
        let label = UILabel()
        label.text = "Discover"
        label.font = UIFont(name: AppConstants.Font.Poppins.Medium, size: 15)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var lblMyanmar: UILabel = {
        let label = UILabel()
        label.text = "Myanmar"
        label.font = UIFont(name: AppConstants.Font.Poppins.ExtraBold, size: 58)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5

        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // Serch Flight Button
    lazy var btnSearch: UIButton = {
        let button = UIButton()
        button.setTitle("Search Flights", for: .normal)
        button.titleLabel?.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 17)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.textColorGray
        button.layer.cornerRadius = 5
        button.isUserInteractionEnabled = false
        return button
    }()
    
    // Fill Data
    lazy var dataBgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black
        view.layer.cornerRadius = 5
        return view
    }()
    
    lazy var selectBgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.lightGray
        view.layer.cornerRadius = 5
        return view
    }()
    
    //dep city
    lazy var depcityBgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        return view
    }()
    
    lazy var lblFrom: UILabel = {
        let label = UILabel()
        label.text = "From"
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var lblFromIATA: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 18)
        label.textAlignment = .right
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var lblFromData: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 18)
        label.textAlignment = .right
        label.textColor = UIColor.textBlack
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var lblFromBefore: UILabel = {
        let label = UILabel()
        label.text = "Select City, Airport"
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        label.textAlignment = .right
        label.textColor = UIColor.lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    //Switch airport
    lazy var btnSwitchAirport: UIButton = {
        let btn = UIButton()
        btn.setTitle("", for: .normal)
        btn.setImage(UIImage(named: "baseline_swap_horiz_black_90px"), for: .normal)
        btn.titleLabel?.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)
        btn.backgroundColor = UIColor.white
        btn.setTitleColor(UIColor.darkText, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 18
        btn.layer.borderColor = UIColor.gray.cgColor
        btn.layer.borderWidth = 1
        btn.isUserInteractionEnabled = true
        
        return btn
    }()
    
    //ret city
    lazy var retcityBgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var lblTo: UILabel = {
        let label = UILabel()
        label.text = "To"
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var lblToIATA: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 18)
        label.textAlignment = .right
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var lblToData: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 18)
        label.textAlignment = .right
        label.textColor = UIColor.textBlack
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var lblToBefore: UILabel = {
        let label = UILabel()
        label.text = "Select City, Airport"
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        label.textAlignment = .right
        label.textColor = UIColor.lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    //Departure Date
    lazy var depDateBgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var lblDeparture: UILabel = {
        let label = UILabel()
        label.text = "Departure"
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var lblDepDate: UILabel = {
        let label = UILabel()
        label.text = Date().showDate
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 18)
        label.textAlignment = .right
        label.textColor = UIColor.textBlack
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    //Return Date
    lazy var retDateBgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var ivReturn: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "Switch On")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var lblReturn: UILabel = {
        let label = UILabel()
        label.text = "Return"
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var lblRetDate: UILabel = {
        let label = UILabel()
        label.text = retDate?.showDate
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 18)
        label.textAlignment = .right
        label.textColor = UIColor.textBlack
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    //Passenger Count
    lazy var passengerCountView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        return view
    }()
    
    lazy var lblPassenger: UILabel = {
        let label = UILabel()
        label.text = "Passenger"
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var lblPassengerCount: UILabel = {
        let label = UILabel()
        label.text = "1 Adult"
        label.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 18)
        label.textAlignment = .right
        label.textColor = UIColor.textBlack
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    //Citizen
    let citizenView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let lblCitizen: UILabel = {
        let label = UILabel()
        label.text = "Myanmar Citizen"
        label.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    let yesnoView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let btnYes: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "Bullet Check White 810"), for: .normal)
        button.setTitle(" Yes", for: .normal)
        button.titleLabel?.font =  UIFont(name: AppConstants.Font.Roboto.Regular, size: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let btnNo: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "Bullet Check Blue 811"), for: .normal)
        button.setTitle(" No", for: .normal)
        button.titleLabel?.font =  UIFont(name: AppConstants.Font.Roboto.Regular, size: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var tripType = "oneway"
    var returnFlag = false {
        didSet{
            if(returnFlag) {
                ivReturn.image = #imageLiteral(resourceName: "Switch On")
                //retDate = Calendar.current.date(byAdding: .day, value: 1, to: depDate!)!
                lblRetDate.isHidden = false
                lblRetDate.isUserInteractionEnabled = true
                tripType = "roundtrip"
            }
            else {
                ivReturn.image = #imageLiteral(resourceName: "Switch Off")
                //lblRetDate.text = ""
                lblRetDate.isHidden = true
                lblRetDate.isUserInteractionEnabled = false
                tripType = "oneway"
            }
        }
    }
    
    lazy var nationality = "Foreigner"
    var citizenFlag = true {
        didSet {
            if(citizenFlag) {
                nationality = "Local"
            }
            else {
                nationality = "Foreigner"
            }
        }
    }
        
    private var depDate: Date? = FlightModel.shared().flightReq.depDate {
        didSet {
            if let date = depDate {
                self.lblDepDate.text = date.showDate
                FlightModel.shared().flightReq.depDate = date
            }
            else {
                depDate = oldValue
                FlightModel.shared().flightReq.depDate = depDate
            }
        }
    }

    private var retDate: Date? = FlightModel.shared().flightReq.retDate { //Calendar.current.date(byAdding: .day, value: 1, to: Date())! {
        didSet {
            if let date = retDate {
                if(retDate! < depDate!) {
                    retDate = Calendar.current.date(byAdding: .day, value: 1, to: depDate!)
                    self.lblRetDate.text = retDate?.showDate
                    FlightModel.shared().flightReq.retDate = retDate
                }
                else {
                    self.lblRetDate.text = date.showDate
                    FlightModel.shared().flightReq.retDate = date
                }
            }
            else {
                retDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
                self.lblRetDate.text = retDate?.showDate
                FlightModel.shared().flightReq.retDate = retDate
            }
        }
    }
    
    private var depCity: CityResponse? = nil {
        didSet {
            if let city = depCity {
                lblFromData.text = city.cityName
                lblFromIATA.text = city.iATA
                lblFromBefore.isHidden = true
            }
            else {
                lblFromData.text = ""
                lblFromIATA.text = ""
                lblFromBefore.isHidden = false
            }
        }
    }
    
    private var retCity: CityResponse? = nil {
        didSet {
            if let city = retCity {
                lblToData.text = city.cityName
                lblToIATA.text = city.iATA
                lblToBefore.isHidden = true
            }
            else {
                lblToData.text = ""
                lblToIATA.text = ""
                lblToBefore.isHidden = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        self.setNeedsStatusBarAppearanceUpdate()
        
        depDate = FlightModel.shared().flightReq.depDate
        if(FlightModel.shared().flightReq.depCity?.cityName != "") {
            lblFromData.text = FlightModel.shared().flightReq.depCity?.cityName
            lblFromIATA.text = FlightModel.shared().flightReq.depCity?.iATA
        }
        if(FlightModel.shared().flightReq.retCity?.cityName != "") {
            lblToData.text = FlightModel.shared().flightReq.retCity?.cityName
            lblToIATA.text = FlightModel.shared().flightReq.retCity?.iATA
        }
        
        if FlightModel.shared().passengerList.isEmpty {
            FlightModel.shared().passengerList.append(FlightPassenger())
        }
        setPassengerCount()
    }
   
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        
        setupUI()
        
        setListener()
        
        readCityListJSON()
        
//        FlightModel.shared().getAirportList(success: {
//            print("=> Airport List - \(FlightModel.shared().airportList.count)")
//        }) { (error) in
//            print("ERROR => \(error)")
//        }
//
        FlightModel.shared().getCountryList(success: {
            print("=> Country List - \(FlightModel.shared().countryList.count)")
        }) { (error) in
            print("ERROR => \(error)")
        }
    }
    
    private func readCityListJSON() {
        
        do {
            if let file = Bundle.main.url(forResource: "citylist", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                
                var citylist: [CityResponse] = []
                for field in (json as? [[String: AnyObject]])! {
                    var city = CityResponse()
                    city.cityId = field["CityId"] as? Int
                    city.cityName = field["CityName"] as? String
                    city.iATA = field["IATA"] as? String
                    citylist.append(city)
                }
                FlightModel.shared().airportList = citylist

            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func setListener() {
        let depCityTap = UITapGestureRecognizer(target: self, action: #selector(self.chooseDepCity))
        depcityBgView.isUserInteractionEnabled = true
        depcityBgView.addGestureRecognizer(depCityTap)
        
        let retCityTap = UITapGestureRecognizer(target: self, action: #selector(self.chooseRetCity))
        retcityBgView.isUserInteractionEnabled = true
        retcityBgView.addGestureRecognizer(retCityTap)
        
        let fromDateTap = UITapGestureRecognizer(target: self, action: #selector(self.chooseFromDate))
        depDateBgView.isUserInteractionEnabled = true
        depDateBgView.addGestureRecognizer(fromDateTap)
        
        let toDateTap = UITapGestureRecognizer(target: self, action: #selector(self.chooseToDate))
        lblRetDate.isUserInteractionEnabled = true
        lblRetDate.addGestureRecognizer(toDateTap)
        
        let returnTap = UITapGestureRecognizer(target: self, action: #selector(self.changeReturn))
        ivReturn.isUserInteractionEnabled = true
        ivReturn.addGestureRecognizer(returnTap)
        
        let passengerTap = UITapGestureRecognizer(target: self, action: #selector(self.clickPassenger))
        passengerCountView.isUserInteractionEnabled = true
        passengerCountView.addGestureRecognizer(passengerTap)
        
        btnYes.addTarget(self, action: #selector(yesClicked(_:)), for: .touchUpInside)
        btnNo.addTarget(self, action: #selector(noClicked(_:)), for: .touchUpInside)
        btnSwitchAirport.addTarget(self, action: #selector(didTapSwitchAirport), for: .touchUpInside)
        btnSearch.addTarget(self, action: #selector(clickSearch(_:)), for: .touchUpInside)
    }
    
    private func setPassengerCount() {
        lblPassengerCount.text = FlightModel.shared().getPassengerCountText()
    }
    
    @objc func clickPassenger(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.StoryboardID.EditPassFlightVC) as! EditCustomerViewController
//        vc.setDelegate(passCount: self)
        vc.changeCountHandler = {
            self.setPassengerCount()
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func chooseFromDate(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
        if(returnFlag) {
            vc.setTripType(type: .RoundTrip, fromDate: depDate, toDate: retDate)
        }
        else {
            vc.setTripType(type: .Departure, fromDate: depDate, toDate: retDate)
        }
        vc.setProtocol(calendar: self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func chooseToDate(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
        vc.setTripType(type: .Return, fromDate: depDate, toDate: retDate)
        vc.setProtocol(calendar: self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func chooseDepCity(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "DestinationSearchViewController") as! DestinationSearchViewController
        vc.setDestinationType(type: .Departure)
        vc.setDelegate(dep: self, ret: self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func chooseRetCity(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "DestinationSearchViewController") as! DestinationSearchViewController
        vc.setDestinationType(type: .Return)
        vc.setDelegate(dep: self, ret: self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func yesClicked(_ sender: UIButton) {
        btnYes.setImage(#imageLiteral(resourceName: "Bullet Check Blue 811"), for: .normal)
        btnNo.setImage(#imageLiteral(resourceName: "Bullet Check White 810"), for: .normal)
        citizenFlag = true
    }
    
    @objc func noClicked(_ sender: UIButton) {
        btnNo.setImage(#imageLiteral(resourceName: "Bullet Check Blue 811"), for: .normal)
        btnYes.setImage(#imageLiteral(resourceName: "Bullet Check White 810"), for: .normal)
        citizenFlag = false
    }
    
    @objc func changeReturn(_ sender: Any) {
        returnFlag = !returnFlag
    }
    
    @objc func didTapSwitchAirport() {
        let tempAirport1 = depCity
        let tempAirport2 = retCity
        depCity = tempAirport2
        retCity = tempAirport1
    }
    
    @objc func clickSearch(_ sender: UIButton) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.StoryboardID.FlightListVC) as! FlightListViewController
        let req = FlightListReq.init(depCity: depCity!, retCity: retCity!,depDate: depDate!, retDate: retDate!, nationality: nationality, tripType: tripType)
        FlightModel.shared().flightReq = req
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func setupUI() {
        
        self.view.backgroundColor = UIColor.black
//        if UIDevice.current.userInterfaceIdiom == .pad {
//            let imgView = UIImageView(image: UIImage(named: "backgroun-mobile-v0-cut"))
//            view.addSubview(imgView)
//
//
//            if(screenHeight >= 1366) {
//                imgView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 1200)
//            }
//            else if(screenHeight >= 1112) {
//                imgView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 1000)
//            }
//            else {
//                imgView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 950)
//            }
//
//        }
//        else {
            let imgView = UIImageView(image: UIImage(named: "background"))
            view.addSubview(imgView)

            if (screenHeight >= 812 && screenWidth >= 414) {
                imgView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 730)
            }
            else if (screenHeight >= 812 && screenWidth >= 375) {
                imgView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 660)
            }
            else if (screenHeight >= 736) {
                imgView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 750)
            }
            else if(screenHeight >= 667) {
                imgView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 670)
            }
            else {
                imgView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 550)
            }
            
       // }
        
        view.addSubview(naviBgView)
        naviBgView.addSubview(ivMenu)
        naviBgView.addSubview(ivIcon)
        naviBgView.addSubview(btnLogin)
        
        self.view.addSubview(textBgView)
        textBgView.addSubview(lblDiscover)
        textBgView.addSubview(lblMyanmar)
        
        view.addSubview(btnSearch)
        
        view.addSubview(dataBgView)
        dataBgView.addSubview(selectBgView)
        
        selectBgView.addSubview(depcityBgView)
        depcityBgView.addSubview(lblFrom)
        depcityBgView.addSubview(lblFromIATA)
        depcityBgView.addSubview(lblFromData)
        depcityBgView.addSubview(lblFromBefore)
        
        dataBgView.addSubview(btnSwitchAirport)
        
        selectBgView.addSubview(retcityBgView)
        retcityBgView.addSubview(lblTo)
        retcityBgView.addSubview(lblToIATA)
        retcityBgView.addSubview(lblToData)
        retcityBgView.addSubview(lblToBefore)
        
        selectBgView.addSubview(depDateBgView)
        depDateBgView.addSubview(lblDeparture)
        depDateBgView.addSubview(lblDepDate)
        
//        selectBgView.addSubview(retDateBgView)
//        retDateBgView.addSubview(ivReturn)
//        retDateBgView.addSubview(lblReturn)
//        retDateBgView.addSubview(lblRetDate)
        
        selectBgView.addSubview(passengerCountView)
        passengerCountView.addSubview(lblPassenger)
        passengerCountView.addSubview(lblPassengerCount)

        dataBgView.addSubview(citizenView)
        citizenView.addSubview(lblCitizen)
        citizenView.addSubview(yesnoView)
        yesnoView.addSubview(btnYes)
        yesnoView.addSubview(btnNo)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        
        //navigation bar
        naviBgView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                make.top.equalTo(self.view.snp.topMargin).offset(20)
//            }
//            else {
                make.top.equalTo(self.view.snp.topMargin).offset(10)
//            }
            make.height.equalTo(30)
        }
        
        ivMenu.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview()
//
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                make.height.equalTo(20)
//                make.width.equalTo(20)
//            }
        }
        
        ivIcon.snp.makeConstraints { (make) in
            make.centerY.centerX.equalToSuperview()
            //make.left.equalTo(ivMenu.snp.right).offset(15)
        
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                make.height.equalTo(45)
//                make.width.equalTo(200)
//            }
//            else {
                make.height.equalTo(25)
                make.width.equalTo(130)
//            }
        }
        
        btnLogin.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview()
        }

        //Discover Myanmar
        textBgView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview().dividedBy(1.5)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalToSuperview()
            make.height.equalTo(lblMyanmar.snp.height)
        }
      
        lblDiscover.snp.makeConstraints { (make) in
            make.right.equalTo(lblMyanmar.snp.right)
            make.top.equalToSuperview()
            
        }
        lblMyanmar.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview()
        }
        
//        if (self.view.frame.width == 320) {
//            lblDiscover.font = lblDiscover.font.withSize(16)
//            lblMyanmar.font = lblMyanmar.font.withSize(55)
//            lblCitizen.font = lblCitizen.font.withSize(15)
//            btnSearch.titleLabel?.font = btnSearch.titleLabel?.font.withSize(18)
//
//        } else if (self.view.frame.width == 375) {
//
//            lblMyanmar.font = lblMyanmar.font.withSize(65)
//            lblCitizen.font = lblCitizen.font.withSize(18)
//            btnSearch.titleLabel?.font = btnSearch.titleLabel?.font.withSize(20)
//            lblFromData.font = lblFromData.font.withSize(18)
//            lblFromIATA.font = lblFromIATA.font.withSize(18)
//            lblFromBefore.font = lblFromBefore.font.withSize(17)
//            lblToData.font = lblToData.font.withSize(18)
//            lblToIATA.font = lblToIATA.font.withSize(18)
//            lblToBefore.font = lblToBefore.font.withSize(17)
//            lblDepDate.font = lblDepDate.font.withSize(18)
//            lblRetDate.font = lblRetDate.font.withSize(18)
//
//        } else if (self.view.frame.width == 414) {//biggest iphone
//
//            lblMyanmar.font = lblMyanmar.font.withSize(71)
//            lblCitizen.font = lblCitizen.font.withSize(20)
//            btnSearch.titleLabel?.font = btnSearch.titleLabel?.font.withSize(22)
//            lblFrom.font = lblFrom.font.withSize(16)
//            lblTo.font = lblTo.font.withSize(16)
//            lblDeparture.font = lblDeparture.font.withSize(16)
//            lblReturn.font = lblReturn.font.withSize(16)
//            lblFromData.font = lblFromData.font.withSize(20)
//            lblFromIATA.font = lblFromIATA.font.withSize(20)
//            lblFromBefore.font = lblFromData.font.withSize(19)
//            lblToData.font = lblToData.font.withSize(20)
//            lblToIATA.font = lblToIATA.font.withSize(20)
//            lblToBefore.font = lblToBefore.font.withSize(19)
//            lblDepDate.font = lblDepDate.font.withSize(20)
//            lblRetDate.font = lblRetDate.font.withSize(20)
//
//        }
//
//        else if (self.view.frame.width >= 768) {
//            lblMyanmar.font = lblMyanmar.font.withSize(100)
//            lblDiscover.font = lblDiscover.font.withSize(25)
//            btnSearch.titleLabel?.font = btnSearch.titleLabel?.font.withSize(30)
//            lblFrom.font = lblFrom.font.withSize(20)
//            lblTo.font = lblTo.font.withSize(20)
//            lblDeparture.font = lblDeparture.font.withSize(20)
//            lblReturn.font = lblReturn.font.withSize(20)
//            lblFromData.font = lblFromData.font.withSize(25)
//            lblFromIATA.font = lblFromIATA.font.withSize(25)
//            lblFromBefore.font = lblFromData.font.withSize(24)
//            lblToData.font = lblToData.font.withSize(25)
//            lblToIATA.font = lblToIATA.font.withSize(25)
//            lblToBefore.font = lblToBefore.font.withSize(24)
//            lblDepDate.font = lblDepDate.font.withSize(25)
//            lblRetDate.font = lblRetDate.font.withSize(25)
//            lblCitizen.font = lblCitizen.font.withSize(25)
//            btnYes.titleLabel!.font = btnYes.titleLabel!.font.withSize(25)
//            btnNo.titleLabel!.font = btnNo.titleLabel!.font.withSize(25)
//            btnLogin.titleLabel!.font = btnLogin.titleLabel!.font.withSize(25)
//        }
        
        
        //Fill Data Form
        dataBgView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                make.width.equalToSuperview().multipliedBy(0.6)
//            }
//            else {
                make.width.equalToSuperview().multipliedBy(0.95)
//            }
            make.bottom.equalTo(btnSearch.snp.top).offset(-10)
//            make.height.equalToSuperview().multipliedBy(0.35)//(0.43)
            
        }
        
        // Choose Citizen
        citizenView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(64)
        }
        
        lblCitizen.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.45)
        }
        
        yesnoView.snp.makeConstraints { (make) in
            make.right.top.bottom.equalToSuperview()
            make.left.equalTo(lblCitizen.snp.right)
        }
        
        btnYes.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.5)
            make.left.equalToSuperview()
        }
        
        btnNo.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.5)
            make.right.equalToSuperview()
        }
        
        //Form
        selectBgView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(citizenView.snp.bottom).offset(1)
        }

        //Departure City
        depcityBgView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(56)
        }
        
        lblFrom.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.3)
        }
        
        lblFromIATA.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.15)
        }
        
        lblFromBefore.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
        }
        
        lblFromData.snp.makeConstraints { (make) in
            make.right.equalTo(lblFromIATA.snp.left)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
        }
        
        btnSwitchAirport.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-8)
            make.centerY.equalTo(depcityBgView.snp.bottom).offset(0)
            make.width.height.equalTo(36)
        }
        
        //Arrival City
        retcityBgView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(56)
            make.top.equalTo(depcityBgView.snp.bottom).offset(1)
        }

        lblTo.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.3)
        }
        
        lblToIATA.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.15)
        }
        
        lblToBefore.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
        }
        
        lblToData.snp.makeConstraints { (make) in
            make.right.equalTo(lblToIATA.snp.left)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
        }

        //Departure Date
        depDateBgView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(56)
            make.top.equalTo(retcityBgView.snp.bottom).offset(1)
        }

        lblDeparture.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.3)
        }
        
        lblDepDate.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
        }
        
        /*retDateBgView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(depDateBgView.snp.bottom).offset(1)
        }
        
        ivReturn.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.width.equalTo(40)
            make.height.equalTo(25)
            
//            if (self.view.frame.width >= 768) {
//                make.width.equalTo(45)
//                make.height.equalTo(30)
//            }
//            else if (self.view.frame.width >= 414) {
//                make.width.equalTo(53)
//                make.height.equalTo(35)
//            }
//            else if (self.view.frame.width >= 375) {
//                make.width.equalTo(48)
//                make.height.equalTo(28)
//            }
//            else {
//                make.width.equalTo(40)
//                make.height.equalTo(25)
//            }
        }
        
        lblReturn.snp.makeConstraints { (make) in
            make.left.equalTo(ivReturn.snp.right).offset(5)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.25)
        }
        
        lblRetDate.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.height.equalToSuperview()
            make.left.equalTo(lblReturn.snp.right).offset(10)
        }
    */
        
        //Passenger Count
        passengerCountView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(56)
            make.top.equalTo(depDateBgView.snp.bottom).offset(1)
        }
        
        lblPassenger.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.3)
        }
        
        lblPassengerCount.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
        }
        
        //Search Flight Button
        btnSearch.snp.makeConstraints { (make) in
            //            if UIDevice.current.userInterfaceIdiom == .pad {
            //                make.width.equalToSuperview().multipliedBy(0.6)
            //            }
            //            else {
            make.width.equalToSuperview().multipliedBy(0.95)
            //            }
            make.bottomMargin.equalToSuperview().offset(-20)
            make.height.equalTo(48)
            make.centerX.equalToSuperview()
        }
    }
}

extension FlightSearchViewController : calProtocol, DepCityProtocol, RetCityProtocol {
    func getDepCity(for city: CityResponse) {
        depCity = city
        if(lblToData.text != "") {
            btnSearch.backgroundColor = UIColor.primaryBlue
            btnSearch.isUserInteractionEnabled = true
        }
    }
    
    func getRetCity(for city: CityResponse) {
        retCity = city
        if(lblFromData.text != "") {
            btnSearch.backgroundColor = UIColor.primaryBlue
            btnSearch.isUserInteractionEnabled = true
        }
    }
    
    func getTravelDate(dep: Date?, ret: Date?) {
        depDate = dep
        retDate = ret
    }
    
}
