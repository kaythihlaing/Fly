//
//  DestinationSearchViewController.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/10/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import SnapKit

enum DestinationType {
    case Departure
    case Return
    case Country
}

struct DestinationCity {
    var name: String = ""
    var iata: String = ""
}

class DestinationSearchViewController: ViewController {
    
    //MARK: - View Components
    lazy private var resultTableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(DestinationSearchResultTableViewCell.self, forCellReuseIdentifier: "DestinationSearchResultTableViewCell")
        return tableView
    }()
    
    lazy private var customNavBarView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0, green: 0.6745098039, blue: 0.9647058824, alpha: 1)
        return view
    }()
    
    lazy private var lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.text = "Select City, Airport"
        lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lbl.font = UIFont(name: AppConstants.Font.AvenirNext.DemiBold, size: 20)
        return lbl
    }()
    
    lazy private var btnClose: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Cancel"), for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnClose), for: .touchUpInside)
        return btn
    }()
    
    lazy private var tfSearch: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Search city or airport"
        tf.borderStyle = UITextField.BorderStyle.roundedRect
        tf.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        let icSearch = UIImageView(image: #imageLiteral(resourceName: "Search"))
        if let size = icSearch.image?.size {
            icSearch.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 20.0, height: size.height)
        }
        icSearch.contentMode = .center
        tf.rightView = icSearch
        tf.rightViewMode = .always
        
        return tf
    }()
    
    //MARK: - Class Attributes
    private var nameList: [String] = [
        "Yangon",
        "Mandalay",
        "Bagan, Nyaung U",
        "Inle, Heho",
        "Nay Pyi Taw",
        "Ann",
        "Bhamo",
        "Bokpyin",
        "Dawei",
        "Homalin"
    ]
    
    private var iataList : [String] = [
        "RGN",
        "MDY",
        "NYU",
        "HEH",
        "NPT",
        "VBA",
        "BMO",
        "VBP",
        "TVY",
        "HOX"
    ]
    
    private var destinationList: [CityResponse] = []
    
    private var destinationType : DestinationType = .Departure
    
    private var filteredList: [CityResponse] = []
    
    private var departureCityProtocol: DepCityProtocol?
    
    private var returnCityProtocol: RetCityProtocol?
    
    private var selectCountryProtocol: CountryProtocol?
    
    private var countryList: [CountryResponse] = []
    
    private var filteredCountryList: [CountryResponse] = []
    

    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNeedsStatusBarAppearanceUpdate()

        self.setupUI()
        
        switch destinationType {
        case .Departure:
            lblTitle.text = "Flight From"
        case .Return:
            lblTitle.text = "Flight To"
        case .Country:
            lblTitle.text = "Country"
        default:
            lblTitle.text = ""
        }
        
        self.destinationList = FlightModel.shared().airportList
        filteredList.removeAll()
        
        if destinationType == .Departure {
            for i in 0..<destinationList.count {
                if destinationList[i].cityId != FlightModel.shared().flightReq.retCity?.cityId {
                    filteredList.append(destinationList[i])
                }
            }
            
        }
        else if destinationType == .Return {
            for i in 0..<destinationList.count {
                if destinationList[i].cityId != FlightModel.shared().flightReq.depCity?.cityId {
                    filteredList.append(destinationList[i])
                }
            }
        }
        
        self.countryList = FlightModel.shared().countryList
        self.filteredCountryList = self.countryList
        

        tfSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        tfSearch.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tfSearch.resignFirstResponder()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Public Functions
    func setDestinationType(type: DestinationType) {
        self.destinationType = type
    }
    
    func setDelegate(dep: DepCityProtocol, ret: RetCityProtocol) {
        self.departureCityProtocol = dep
        self.returnCityProtocol = ret
    }
    
    func setDelegate(country: CountryProtocol) {
        self.selectCountryProtocol = country
    }
    
    //MARK: - Private Functions
    fileprivate func setupUI() {
        self.view.addSubview(customNavBarView)
        self.customNavBarView.addSubview(lblTitle)
        self.customNavBarView.addSubview(tfSearch)
        self.customNavBarView.addSubview(btnClose)
        
        self.view.addSubview(resultTableView)
        
        self.setConstraints()
    }
    
    fileprivate func setConstraints() {
        customNavBarView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.bottom.equalTo(tfSearch.snp.bottom).offset(8)
        }
        
        btnClose.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.topMargin).offset(8)
            make.left.equalToSuperview().offset(8)
            make.width.height.equalTo(32)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.topMargin).offset(8)
            make.centerX.equalToSuperview()
        }
        
        tfSearch.snp.makeConstraints { (make) in
            make.top.equalTo(lblTitle.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(8)
            make.right.equalToSuperview().offset(-8)
            make.height.equalTo(48)
        }
        
        resultTableView.snp.makeConstraints { (make) in
            make.top.equalTo(customNavBarView.snp.bottom).offset(0)
            make.left.right.bottom.equalToSuperview()
        }
        
        
    }
    
    fileprivate func filterResult(keyword: String) {
        if destinationType == .Country {
            if !keyword.isEmpty {
                let filtered = countryList.filter {
                    $0.countryName!.localizedCaseInsensitiveContains(keyword)
                }
                self.filteredCountryList = filtered
                resultTableView.reloadData()
                self.changeAllItemTextColor()
                self.changeFirstItemTextColor()
            } else {
                self.filteredCountryList = self.countryList
                resultTableView.reloadData()
                self.changeAllItemTextColor()
            }
        }
        else {
            if !keyword.isEmpty {
                let filtered = destinationList.filter {
                    $0.cityName!.localizedCaseInsensitiveContains(keyword) || $0.iATA!.localizedCaseInsensitiveContains(keyword)
                }
                self.filteredList = filtered
                resultTableView.reloadData()
                self.changeAllItemTextColor()
                self.changeFirstItemTextColor()
            } else {
                self.filteredList = self.destinationList
                resultTableView.reloadData()
                self.changeAllItemTextColor()
            }
        }
    }
    
    fileprivate func changeAllItemTextColor() {
        for cell in resultTableView.visibleCells {
            let c = cell as? DestinationSearchResultTableViewCell
            c?.changeTextColorBlack()
        }
    }
    
    fileprivate func changeFirstItemTextColor() {
        let cell = resultTableView.visibleCells.first as? DestinationSearchResultTableViewCell
        cell?.changeTextColorBlue()
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        self.filterResult(keyword: textField.text ?? "")
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc private func didTapBtnClose() {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: -
extension DestinationSearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if destinationType == .Country {
            return filteredCountryList.count
        }
        else {
            return filteredList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "DestinationSearchResultTableViewCell", for: indexPath) as? DestinationSearchResultTableViewCell {
            if destinationType == .Country {
                cell.setData(filteredCountryList[indexPath.row])
            }
            else {
//                if destinationType == .Departure {
//                    if(FlightModel.shared().flightReq.retCity?.cityId == filteredList[indexPath.row].cityId){
//                        cell.setAlreadySelected()
//
//                    }
//                }
//                else {
//                    if(FlightModel.shared().flightReq.depCity?.cityId == filteredList[indexPath.row].cityId){
//                        cell.setAlreadySelected()
//                    }
//                }
                
                cell.setData(filteredList[indexPath.row])
                
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if destinationType == .Country {
            return "Passport Country"
        }
        else {
            return "Popular Destination"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

//MARK: -
extension DestinationSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if destinationType == .Country {
            tfSearch.text = filteredCountryList[indexPath.row].countryName
        }
        else {
            tfSearch.text = filteredList[indexPath.row].cityName
        }
        tfSearch.resignFirstResponder()
        
        self.changeAllItemTextColor()
        
        let cell = tableView.cellForRow(at: indexPath) as? DestinationSearchResultTableViewCell
        cell?.changeTextColorBlue()
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if destinationType == .Departure {
            FlightModel.shared().flightReq.depCity = filteredList[indexPath.row]
            self.departureCityProtocol?.getDepCity(for: filteredList[indexPath.row])
            dismiss(animated: true, completion: nil)
        } else if destinationType == .Return {
            FlightModel.shared().flightReq.retCity = filteredList[indexPath.row]
            self.returnCityProtocol?.getRetCity(for: filteredList[indexPath.row])
            dismiss(animated: true, completion: nil)
        }
        else {
            FlightModel.shared().passengerList[FlightModel.shared().currentPassenger].country = filteredCountryList[indexPath.row]
            self.selectCountryProtocol?.chooseCountry(country: filteredCountryList[indexPath.row])
            dismiss(animated: true, completion: nil)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tfSearch.resignFirstResponder()
    }
}
