//
//  EditCustomerViewController.swift
//  Flymya
//
//  Created by Kay Kay on 6/20/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit

class EditCustomerViewController: UIViewController {
    
    lazy var bgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        return view
    }()
    
    lazy var adultView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var lblAdult: UILabel = {
        let label = UILabel()
        label.text = "Adult"
        label.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        return label
    }()
    
    lazy var lblAdultAge: UILabel = {
        let label = UILabel()
        label.text = "(Age 12 and over)"
        label.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 13)
        label.textColor = UIColor().HexToColor(hexString: "#6D8494")
        return label
    }()
    
    lazy var adultCountView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor().HexToColor(hexString: "#C7C7C7").cgColor
        return view
    }()
    
    lazy var btnMinusAdult: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "Minus"), for: .normal)
        button.contentMode = .center
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }()
    
    lazy var lineView1: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor().HexToColor(hexString: "#C7C7C7")
        return view
    }()
    
    lazy var lblAdultCount: UILabel = {
        let label = UILabel()
        label.text = "\(adultCount)"
        label.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        label.textAlignment = .center
        return label
    }()
    
    lazy var lineView2: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor().HexToColor(hexString: "#C7C7C7")
        return view
    }()
    
    lazy var btnPlusAdult: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "Plus"), for: .normal)
        button.contentMode = .center
        return button
    }()
    
    lazy var childView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var lblChild: UILabel = {
        let label = UILabel()
        label.text = "Child"
        label.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        return label
    }()
    
    lazy var lblChildAge: UILabel = {
        let label = UILabel()
        label.text = "(Age 2 - 11)"
        label.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 13)
        label.textColor = UIColor().HexToColor(hexString: "#6D8494")
        return label
    }()
    
    lazy var childCountView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor().HexToColor(hexString: "#C7C7C7").cgColor
        return view
    }()
    
    lazy var btnMinusChild: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "Minus"), for: .normal)
        button.contentMode = .center
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }()
    
    lazy var lineView3: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor().HexToColor(hexString: "#C7C7C7")
        return view
    }()
    
    lazy var lblChildCount: UILabel = {
        let label = UILabel()
        label.text = "\(childCount)"
        label.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        label.textAlignment = .center
        return label
    }()
    
    lazy var lineView4: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor().HexToColor(hexString: "#C7C7C7")
        return view
    }()
    
    lazy var btnPlusChild: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "Plus"), for: .normal)
        button.contentMode = .center
        return button
    }()
    
    lazy var infantView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var lblInfant: UILabel = {
        let label = UILabel()
        label.text = "Infant"
        label.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        return label
    }()
    
    lazy var lblInfantAge: UILabel = {
        let label = UILabel()
        label.text = "(Below age 2)"
        label.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 13)
        label.textColor = UIColor().HexToColor(hexString: "#6D8494")
        return label
    }()
    
    lazy var infantCountView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor().HexToColor(hexString: "#C7C7C7").cgColor
        return view
    }()
    
    lazy var btnMinusInfant: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "Minus"), for: .normal)
        button.contentMode = .center
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }()
    
    lazy var lineView5: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor().HexToColor(hexString: "#C7C7C7")
        return view
    }()
    
    lazy var lblInfantCount: UILabel = {
        let label = UILabel()
        label.text = "\(infantCount)"
        label.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        label.textAlignment = .center
        return label
    }()
    
    lazy var lineView6: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor().HexToColor(hexString: "#C7C7C7")
        return view
    }()
    
    lazy var btnPlusInfant: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "Plus"), for: .normal)
        button.contentMode = .center
        return button
    }()
    
    lazy var btnCancel: UIButton = {
        let button = UIButton()
        button.setTitle("Cancel", for: .normal)
        button.backgroundColor = UIColor().HexToColor(hexString: "#F0F0F0")
        button.titleLabel?.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 16)
        button.setTitleColor(UIColor.primaryBlue, for: .normal)
        button.layer.cornerRadius = 5
        button.isHidden = true
        return button
    }()
    
    lazy var btnSelect: UIButton = {
        let button = UIButton()
        button.setTitle("Select", for: .normal)
        button.backgroundColor = UIColor.primaryBlue
        button.titleLabel?.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 16)
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 5
        button.isHidden = true
        return button
    }()
    
    private var adultCount: Int = FlightModel.shared().getAdultPassengerCount() {
        didSet {
            if adultCount > 10 {
                lblAdultCount.text = "10"
                adultCount = 10
            }
            else if adultCount < 1 {
                lblAdultCount.text = "1"
                adultCount = 1
            }
            else {
                lblAdultCount.text = "\(adultCount)"
            }
        }
    }
    
    private var childCount: Int = FlightModel.shared().getChildPassengerCount() {
        didSet {
            if childCount > 10 {
                lblChildCount.text = "10"
                childCount = 10
            }
            else if childCount < 0 {
                lblChildCount.text = "0"
                childCount = 0
            }
            else {
                lblChildCount.text = "\(childCount)"
            }
        }
    }
    
    private var infantCount: Int = FlightModel.shared().getInfantPassengerCount() {
        didSet {
            if infantCount > 10 {
                lblInfantCount.text = "10"
                infantCount = 10
            }
            else if infantCount < 0 {
                lblInfantCount.text = "0"
                infantCount = 0
            }
            else {
                lblInfantCount.text = "\(infantCount)"
            }
        }
    }
    
    private var passengerProtocol: PassengerCountProtocol?
    
    var changeCountHandler: () -> () = {}
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.setNeedsStatusBarAppearanceUpdate()
        
        setCount()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func setDelegate(passCount: PassengerCountProtocol) {
        self.passengerProtocol = passCount
    }
    
    private func setCount() {
        self.adultCount = FlightModel.shared().getAdultPassengerCount()
        self.childCount = FlightModel.shared().getChildPassengerCount()
        self.infantCount = FlightModel.shared().getInfantPassengerCount()
    }
    
    override func viewDidLoad() {
        setupUI()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(cancelClicked(_:)))
        view.addGestureRecognizer(tap)
        
        btnCancel.addTarget(self, action: #selector(cancelClicked(_:)), for: .touchUpInside)
        btnSelect.addTarget(self, action: #selector(selectClicked(_:)), for: .touchUpInside)
        
        btnPlusAdult.addTarget(self, action: #selector(plusAdultClicked(_:)), for: .touchUpInside)
        btnMinusAdult.addTarget(self, action: #selector(minusAdultClicked(_:)), for: .touchUpInside)
        btnPlusChild.addTarget(self, action: #selector(plusChildClicked(_:)), for: .touchUpInside)
        btnMinusChild.addTarget(self, action: #selector(minusChildClicked(_:)), for: .touchUpInside)
        btnPlusInfant.addTarget(self, action: #selector(plusInfantClicked(_:)), for: .touchUpInside)
        btnMinusInfant.addTarget(self, action: #selector(minusInfantClicked(_:)), for: .touchUpInside)
        
    }
    
    private func setupUI() {
        
        view.addSubview(bgView)
        
        bgView.addSubview(adultView)
        adultView.addSubview(lblAdult)
        adultView.addSubview(lblAdultAge)
        adultView.addSubview(adultCountView)
        adultCountView.addSubview(btnMinusAdult)
        adultCountView.addSubview(lineView1)
        adultCountView.addSubview(lblAdultCount)
        adultCountView.addSubview(lineView2)
        adultCountView.addSubview(btnPlusAdult)
        
        bgView.addSubview(childView)
        childView.addSubview(lblChild)
        childView.addSubview(lblChildAge)
        childView.addSubview(childCountView)
        childCountView.addSubview(btnMinusChild)
        childCountView.addSubview(lineView3)
        childCountView.addSubview(lblChildCount)
        childCountView.addSubview(lineView4)
        childCountView.addSubview(btnPlusChild)
        
        bgView.addSubview(infantView)
        infantView.addSubview(lblInfant)
        infantView.addSubview(lblInfantAge)
        infantView.addSubview(infantCountView)
        infantCountView.addSubview(btnMinusInfant)
        infantCountView.addSubview(lineView5)
        infantCountView.addSubview(lblInfantCount)
        infantCountView.addSubview(lineView6)
        infantCountView.addSubview(btnPlusInfant)
        
//        bgView.addSubview(btnCancel)
//        bgView.addSubview(btnSelect)
        
        addConstraints()
        
    }
    
    func addConstraints() {
        
        bgView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(250)
        }
        
        adultView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(10)
            make.height.equalTo(75)
            make.right.equalToSuperview().offset(-10)
        }
        
        lblAdult.snp.makeConstraints{ (make) in
            make.leading.equalToSuperview()
            make.top.equalToSuperview().offset(5)
        }
        
        lblAdultAge.snp.makeConstraints{ (make) in
            make.leading.equalToSuperview()
            make.top.equalTo(lblAdult.snp.bottom).offset(5)
        }
        
        adultCountView.snp.makeConstraints{ (make) in
            make.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(5)
            make.width.equalTo(150)
            make.height.equalTo(40)
        }
        
        btnMinusAdult.snp.makeConstraints{ (make) in
            make.centerY.leading.height.equalToSuperview()
            make.width.equalTo(49)
        }
        
        lineView1.snp.makeConstraints{ (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(btnMinusAdult.snp.right)
            make.width.equalTo(1)
        }
        
        lblAdultCount.snp.makeConstraints{ (make) in
            make.left.equalTo(lineView1.snp.right)
            make.centerY.equalToSuperview()
            make.width.equalTo(50)
        }
        
        lineView2.snp.makeConstraints{ (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(lblAdultCount.snp.right)
            make.width.equalTo(1)
        }
        
        btnPlusAdult.snp.makeConstraints{ (make) in
            make.centerY.height.equalToSuperview()
            make.width.equalTo(49)
            make.height.equalTo(15)
            make.left.equalTo(lineView2.snp.right)
        }
        
        childView.snp.makeConstraints { (make) in
            make.top.equalTo(adultView.snp.bottom)
            make.left.equalToSuperview().offset(10)
            make.height.equalTo(75)
            make.right.equalToSuperview().offset(-10)
        }
        
        lblChild.snp.makeConstraints{ (make) in
            make.leading.equalToSuperview()
            make.top.equalToSuperview().offset(5)
        }
        
        lblChildAge.snp.makeConstraints{ (make) in
            make.leading.equalToSuperview()
            make.top.equalTo(lblChild.snp.bottom).offset(5)
        }
        
        childCountView.snp.makeConstraints{ (make) in
            make.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(5)
            make.width.equalTo(150)
            make.height.equalTo(40)
        }
        
        btnMinusChild.snp.makeConstraints{ (make) in
            make.centerY.leading.height.equalToSuperview()
            make.width.equalTo(49)
        }
        
        lineView3.snp.makeConstraints{ (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(btnMinusChild.snp.right)
            make.width.equalTo(1)
        }
        
        lblChildCount.snp.makeConstraints{ (make) in
            make.left.equalTo(lineView3.snp.right)
            make.centerY.equalToSuperview()
            make.width.equalTo(50)
        }
        
        lineView4.snp.makeConstraints{ (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(lblChildCount.snp.right)
            make.width.equalTo(1)
        }
        
        btnPlusChild.snp.makeConstraints{ (make) in
            make.centerY.height.equalToSuperview()
            make.width.equalTo(49)
            make.height.equalTo(15)
            make.left.equalTo(lineView4.snp.right)
        }
        
        infantView.snp.makeConstraints { (make) in
            make.top.equalTo(childView.snp.bottom)
            make.left.equalToSuperview().offset(10)
            make.height.equalTo(75)
            make.right.equalToSuperview().offset(-10)
        }
        
        lblInfant.snp.makeConstraints{ (make) in
            make.leading.equalToSuperview()
            make.top.equalToSuperview().offset(5)
        }
        
        lblInfantAge.snp.makeConstraints{ (make) in
            make.leading.equalToSuperview()
            make.top.equalTo(lblInfant.snp.bottom).offset(5)
        }
        
        infantCountView.snp.makeConstraints{ (make) in
            make.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(5)
            make.width.equalTo(150)
            make.height.equalTo(40)
            make.bottomMargin.equalToSuperview().offset(-10)
        }
        
        btnMinusInfant.snp.makeConstraints{ (make) in
            make.centerY.leading.height.equalToSuperview()
            make.width.equalTo(49)
        }
        
        lineView5.snp.makeConstraints{ (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(btnMinusInfant.snp.right)
            make.width.equalTo(1)
        }
        
        lblInfantCount.snp.makeConstraints{ (make) in
            make.left.equalTo(lineView5.snp.right)
            make.centerY.equalToSuperview()
            make.width.equalTo(50)
        }
        
        lineView6.snp.makeConstraints{ (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(lblInfantCount.snp.right)
            make.width.equalTo(1)
        }
        
        btnPlusInfant.snp.makeConstraints{ (make) in
            make.centerY.height.equalToSuperview()
            make.width.equalTo(49)
            make.height.equalTo(15)
            make.left.equalTo(lineView6.snp.right)
        }
        
//        btnCancel.snp.makeConstraints{ (make) in
//            make.width.equalToSuperview().multipliedBy(0.45)
//            make.height.equalTo(40)
//            make.leading.equalToSuperview().offset(10)
//            make.bottomMargin.equalToSuperview().offset(-10)
//        }
//
//        btnSelect.snp.makeConstraints{ (make) in
//            make.width.equalToSuperview().multipliedBy(0.45)
//            make.height.equalTo(40)
//            make.trailing.bottomMargin.equalToSuperview().offset(-10)
//        }
    }
    
    @objc func cancelClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func selectClicked(_ sender: UIButton) {
        passengerProtocol?.getPassengerCount(adult: adultCount, child: childCount, infant: infantCount)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func plusAdultClicked(_ sender: UIButton) {
        if adultCount + childCount + infantCount < 9 {
            adultCount += 1
            addPassenger(for: .Adult)
        }
    }
    
    @objc func minusAdultClicked(_ sender: UIButton) {
        if adultCount > 1 {
            adultCount -= 1
            removePassenger(for: .Adult)
        }
    }
    
    @objc func plusChildClicked(_ sender: UIButton) {
        if adultCount + childCount + infantCount < 9 {
            childCount += 1
            addPassenger(for: .Child)
        }
    }
    
    @objc func minusChildClicked(_ sender: UIButton) {
        if childCount > 0 {
            childCount -= 1
            removePassenger(for: .Child)
        }
    }
    
    @objc func plusInfantClicked(_ sender: UIButton) {
        if adultCount + childCount + infantCount < 9 {
            infantCount += 1
            addPassenger(for: .Infant)
        }
    }
    
    @objc func minusInfantClicked(_ sender: UIButton) {
        if infantCount > 0 {
            infantCount -= 1
            removePassenger(for: .Infant)
        }
    }
    
    private func addPassenger(for type: PassengerType) {
        var passenger = FlightPassenger()
        switch type {
        case .Adult: passenger.type = .Adult
        case .Child: passenger.type = .Child
        case .Infant: passenger.type = .Infant
        }
        FlightModel.shared().passengerList.append(passenger)
        changeCountHandler()
    }
    
    private func removePassenger(for type: PassengerType) {
        var index: Int?
        
        switch type {
        case .Adult: index = FlightModel.shared().passengerList.lastIndex(where: { $0.type == .Adult })
        case .Child: index = FlightModel.shared().passengerList.lastIndex(where: { $0.type == .Child })
        case .Infant: index = FlightModel.shared().passengerList.lastIndex(where: { $0.type == .Infant })
        }
        
        if let i = index {
            FlightModel.shared().passengerList.remove(at: i)
        }
        changeCountHandler()
    }
}
