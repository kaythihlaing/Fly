//
//  EditSearchFlightViewController.swift
//  Flymya
//
//  Created by Kay Kay on 6/19/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

class EditFlightSearchViewController: UIViewController {
    
    lazy var naviBgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.primaryBlue
        return view
    }()
    
    lazy private var lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.text = "Edit New Flights"
        lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        return lbl
    }()
    
    lazy private var btnClose: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Cancel"), for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnClose), for: .touchUpInside)
        return btn
    }()
    
    lazy var bgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        return view
    }()
    
    lazy var citizenView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let lblCitizen: UILabel = {
        let label = UILabel()
        label.text = "Myanmar Citizen"
        label.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        label.textColor = UIColor.textBlack
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    let yesnoView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let btnYes: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "Bullet Check White 810"), for: .normal)
        button.setTitle(" Yes", for: .normal)
        button.setTitleColor(UIColor.textBlack, for: .normal)
        button.titleLabel?.font =  UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(yesClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    let btnNo: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "Bullet Check Blue 811"), for: .normal)
        button.setTitle(" No", for: .normal)
        button.setTitleColor(UIColor.textBlack, for: .normal)
        button.titleLabel?.font =  UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(noClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var stackView: UIStackView = {
       let sView = UIStackView()
        sView.translatesAutoresizingMaskIntoConstraints = false
        sView.axis = .vertical
        sView.alignment = .fill // .leading .firstBaseline .center .trailing .lastBaseline
        sView.distribution = .fillEqually // .fillEqually .fillProportionally .equalSpacing .equalCentering
        sView.spacing = 10
        return sView
    }()
    
    lazy var fromCityView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var ivFrom: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "Departure 2"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy private var tfFromCity: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "From"
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)
        tf.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        tf.selectedTitleColor = UIColor.lightGray
        tf.selectedLineColor = UIColor.lightGray
        tf.textColor = UIColor.textBlack
        tf.titleFormatter = { $0 }
        if let city = FlightModel.shared().flightReq.depCity {
            if let cityName = city.cityName , let cityIATA = city.iATA{
                tf.text = "\(cityName) (\(cityIATA))"
            }
        }
        else {
            tf.text = ""
        }
        
        return tf
    }()
    
    lazy var toCityView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var ivTo: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "Arrival 2"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy private var tfToCity: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "To"
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)
        tf.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        tf.selectedTitleColor = UIColor.lightGray
        tf.selectedLineColor = UIColor.lightGray
        tf.textColor = UIColor.textBlack
        tf.titleFormatter = { $0 }
        
        if let city = FlightModel.shared().flightReq.retCity {
            if let cityName = city.cityName , let cityIATA = city.iATA{
                tf.text = "\(cityName) (\(cityIATA))"
            }
        }
        else {
            tf.text = ""
        }
        
        return tf
    }()
    
    //Switch airport
    lazy var btnSwitchAirport: UIButton = {
        let btn = UIButton()
        btn.setTitle("", for: .normal)
        btn.setImage(UIImage(named: "baseline_swap_horiz_black_90px"), for: .normal)
        btn.titleLabel?.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)
        btn.backgroundColor = UIColor.white
        btn.setTitleColor(UIColor.darkText, for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.cornerRadius = 18
        btn.layer.borderColor = UIColor.gray.cgColor
        btn.layer.borderWidth = 1
        btn.isUserInteractionEnabled = true
        
        return btn
    }()
    
    lazy var depDateView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var ivDepDate: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "Date"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy private var tfDepDate: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "Departure date"
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)
        tf.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        tf.selectedTitleColor = UIColor.lightGray
        tf.selectedLineColor = UIColor.lightGray
        tf.textColor = UIColor.textBlack
        tf.titleFormatter = { $0 }
        tf.text = "\(FlightModel.shared().flightReq.depDate!.simpleDate)"
        
        return tf
    }()
    
    lazy var retDateView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var ivRetDate: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "Date"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy private var tfRetDate: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "Return date"
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)
        tf.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        tf.selectedTitleColor = UIColor.lightGray
        tf.selectedLineColor = UIColor.lightGray
        tf.textColor = UIColor.textBlack
        tf.titleFormatter = { $0 }
        tf.text = "\(FlightModel.shared().flightReq.retDate!.simpleDate)"
        
        return tf
    }()
    
    lazy var passView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var ivPass: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "profile"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy private var tfPassenger: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "Passenger types"
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)
        tf.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        tf.selectedTitleColor = UIColor.lightGray
        tf.selectedLineColor = UIColor.lightGray
        tf.textColor = UIColor.textBlack
        tf.titleFormatter = { $0 }
        tf.text = ""
        
        return tf
    }()
    
    lazy var btnSearch: UIButton = {
        let button = UIButton()
        button.setTitle("Search Flights", for: .normal)
        button.titleLabel?.font =  UIFont(name: AppConstants.Font.Roboto.Medium, size: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.primaryBlue
        button.layer.cornerRadius = 5
        return button
    }()
    
    private var depDate: Date? = FlightModel.shared().flightReq.depDate {
        didSet {
            if let date = depDate {
                self.tfDepDate.text = date.simpleDate
            }
            else {
                depDate = oldValue
                self.tfDepDate.text = depDate?.simpleDate
            }
        }
    }
    
    private var adultCount: Int = FlightModel.shared().getAdultPassengerCount() {
        didSet {
            tfPassenger.text = "\(adultCount) Adult, \(childCount) Child, \(infantCount) Infant"
        }
    }
    
    private var childCount: Int = FlightModel.shared().getChildPassengerCount() {
        didSet {
            tfPassenger.text = "\(adultCount) Adult, \(childCount) Child, \(infantCount) Infant"
        }
    }
    
    private var infantCount: Int = FlightModel.shared().getInfantPassengerCount() {
        didSet {
            tfPassenger.text = "\(adultCount) Adult, \(childCount) Child, \(infantCount) Infant"
        }
    }
    
    private var retDate: Date? = FlightModel.shared().flightReq.retDate {
        didSet {
            if let date = retDate {
                if(retDate! < depDate!) {
                    retDate = Calendar.current.date(byAdding: .day, value: 1, to: depDate!)
                    self.tfRetDate.text = retDate?.simpleDate
                }
                else {
                    self.tfRetDate.text = date.simpleDate
                }
            }
            else {
                retDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
                self.tfRetDate.text = retDate?.simpleDate
            }
        }
    }
    
    lazy var nationality = FlightModel.shared().flightReq.nationality
    var citizenFlag = false {
        didSet {
            if(citizenFlag) {
                nationality = "Local"
            }
            else {
                nationality = "Foreigner"
            }
        }
    }
    
    private var depCity: CityResponse? = FlightModel.shared().flightReq.depCity {
        didSet {
            if let city = depCity {
                if let cityName = city.cityName , let cityIATA = city.iATA{
                    tfFromCity.text = "\(cityName) (\(cityIATA))"
                }
            }
            else {
                tfFromCity.text = ""
            }
        }
    }
    
    private var retCity: CityResponse? = FlightModel.shared().flightReq.retCity {
        didSet {
            if let city = retCity {
                if let cityName = city.cityName , let cityIATA = city.iATA{
                    tfToCity.text = "\(cityName) (\(cityIATA))"
                }
            }
            else {
                tfToCity.text = ""
            }
        }
    }
    
    private var editSearchProtocol: EditSearchProtocol?
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.setNeedsStatusBarAppearanceUpdate()
        
        setPassengerCount()
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        setupUI()
        setListener()
        
        if nationality == "Local" {
            btnYes.setImage(#imageLiteral(resourceName: "Bullet Check Blue 811"), for: .normal)
            btnNo.setImage(#imageLiteral(resourceName: "Bullet Check White 810"), for: .normal)
            citizenFlag = true
        } else {
            btnNo.setImage(#imageLiteral(resourceName: "Bullet Check Blue 811"), for: .normal)
            btnYes.setImage(#imageLiteral(resourceName: "Bullet Check White 810"), for: .normal)
            citizenFlag = false
        }
    }
    
    private func setListener() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapBtnClose))
        view.addGestureRecognizer(tap)
        
        let passTap = UITapGestureRecognizer(target: self, action: #selector(self.clickPassenger(_:)))
        tfPassenger.isUserInteractionEnabled = true
        tfPassenger.addGestureRecognizer(passTap)
        
        let fromTap = UITapGestureRecognizer(target: self, action: #selector(self.chooseDepCity(_:)))
        tfFromCity.isUserInteractionEnabled = true
        tfFromCity.addGestureRecognizer(fromTap)
        
        let toTap = UITapGestureRecognizer(target: self, action: #selector(self.chooseRetCity(_:)))
        tfToCity.isUserInteractionEnabled = true
        tfToCity.addGestureRecognizer(toTap)

        let fromDateTap = UITapGestureRecognizer(target: self, action: #selector(self.chooseFromDate))
        tfDepDate.isUserInteractionEnabled = true
        tfDepDate.addGestureRecognizer(fromDateTap)
        
        let toDateTap = UITapGestureRecognizer(target: self, action: #selector(self.chooseToDate))
        tfRetDate.isUserInteractionEnabled = true
        tfRetDate.addGestureRecognizer(toDateTap)
        
        btnSearch.addTarget(self, action: #selector(searchClicked(_:)), for: .touchUpInside)
        btnSwitchAirport.addTarget(self, action: #selector(didTapSwitchAirport), for: .touchUpInside)
    }
    
    private func setupUI() {
        
        view.addSubview(naviBgView)
        naviBgView.addSubview(lblTitle)
        naviBgView.addSubview(btnClose)
        
        view.addSubview(bgView)
        
        bgView.addSubview(citizenView)
        citizenView.addSubview(lblCitizen)
        citizenView.addSubview(yesnoView)
        yesnoView.addSubview(btnYes)
        yesnoView.addSubview(btnNo)
        
        bgView.addSubview(stackView)
        stackView.addArrangedSubview(fromCityView)
        fromCityView.addSubview(ivFrom)
        fromCityView.addSubview(tfFromCity)
        
        stackView.addArrangedSubview(toCityView)
        toCityView.addSubview(ivTo)
        toCityView.addSubview(tfToCity)
        
        stackView.addArrangedSubview(depDateView)
        depDateView.addSubview(ivDepDate)
        depDateView.addSubview(tfDepDate)
        
        bgView.addSubview(btnSwitchAirport)
        
        //stackView.addArrangedSubview(retDateView)
        //retDateView.addSubview(ivRetDate)
        //retDateView.addSubview(tfRetDate)
        
        stackView.addArrangedSubview(passView)
        passView.addSubview(ivPass)
        passView.addSubview(tfPassenger)
        
        bgView.addSubview(btnSearch)
        
        addConstraints()
        
    }
    
    func addConstraints() {
        
        //navigation bar
        naviBgView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(statusBarHeight + 44)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.topMargin).offset(8)
            make.centerX.equalToSuperview()
        }
        
        btnClose.snp.makeConstraints { (make) in
            make.centerY.equalTo(lblTitle.snp.centerY)
            make.right.equalToSuperview().offset(-8)
            make.width.height.equalTo(32)
        }
        
        //sub view
        bgView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(350)//(400)
            make.top.equalTo(naviBgView.snp.bottom)
        }
        
        citizenView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview().offset(10)
            make.height.equalTo(40)
        }
        
        lblCitizen.snp.makeConstraints { (make) in
            make.leading.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.35)
        }
        
        yesnoView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.35)
            make.left.equalTo(lblCitizen.snp.right)
        }
        
        btnYes.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.5)
            make.left.equalToSuperview()
        }
        
        btnNo.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.5)
            make.right.equalToSuperview()
        }
        
        stackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.top.equalTo(citizenView.snp.bottom)
            make.height.equalToSuperview().multipliedBy(0.6)//(0.7)
        }
        
        ivFrom.snp.makeConstraints { (make) in
            make.leading.centerY.equalToSuperview()
            make.width.equalTo(20)
            make.height.equalTo(12)
        }
        
        tfFromCity.snp.makeConstraints { (make) in
            make.left.equalTo(ivFrom.snp.right).offset(10)
            make.top.trailing.equalToSuperview()
        }
        
        ivTo.snp.makeConstraints { (make) in
            make.leading.centerY.equalToSuperview()
            make.width.equalTo(20)
            make.height.equalTo(12)
        }
        
        tfToCity.snp.makeConstraints { (make) in
            make.left.equalTo(ivTo.snp.right).offset(10)
            make.top.trailing.equalToSuperview()
        }
        
        btnSwitchAirport.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.centerY.equalTo(tfFromCity.snp.bottom).offset(0)
            make.width.height.equalTo(36)
        }
        
        ivDepDate.snp.makeConstraints { (make) in
            make.leading.centerY.equalToSuperview()
            make.width.equalTo(20)
            make.height.equalTo(20)
        }
        
        tfDepDate.snp.makeConstraints { (make) in
            make.left.equalTo(ivDepDate.snp.right).offset(10)
            make.top.trailing.equalToSuperview()
        }
        
        
//        ivRetDate.snp.makeConstraints { (make) in
//            make.leading.centerY.equalToSuperview()
//            make.width.equalTo(20)
//            make.height.equalTo(20)
//        }
//
//        tfRetDate.snp.makeConstraints { (make) in
//            make.left.equalTo(ivRetDate.snp.right).offset(10)
//            make.top.trailing.equalToSuperview()
//        }
        
        
        ivPass.snp.makeConstraints { (make) in
            make.leading.centerY.equalToSuperview()
            make.width.equalTo(20)
            make.height.equalTo(20)
        }
        
        tfPassenger.snp.makeConstraints { (make) in
            make.left.equalTo(ivPass.snp.right).offset(10)
            make.top.trailing.equalToSuperview()
        }
        
        btnSearch.snp.makeConstraints { (make) in
            if UIDevice.current.userInterfaceIdiom == .pad {
                make.width.equalToSuperview().multipliedBy(0.6)
            }
            else {
                make.width.equalToSuperview().multipliedBy(0.95)
            }
            make.height.equalTo(40)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-10)
        }
        
    }
    
    func setProtocol(editSearch: EditSearchProtocol) {
        self.editSearchProtocol = editSearch
    }
    
    private func setPassengerCount() {
        tfPassenger.text = FlightModel.shared().getPassengerCountText()
    }
    
    @objc private func didTapBtnClose() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func yesClicked(_ sender: UIButton) {
        btnYes.setImage(#imageLiteral(resourceName: "Bullet Check Blue 811"), for: .normal)
        btnNo.setImage(#imageLiteral(resourceName: "Bullet Check White 810"), for: .normal)
        citizenFlag = true
    }
    
    @objc func noClicked(_ sender: UIButton) {
        btnNo.setImage(#imageLiteral(resourceName: "Bullet Check Blue 811"), for: .normal)
        btnYes.setImage(#imageLiteral(resourceName: "Bullet Check White 810"), for: .normal)
        citizenFlag = false
    }
    
    @objc func clickPassenger(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.StoryboardID.EditPassFlightVC) as! EditCustomerViewController
        vc.setDelegate(passCount: self)
        vc.changeCountHandler = {
            self.setPassengerCount()
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func chooseDepCity(_ sender: Any) {
        print("choose dep city")
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "DestinationSearchViewController") as! DestinationSearchViewController
        vc.setDestinationType(type: .Departure)
        vc.setDelegate(dep: self, ret: self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func chooseRetCity(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "DestinationSearchViewController") as! DestinationSearchViewController
        vc.setDestinationType(type: .Return)
        vc.setDelegate(dep: self, ret: self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func chooseFromDate(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
//        if(returnFlag) {
            vc.setTripType(type: .Departure, fromDate: depDate, toDate: retDate)
//        }
//        else {
//            vc.setTripType(type: .Departure, fromDate: depDate, toDate: retDate)
        //}
        vc.setProtocol(calendar: self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func chooseToDate(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
        vc.setTripType(type: .Return, fromDate: depDate, toDate: retDate)
        vc.setProtocol(calendar: self)
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func didTapSwitchAirport() {
        let tempAirport1 = depCity
        let tempAirport2 = retCity
        depCity = tempAirport2
        retCity = tempAirport1
    }
    
    @objc func searchClicked(_ sender: UIButton) {
        
        NetworkManager.shared().cancelAllRequest()
        
        if(tfFromCity.text == "") {
            tfFromCity.errorMessage = "Departure City or Airpot is required."
        }
        else if(tfFromCity.text == "") {
            tfToCity.errorMessage = "Arrival City or Airpot is required."
        }
        else if(tfDepDate.text == "") {
            tfFromCity.errorMessage = "Departure Date is required."
        }
//        else if(tfFromCity.text == "") {
//            tfFromCity.errorMessage = "Return Date is required."
//        }
        else {
            let req = FlightListReq.init(depCity: depCity!, retCity: retCity!,depDate: depDate!, retDate: retDate!, nationality: nationality, tripType: FlightModel.shared().flightReq.tripType)
            FlightModel.shared().flightReq = req
            editSearchProtocol?.editSearch()
            dismiss(animated: true, completion: nil)
        }
    }
    
}

extension EditFlightSearchViewController : calProtocol, DepCityProtocol, RetCityProtocol, PassengerCountProtocol {
    
    func getTravelDate(dep: Date?, ret: Date?) {
        depDate = dep
        retDate = ret
    }
    
    func getDepCity(for city: CityResponse) {
        depCity = city
        
    }
    
    func getRetCity(for city: CityResponse) {
        retCity = city
    }
    
    func getPassengerCount(adult: Int, child: Int, infant: Int) {
        adultCount = adult
        childCount = child
        infantCount = infant
    }
    
}
