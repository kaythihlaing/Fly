//
//  FlightListViewController.swift
//  Flymya
//
//  Created by Kay Kay on 6/18/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import Alamofire

class FlightListViewController: UIViewController {
    
    var parameters: Parameters!
    
    lazy var naviBgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.primaryBlue
        return view
    }()
    
    lazy private var btnBack: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Left Arrow 1158"), for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnClose), for: .touchUpInside)
        return btn
    }()
    
    lazy var lblFromCity: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        if let city = FlightModel.shared().flightReq.depCity {
            if let cityName = city.cityName , let cityIATA = city.iATA{
                lbl.text = "\(cityName) (\(cityIATA))"
            }
        }
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var ivArrow: UIImageView = {
        let imageview = UIImageView()
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.image = #imageLiteral(resourceName: "right-arrow")
        return imageview
    }()
    
    lazy var lblToCity: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        if let city = FlightModel.shared().flightReq.retCity {
            if let cityName = city.cityName , let cityIATA = city.iATA{
                lbl.text = "\(cityName) (\(cityIATA))"
            }
        }
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblDate: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 12)
        lbl.sizeToFit()
        return lbl
    }()
    
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.register(FlightListTableViewCell.self, forCellReuseIdentifier: "FlightListTableViewCell")
        tableView.rowHeight = 150
        return tableView
    }()
    
    lazy var btnEdit: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Edit Search", for: .normal)
        button.setTitleColor(UIColor().HexToColor(hexString: "#231F20"), for: .normal)
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.primaryBlue.cgColor
        return button
    }()
    
    //MARK: - Attributes
    
    let bulletChar: String = " ● "
    lazy var flightList: [Flights] = []
    
    private var depCity: CityResponse? = FlightModel.shared().flightReq.depCity {
        didSet {
            if let city = depCity {
                if let cityName = city.cityName , let cityIATA = city.iATA{
                    lblFromCity.text = "\(cityName) (\(cityIATA))"
                }
                
            }
            else {
                if let city = FlightModel.shared().flightReq.depCity {
                    if let cityName = city.cityName , let cityIATA = city.iATA{
                        lblFromCity.text = "\(cityName) (\(cityIATA))"
                    }
                    
                }
            }
        }
    }
    
    private var retCity: CityResponse? = FlightModel.shared().flightReq.retCity {
        didSet {
            if let city = retCity {
                if let cityName = city.cityName , let cityIATA = city.iATA{
                    lblToCity.text = "\(cityName) (\(cityIATA))"
                }
            }
            else {
                if let city = FlightModel.shared().flightReq.retCity {
                    if let cityName = city.cityName , let cityIATA = city.iATA{
                        lblToCity.text = "\(cityName) (\(cityIATA))"
                    }
                    
                }
            }
        }
    }
    
    lazy var showErrorMessage = false
    
    var isNeedReload = false
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.setNeedsStatusBarAppearanceUpdate()
        
        lblDate.text = "\(FlightModel.shared().flightReq.depDate!.day),\(FlightModel.shared().flightReq.depDate!.simpleDate)\(bulletChar)\(FlightModel.shared().getPassengerCountText())\(bulletChar)\(FlightModel.shared().flightReq.tripType)"
        
        if isNeedReload {
            configureAPI()
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        setupUI()
        
        if !isNeedReload {
            configureAPI()
        }
        
        btnEdit.addTarget(self, action: #selector(clickEdit(_:)), for: .touchUpInside)
    }
    
    public func configureAPI() {
        
        self.showLoadingIndicator(message: "Searching Flights...")
        let req = FlightModel.shared().flightReq
        
        parameters = [
            "Adults": 1,
            "ArrivalCity": req.retCity?.iATA ?? "",
            "Children": 0,
            "Class": "Economy",
            "DepartureCity": req.depCity?.iATA ?? "",
            "DepartureDate": req.depDate!.apiDate,
            "Infants": 0,
            "Nationality": req.nationality,
            "ReturnDate": req.retDate!.apiDate,
            "SortBy": "Price",
            "TripType": req.tripType,
            "ampm": ""
        ]
        print(parameters)
        
        FlightModel.shared().flightList.removeAll()
        self.flightList.removeAll()
        self.showErrorMessage = false
        self.tableView.reloadData()
        
        var count = 0
        FlightModel.shared().getFlightKBZList(parameters: parameters, success: {
            self.sortFlightList()
        }) { (error) in
            count += 1
            self.showError(error, count: count)
        }

        FlightModel.shared().getFlightYGNList(parameters: parameters, success: {
            self.sortFlightList()
        }) { (error) in
            count += 1
            self.showError(error, count: count)
        }

        FlightModel.shared().getFlightGMAList(parameters: parameters, success: {
            self.sortFlightList()
        }) { (error) in
            count += 1
            self.showError(error, count: count)
        }
        
        FlightModel.shared().getFlightMNAList(parameters: parameters, success: {
            print("=> Flight List - \(FlightModel.shared().flightList.count)")
            self.sortFlightList()
        }) { (error) in
            count += 1
            self.showError(error, count: count)
        }
    }
    
    private func showError(_ error: String, count: Int) {
        print("ERROR => \(error)")
        if(count > 3) {
            self.showErrorMessage = true
            self.tableView.reloadData()
            self.hideLoadingIndicator()
        }
    }
    
    private func sortFlightList() {
        self.flightList = FlightModel.shared().flightList
        self.flightList = self.flightList.sorted { t1, t2 in
            let period1 = t1.d_Time!.substring(from: (t1.d_Time!.index((t1.d_Time?.endIndex)!, offsetBy: -2)))
            let period2 = t2.d_Time!.substring(from: (t2.d_Time!.index((t2.d_Time?.endIndex)!, offsetBy: -2)))
            if (period1 == period2) {
                return (t1.d_Time as! String) < (t2.d_Time as! String)
            }
            return period1 < period2
        }
        FlightModel.shared().flightList = self.flightList
        self.showErrorMessage = true
        self.tableView.reloadData()
        self.hideLoadingIndicator()
    }
    
    private func setupUI() {
        
        view.addSubview(naviBgView)
        naviBgView.addSubview(btnBack)
        naviBgView.addSubview(lblFromCity)
        naviBgView.addSubview(ivArrow)
        naviBgView.addSubview(lblToCity)
        naviBgView.addSubview(lblDate)
        
        view.addSubview(tableView)
        view.addSubview(btnEdit)
        
        addConstraints()
        
    }
    
    func addConstraints() {
        
        naviBgView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(statusBarHeight + 44)
        }
        
        btnBack.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.topMargin).offset(8)
            make.left.equalToSuperview().offset(8)
            make.width.height.equalTo(32)
        }
        
        lblFromCity.snp.makeConstraints { (make) in
            make.left.equalTo(btnBack.snp.right).offset(8)
            make.topMargin.equalToSuperview().offset(5)
            make.height.equalTo(20)
            
        }
        
        ivArrow.snp.makeConstraints { (make) in
            make.left.equalTo(lblFromCity.snp.right).offset(3)
            make.centerY.equalTo(lblFromCity)
        }
        
        lblToCity.snp.makeConstraints { (make) in
            make.left.equalTo(ivArrow.snp.right).offset(3)
            make.centerY.equalTo(lblFromCity)
            make.width.equalToSuperview()
            make.height.equalTo(20)
            
        }
        
        lblDate.snp.makeConstraints { (make) in
            make.left.equalTo(btnBack.snp.right).offset(8)
            make.top.equalTo(lblFromCity.snp.bottom)
            make.height.equalTo(15)
            
        }
        
        tableView.snp.makeConstraints { (make) in
            make.leftMargin.rightMargin.equalToSuperview()
            make.top.equalTo(naviBgView.snp.bottom).offset(5)
            make.bottom.equalTo(btnEdit.snp.top).offset(-5)
        }
        
        btnEdit.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(50)
            make.bottom.equalTo(view.snp.bottomMargin)
        }
        
    }
    
    @objc func clickEdit(_ sender: UIButton) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.StoryboardID.EditSearchFlightVC) as! EditFlightSearchViewController
        vc.setProtocol(editSearch: self)
        self.present(vc, animated: false, completion: nil)
    }
    
    @objc private func didTapBtnClose() {
        navigationController?.popViewController(animated: true)
    }
    
    private func goToFlightDetails(index: Int) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.StoryboardID.FlightDetailsVC) as! FlightDetailsTabViewController
        vc.selectedFlightIndex = index
        vc.detailChooseProtocol = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    private func goToPersonalInfo() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.StoryboardID.PersonalInfoVC) as! CustomerViewController
        vc.flightListReloadHandler = { flag in
            self.isNeedReload = flag
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension FlightListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if flightList.count == 0 {
            if showErrorMessage {
                EmptyViewHelper.TableViewEmptyMessage(message: "There is no flight for these day.", viewController: self, tableview: tableView)
            } else {
                EmptyViewHelper.TableViewEmptyMessage(message: "", viewController: self, tableview: tableView)
            }
        } else {
            EmptyViewHelper.TableViewEmptyMessage(message: "", viewController: self, tableview: tableView)
        }
        
        
        return flightList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FlightListTableViewCell", for: indexPath) as? FlightListTableViewCell {
            
            cell.lblFlightName.text = flightList[indexPath.row].carrierName
            
            let temp = flightList[indexPath.row].fare!.sorted (by: {Double($0.totalPrice ?? "")! < Double($1.totalPrice ?? "")!})
            
            cell.lblPrice.text = "\(temp.first?.currency ?? "") \(temp.first?.totalPrice ?? "")"
            cell.lblFromTime.text = flightList[indexPath.row].d_Time
            cell.lblToTime.text = flightList[indexPath.row].a_Time

            cell.didTapBtnDetailHandler = {
                FlightModel.shared().chooseFlight = self.flightList[indexPath.row]
                FlightModel.shared().chooseFare = temp.first ?? Fare()
                self.goToFlightDetails(index: indexPath.row)
            }
            
            cell.didTapBtnChooseHandler = {
                FlightModel.shared().chooseFlight = self.flightList[indexPath.row]
                FlightModel.shared().chooseFare = temp.first ?? Fare()
                self.goToPersonalInfo()
            }
            return cell
        }
        return UITableViewCell()
    }

}
extension FlightListViewController: UITableViewDelegate {
   

}

extension FlightListViewController: EditSearchProtocol {
    func editSearch() {
        let req = FlightModel.shared().flightReq
        lblDate.text = "\(req.depDate!.day),\(req.depDate!.simpleDate)\(bulletChar)1 Adult\(bulletChar)\(req.tripType)"
        depCity = req.depCity
        retCity = req.retCity
        
        self.configureAPI()
    }
    
}

extension FlightListViewController: DetailChooseProtocol {
    func chooseClick() {
        goToPersonalInfo()
    }
}
