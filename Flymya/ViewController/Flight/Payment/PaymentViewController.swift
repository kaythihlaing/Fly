//
//  PaymentViewController.swift
//  Flymya
//
//  Created by Kay Kay on 6/24/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Stripe

class PaymentViewController: UIViewController, STPPaymentCardTextFieldDelegate {
    
    lazy var naviBgView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.primaryBlue
        return view
    }()
    
    lazy private var btnBack: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Left Arrow 1158"), for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnClose), for: .touchUpInside)
        btn.isHidden = true
        return btn
    }()
    
    lazy var lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white
        lbl.text = "Payment"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        return scroll
    }()
    
    lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var lblPaymentMethod: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = "Payment Method"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        return lbl
    }()
    
    lazy private var tfPaymentMethod: UITextField = {
        let tf = UITextField()
        tf.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        tf.textColor = UIColor.textBlack
        tf.tintColor = .clear
        tf.layer.borderColor = UIColor.lightGray.cgColor
        tf.layer.borderWidth = 1
        tf.layer.cornerRadius = 5
        tf.borderStyle = .roundedRect
        return tf
    }()
    
    lazy private var ivChevron: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Down")
        iv.contentMode = UIView.ContentMode.scaleAspectFit
        return iv
    }()
    
    //MARK: - Credit Card View
    lazy var creditCardView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.isHidden = true
        return view
    }()
    
    lazy var lblCardNumber: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = "Card Number"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        return lbl
    }()

    lazy private var tfCardNo: STPPaymentCardTextField = {
        let tf = STPPaymentCardTextField()
        tf.numberPlaceholder = "1234 1234 1234 1234"
        tf.delegate = self
        return tf
    }()
    
    lazy var lblCardHolderName: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = "Cardholder Name"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        return lbl
    }()
    
    lazy private var tfCardHolder: UITextField = {
        let tf = UITextField()
        tf.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        tf.textColor = UIColor.textBlack
        tf.placeholder = "Cardholder Name"
        tf.layer.borderColor = UIColor.lightGray.cgColor
        tf.layer.borderWidth = 1
        tf.layer.cornerRadius = 5
        tf.borderStyle = .roundedRect
        tf.textContentType = .name
        return tf
    }()
    
    //MARK: - Offline Payment View
    lazy var offlinePaymentView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.isHidden = false
        return view
    }()
    
    lazy var ivPaymentIcon: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "Cash On Delivery"))
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        return imageView
    }()
    
    lazy var lblPaymentDescription: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.black
        lbl.text = "You will receive an invoice and guideline by email \n to finalized your payment with PayPal."
        lbl.numberOfLines = 0
        lbl.textAlignment = .center
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 16)
        lbl.sizeToFit()
        return lbl
    }()
    
    //MARK: -
    lazy var lblView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor().HexToColor(hexString: "#F5F6F9")
        return view
    }()
    
    lazy var lblTerms: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.textBlack
        let attributedWithTextColor: NSAttributedString = "By clicking the Pay Now, you agree to\nFlymya's Terms and conditions and Privacy Policy".attributedStringWithColor(["Flymya's Terms and conditions", "Privacy Policy"], color: UIColor.primaryBlue)
        lbl.attributedText = attributedWithTextColor
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        lbl.numberOfLines = 3
        lbl.textAlignment = .center
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var flightDetailView: FlightDetailsExpandableViewPod = {
        let vp = FlightDetailsExpandableViewPod()
        vp.expandableHandler = { isExpanded in
            self.isFlightDetailsExpended = isExpanded
            self.expandViewController()
        }
        return vp
    }()
    
    lazy var btnPay: UIButton = {
        let btn = UIButton()
        btn.setTitle("Pay Now", for: .normal)
        btn.titleLabel?.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        btn.backgroundColor = UIColor.primaryBlue
        btn.titleLabel?.textColor = UIColor.white
        btn.layer.cornerRadius = 5
        return btn
    }()
    
    //MARK: - Attributes
    private var isPaymentListShow = false
    
    private var isFlightDetailsExpended = false
    
    private var isSTPCardVaild = false
    
    let paymentMethodPicker = UIPickerView()
    
    //MARK: - View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        setupUI()
        
        addListener()
        
        createPicker()
        
        FlightModel.shared().selectedPaymentMethod = FlightModel.shared().paymentMethodList.first
        self.tfPaymentMethod.text = FlightModel.shared().selectedPaymentMethod?.displayName
        self.changePaymentMethod()
    }
    
    func addListener() {
        dismissKeyboardOnTapView()
        
        btnPay.addTarget(self, action: #selector(payNowClicked(_:)), for: .touchUpInside)
        
        tfCardNo.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfCardHolder.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        isSTPCardVaild = textField.isValid
        print("isSTPCardVaild => \(isSTPCardVaild)")
        if isSTPCardVaild {
            btnPay.isEnabled = true
        } else {
            btnPay.isEnabled = false
        }
    }
    
    func getToken(success: @escaping (STPToken) -> ()) {
        let card = STPCardParams()
        card.number = tfCardNo.cardNumber
        card.expMonth = tfCardNo.expirationMonth
        card.expYear = tfCardNo.expirationYear
        card.cvc = tfCardNo.cvc
        
        STPAPIClient.shared().createToken(withCard: card) { (token, error) in
            
            if let error = error {
                print("=> \(error)")
            } else {
                if let token = token {
                    print("=> \(token)")
                    success(token)
                }
            }
            
        }
    }
    
    private func createPicker() {
        paymentMethodPicker.delegate = self
        tfPaymentMethod.inputView = paymentMethodPicker
    }
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        
//        if(tfCardNo.text != "") {
//            tfCardNo.errorMessage = ""
//        }
//        if(tfCardHolder.text != "") {
//            tfCardHolder.errorMessage = ""
//        }
        
    }
    
    @objc private func expandViewController() {
        UIView.animate(withDuration: 0.3, animations: {
            if self.isFlightDetailsExpended {
                self.flightDetailView.snp.updateConstraints { (make) in
                    make.height.equalTo(571)
                }
            } else {
                self.flightDetailView.snp.updateConstraints { (make) in
                    make.height.equalTo(571 - 213)
                }
                
            }
            self.view.layoutIfNeeded()
        }) { complete in
            self.scrollView.scrollToBottom()
        }
    }
    
    private func showConfirmAlert() {
        
        let titleFont = [NSAttributedString.Key.font: UIFont(name: "Roboto-Bold", size: 18.0)!]
        let titleAttrString = NSMutableAttributedString(string: "Are you sure to make this transaction?", attributes: titleFont)
        
        let messageFont = [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 15.0)!]
        let messageAttrString = NSMutableAttributedString(string: "", attributes: messageFont)
        
        let cancelButtonText = NSLocalizedString("Check Again", comment: "")
        let signupButtonText = NSLocalizedString("Yes, Confirm", comment: "")
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        let cancelAction = UIAlertAction(title: cancelButtonText, style: UIAlertAction.Style.cancel, handler: nil)
        let confirmAction = UIAlertAction(title: signupButtonText, style: UIAlertAction.Style.default, handler: { action in
            self.prepareDataForPayment()
        })
        alert.addAction(cancelAction)
        alert.addAction(confirmAction)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func payNowClicked(_ sender: UIButton) {
//        self.showConfirmAlert()
        self.prepareDataForPayment()
    }
    
    private func prepareDataForPayment() {
        guard let selectedPayment = FlightModel.shared().selectedPaymentMethod else {
            print("=> No Payment Selected!")
            return
        }
        
        guard let filghtResponse = FlightModel.shared().flightBookingResponse else {
            print("=> No Flight Response Data!")
            return
        }
        
        guard let bookingId = filghtResponse.bookingId else {
            print("=> Booking ID is nil!")
            return
        }
        
        self.showLoadingIndicator(message: "Processing your transaction...")
        
        let transactionId : String = String(bookingId)
        let cardHolderName : String = self.tfCardHolder.text ?? ""
        let amount : String = filghtResponse.total?.description ?? ""
        let currency : String = filghtResponse.currency ?? ""
        
        var meta = formDataInput(productType: "domestic", transactionId: transactionId)
        var input = ominipayGatewayInput(gateway: "", metadata: meta, description: "testing ios")
        
        if selectedPayment.gatewayName == .stripe {
            var valid = true
            if(!tfCardNo.isValid) {
                valid = false
            }
            
            if(tfCardHolder.text == "") {
                valid = false
            }
            
            if(valid) {
                self.getToken { (token) in
                    meta = formDataInput(productType: "domestic",
                                         transactionId: transactionId,
                                         cardholderName: cardHolderName)
                    
                    input = ominipayGatewayInput(gateway: "stripe",
                                                 amount: amount,
                                                 currency: currency,
                                                 token: token.tokenId,
                                                 metadata: meta,
                                                 description: "ORDER \(filghtResponse.fM_BookingId ?? "") via iOS")
                    
                    self.createPayment(meta: meta, input: input)
                }
            } else {
                print("=> Credit Card Not Valid")
                self.hideLoadingIndicator()
            }
        } else {
            meta = formDataInput(productType: "domestic", transactionId: transactionId)
            input = ominipayGatewayInput(gateway: selectedPayment.gatewayName.rawValue, metadata: meta, description: "ORDER \(filghtResponse.fM_BookingId ?? "")")
            
            self.createPayment(meta: meta, input: input)
        }
    }
    
    private func createPayment(meta: formDataInput, input: ominipayGatewayInput) {
        print("=> meta - \(meta)")
        print("=> input - \(input)")
        
        ApolloManager.shared.client.perform(mutation: PayGatewayMutationMutation(input: input)) { result in
            print("GraphQL =>")
            
            guard let data = try? result.get().data else {
                print("=> Payment Fail, No Data")
                self.hideLoadingIndicator()
                self.showAlertDialog(inputTitle: "Payment Fail!", inputMessage: "Your payment is seem liked something wrong. We'll contact to you soon.")
                return
            }
            
            let resultMap = data.resultMap["createPayment"] as? [String: Any]
            if let status = resultMap?["status"] as? Bool {
                if status {
                    print("=> Payment Success")
                    self.hideLoadingIndicator()
                    self.goToFlightInformation()
                } else {
                    if let message = resultMap?["message"] as? String {
                        print(message)
                        self.hideLoadingIndicator()
                        self.showAlertDialog(inputTitle: "Payment Fail!", inputMessage: message)
                    } else {
                        print("=> Payment Fail")
                        self.hideLoadingIndicator()
                        self.showAlertDialog(inputTitle: "Payment Fail!", inputMessage: "Your payment is seem liked something wrong. We'll contact to you soon.")
                    }
                    self.hideLoadingIndicator()
                    self.showAlertDialog(inputTitle: "Payment Fail!", inputMessage: "Your payment is seem liked something wrong. We'll contact to you soon.")
                }
            } else {
                print("=> Payment Fail")
                self.hideLoadingIndicator()
                self.showAlertDialog(inputTitle: "Payment Fail!", inputMessage: "Your payment is seem liked something wrong. We'll contact to you soon.")
            }
        }
    }
    
    private func changePaymentMethod() {
        let gateway = FlightModel.shared().selectedPaymentMethod?.gatewayName ?? PaymentGateWay.stripe
        switch gateway {
        case .stripe:
            showCreditCardView()
            
        case .paypal:
            showOfflinePaymentView()
            ivPaymentIcon.image = UIImage(named: "payment_paypal_logo")
            
        case .kbz_quick_pay:
            showOfflinePaymentView()
            ivPaymentIcon.image = UIImage(named: "payment_kbz_quickpay_logo")
            
        case .mobile_banking:
            showOfflinePaymentView()
            ivPaymentIcon.image = UIImage(named: "payment_mobile_banking_logo")
            
        case .myanmar123:
            showOfflinePaymentView()
            ivPaymentIcon.image = UIImage(named: "payment_123_logo")
            
        case .mpu:
            showOfflinePaymentView()
            ivPaymentIcon.image = UIImage(named: "payment_mpu")
            
        case .cod:
            showOfflinePaymentView()
            ivPaymentIcon.image = UIImage(named: "payment_cod_logo")
            
        default:
            showCreditCardView()
        }
    }
    
    private func showCreditCardView() {
        creditCardView.isHidden = false
        offlinePaymentView.isHidden = true
        
        if isSTPCardVaild {
            btnPay.isEnabled = true
        } else {
            btnPay.isEnabled = false
        }
    }
    
    private func showOfflinePaymentView() {
        creditCardView.isHidden = true
        offlinePaymentView.isHidden = false
        
        let paymentName = FlightModel.shared().selectedPaymentMethod?.displayName ?? "Credit Card"
        lblPaymentDescription.text = "You will receive an invoice and guideline by email to finalized your payment with \(paymentName)."
        
        btnPay.isEnabled = true
    }
    
    @objc private func didTapBtnClose() {
        navigationController?.popViewController(animated: true)
    }
    
    private func goToFlightInformation() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.StoryboardID.ConfirmVC) as! FlightConfirmationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func setupUI() {
        
        view.addSubview(naviBgView)
        naviBgView.addSubview(btnBack)
        naviBgView.addSubview(lblTitle)
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        contentView.addSubview(lblPaymentMethod)
        contentView.addSubview(tfPaymentMethod)
        contentView.addSubview(ivChevron)
        
        contentView.addSubview(creditCardView)
        creditCardView.addSubview(lblCardNumber)
        creditCardView.addSubview(tfCardNo)
        creditCardView.addSubview(lblCardHolderName)
        creditCardView.addSubview(tfCardHolder)
        
        contentView.addSubview(offlinePaymentView)
        offlinePaymentView.addSubview(ivPaymentIcon)
        offlinePaymentView.addSubview(lblPaymentDescription)
        
        contentView.addSubview(lblView)
        lblView.addSubview(lblTerms)
        
        contentView.addSubview(flightDetailView)
        
        contentView.addSubview(btnPay)
        
        addConstraints()
    }
    
    func addConstraints() {
        
        naviBgView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(statusBarHeight + 44)
        }
        
        btnBack.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.topMargin).offset(8)
            make.left.equalToSuperview().offset(8)
            make.width.height.equalTo(32)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(btnBack.snp.centerY)
            make.height.equalTo(20)
        }
        
        scrollView.snp.makeConstraints { (make) in
            make.left.right.bottom.width.equalToSuperview()
            make.top.equalTo(naviBgView.snp.bottom)
        }
        
        contentView.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(scrollView)
            make.left.right.equalTo(view)
        }
        
        lblPaymentMethod.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(24)
            make.top.equalToSuperview().offset(16)
        }
        
        tfPaymentMethod.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(50)
            make.top.equalTo(lblPaymentMethod.snp.bottom).offset(8)
        }
        
        ivChevron.snp.makeConstraints { (make) in
            make.right.equalTo(tfPaymentMethod.snp.right).offset(-16)
            make.centerY.equalTo(tfPaymentMethod)
            make.width.height.equalTo(16)
        }
        
        //Credit Card View
        creditCardView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(tfPaymentMethod.snp.bottom).offset(16)
        }
        
        lblCardNumber.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(24)
        }
        
        tfCardNo.snp.makeConstraints { (make) in
            make.top.equalTo(lblCardNumber.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(50)
        }
        
        lblCardHolderName.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(24)
            make.top.equalTo(tfCardNo.snp.bottom).offset(16)
        }
        
        tfCardHolder.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(50)
            make.top.equalTo(lblCardHolderName.snp.bottom).offset(8)
            make.bottom.equalToSuperview().offset(-16)
        }
        
        //Offline Payment
        offlinePaymentView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(tfPaymentMethod.snp.bottom).offset(16)
            make.height.equalTo(creditCardView)
        }
        
        ivPaymentIcon.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.centerX.equalToSuperview()
            make.height.equalTo(100)
            make.width.equalToSuperview().multipliedBy(0.50)
        }
        
        lblPaymentDescription.snp.makeConstraints { (make) in
            make.top.equalTo(ivPaymentIcon.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-16)
        }
        
        //T&C
        lblView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(70)
            make.top.equalTo(creditCardView.snp.bottom).offset(16)
        }
        
        lblTerms.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.centerY.equalToSuperview()
        }
        
        flightDetailView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(350)
            make.top.equalTo(lblView.snp.bottom)
        }
        
        btnPay.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(48)
            make.top.equalTo(flightDetailView.snp.bottom).offset(16)
            make.bottom.equalTo(contentView.snp.bottom).offset(-16)
        }
        
    }
}

extension PaymentViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return FlightModel.shared().paymentMethodList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return FlightModel.shared().paymentMethodList[row].displayName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        FlightModel.shared().selectedPaymentMethod = FlightModel.shared().paymentMethodList[row]
        self.tfPaymentMethod.text = FlightModel.shared().selectedPaymentMethod?.displayName
        self.changePaymentMethod()
    }
}
