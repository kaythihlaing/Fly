//
//  CustomerViewController.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/25/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Columbus
import ViewAnimator
import Alamofire

class CustomerViewController: UIViewController {
    
    //MARK: - View Components
    lazy private var navBarView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().HexToColor(hexString: AppConstants.Color.secondaryBlue)
        return view
    }()
    
    lazy private var btnBack: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Left Arrow 1158"), for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnClose), for: .touchUpInside)
        return btn
    }()
    
    lazy private var lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.text = "Customer Info"
        lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 16)
        return lbl
    }()
    
    lazy private var lblContactDetails: UILabel = {
        let lbl = UILabel()
        lbl.text = "Contact Details"
        lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        return lbl
    }()
    
    lazy private var scrollview: UIScrollView = {
        let view = UIScrollView()
        return view
    }()
    
    lazy private var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy private var tfFirstName: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "First Name"
        tf.textContentType = UITextContentType.givenName
        tf.lineColor = UIColor.gray
        tf.selectedLineColor = UIColor.black
        tf.selectedTitleColor = UIColor.black
        tf.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        tf.titleFont = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)!
        tf.titleFormatter = { $0 }
        tf.addTarget(self, action: #selector(textFieldValueChange(_:)), for: .editingChanged)
        return tf
    }()

    lazy private var tfLastName: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "Last Name"
        tf.textContentType = UITextContentType.familyName
        tf.lineColor = UIColor.gray
        tf.selectedLineColor = UIColor.black
        tf.selectedTitleColor = UIColor.black
        tf.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        tf.titleFont = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)!
        tf.titleFormatter = { $0 }
        tf.addTarget(self, action: #selector(textFieldValueChange(_:)), for: .editingChanged)
        return tf
    }()
    
    lazy private var btnCountryCode: UIButton = {
        let btn = UIButton()
        btn.setTitle("+95", for: .normal)
        btn.setImage(#imageLiteral(resourceName: "Myanmar Flag"), for: .normal)
        btn.setTitleColor(UIColor.black, for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnCountryCode), for: .touchUpInside)
        return btn
    }()
    
    lazy private var tfMobileNumber: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "Mobile Number"
        tf.keyboardType = UIKeyboardType.phonePad
        tf.textContentType = UITextContentType.telephoneNumber
        tf.lineColor = UIColor.gray
        tf.selectedLineColor = UIColor.black
        tf.selectedTitleColor = UIColor.black
        tf.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        tf.titleFont = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)!
        tf.titleFormatter = { $0 }
        tf.addTarget(self, action: #selector(textFieldValueChange(_:)), for: .editingChanged)
        return tf
    }()
    
    lazy private var tfEmail: SkyFloatingLabelTextField = {
        let tf = SkyFloatingLabelTextField()
        tf.placeholder = "Email"
        tf.keyboardType = UIKeyboardType.emailAddress
        tf.textContentType = UITextContentType.emailAddress
        tf.lineColor = UIColor.gray
        tf.selectedLineColor = UIColor.black
        tf.selectedTitleColor = UIColor.black
        tf.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        tf.placeholderFont = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        tf.titleFont = UIFont(name: AppConstants.Font.Roboto.Medium, size: 12)!
        tf.titleFormatter = { $0 }
        tf.addTarget(self, action: #selector(textFieldValueChange(_:)), for: .editingChanged)
        return tf
    }()
    
    lazy private var passengerDetailsView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().HexToColor(hexString: "#F5F6F9")
        return view
    }()
    
    lazy private var lblPassengerDetails: UILabel = {
        let lbl = UILabel()
        lbl.text = "Passenger Details"
        lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        return lbl
    }()
    
    lazy private var lblPassengerDetailsRequired: UILabel = {
        let lbl = UILabel()
        lbl.text = "*"
        lbl.textColor = UIColor.red
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 17)
        return lbl
    }()
    
    lazy private var passengerDetailsTableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.register(PassengerDetailsTableViewCell.self, forCellReuseIdentifier: "PassengerDetailsTableViewCell")
        return tableView
    }()
    
    lazy private var paymentMethodView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy private var lblPaymentMethod: UILabel = {
        let lbl = UILabel()
        lbl.text = "Payment Method"
        lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        return lbl
    }()
    
    lazy private var tfPaymentMethod: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Payment Method"
        tf.borderStyle = UITextField.BorderStyle.roundedRect
        tf.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        return tf
    }()
    
    lazy private var ivChevron: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Down")
        iv.contentMode = UIView.ContentMode.scaleAspectFit
        return iv
    }()
    
    lazy private var seperatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    lazy private var flightDetailsView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy private var flightDetailsViewPod: FlightDetailsExpandableViewPod = {
        let vp = FlightDetailsExpandableViewPod()
        vp.expandableHandler = { isExpanded in
            self.isFlightDetailsExpended = isExpanded
            self.expandViewController()
        }
        return vp
    }()
    
    lazy private var btnContinue: UIButton = {
        let btn = UIButton()
        btn.setTitle("Continue", for: .normal)
        btn.backgroundColor = UIColor.primaryBlue
        btn.layer.cornerRadius = 5
        btn.layer.masksToBounds = true
        btn.titleLabel?.font =  UIFont(name: AppConstants.Font.Roboto.Medium, size: 16)
        btn.addTarget(self, action: #selector(didTapBtnContinue), for: .touchUpInside)
        return btn
    }()
    
    lazy var coverView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.isHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapCoverView))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    lazy private var passengerEditViewPod: PassengerInfoEditViewPod = {
        let vp = PassengerInfoEditViewPod()
        vp.isHidden = true
        vp.btnSaveTapHandler = {
            self.hidePassengerEdit()
//            self.validateCheck()
            self.emptyPasseger.removeAll()
            for (index, passenger) in FlightModel.shared().passengerList.enumerated() {
                if passenger.firstName.isEmpty || passenger.lastName.isEmpty {
                    self.emptyPasseger.append(index)
                }
            }
            self.passengerDetailsTableView.reloadData()
        }
        vp.getContactDetailHandler = { () -> (ContactDetail) in
            return self.getContactDetail()
        }
        return vp
    }()
    
    private var isFlightDetailsExpended = false
    
    private var selectedCountry = "MM"
    
    private var emptyPasseger: [Int] = []
    
    private let paymentMethodPicker = UIPickerView()
    
    private var paymentMethodList: [paymentMethod] = []
    
    var flightListReloadHandler : (Bool) -> () = { _ in }
    
    //MARK: - Life Cycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNeedsStatusBarAppearanceUpdate()
        
        dismissKeyboardOnTapView()
        
        setupUI()
        
        FlightModel.shared().selectedPaymentMethod = paymentMethodList.first
        tfPaymentMethod.text = paymentMethodList.first?.displayName
        
        let flight = FlightModel.shared().chooseFlight
        if flight.carrierName == "Myanmar National Airlines" {
            checkFlight()
        } else {
            self.flightListReloadHandler(false)
        }
    }
    
    //MARK: - Private Functions
    /// Set up UI to view
    private func setupUI() {
        //Navigation Bar
        view.addSubview(navBarView)
        navBarView.addSubview(lblTitle)
        navBarView.addSubview(btnBack)
        
        view.addSubview(scrollview)
        scrollview.addSubview(contentView)
        
        contentView.addSubview(lblContactDetails)
        contentView.addSubview(tfFirstName)
        contentView.addSubview(tfLastName)
        contentView.addSubview(btnCountryCode)
        contentView.addSubview(tfMobileNumber)
        contentView.addSubview(tfEmail)
        
        contentView.addSubview(passengerDetailsView)
        passengerDetailsView.addSubview(lblPassengerDetails)
        passengerDetailsView.addSubview(lblPassengerDetailsRequired)
        passengerDetailsView.addSubview(passengerDetailsTableView)
        
        contentView.addSubview(flightDetailsView)
        flightDetailsView.addSubview(flightDetailsViewPod)
        
        contentView.addSubview(btnContinue)
        
        view.addSubview(coverView)
        view.addSubview(passengerEditViewPod)
        
        setConstraints()
    }
    
    /// Set up constraint to UI Components
    private func setConstraints() {
        //Navigation Bar
        navBarView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(statusBarHeight + 44)
        }
        
        btnBack.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.topMargin).offset(8)
            make.left.equalToSuperview().offset(8)
            make.width.height.equalTo(32)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.centerY.equalTo(btnBack.snp.centerY)
            make.centerX.equalToSuperview()
        }
        
        //Scroll View
        scrollview.snp.makeConstraints { (make) in
            make.top.equalTo(navBarView.snp.bottom).offset(0)
            make.left.right.bottom.equalToSuperview()
        }
        
        contentView.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(scrollview)
            make.left.right.equalTo(view)
        }
        
        //Components
        lblContactDetails.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        tfFirstName.snp.makeConstraints { (make) in
            make.top.equalTo(lblContactDetails.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(48)
        }
        
        tfLastName.snp.makeConstraints { (make) in
            make.top.equalTo(tfFirstName.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(48)
        }
        
        btnCountryCode.snp.makeConstraints { (make) in
            make.bottom.equalTo(tfMobileNumber.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(16)
            make.width.equalTo(72)
            make.height.equalTo(48)
        }
        
        tfMobileNumber.snp.makeConstraints { (make) in
            make.top.equalTo(tfLastName.snp.bottom).offset(8)
            make.left.equalTo(btnCountryCode.snp.right).offset(8)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(48)
        }
        
        tfEmail.snp.makeConstraints { (make) in
            make.top.equalTo(tfMobileNumber.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(48)
        }
        
        //Passenger Details
        passengerDetailsView.snp.makeConstraints { (make) in
            make.top.equalTo(tfEmail.snp.bottom).offset(16)
            make.left.right.equalToSuperview()
            make.bottom.equalTo(passengerDetailsTableView).offset(16)
        }
        
        lblPassengerDetails.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(16)
        }
        
        lblPassengerDetailsRequired.snp.makeConstraints { (make) in
            make.left.equalTo(lblPassengerDetails.snp.right).offset(4)
            make.top.equalTo(lblPassengerDetails)
        }
        
        passengerDetailsTableView.snp.makeConstraints { (make) in
            make.top.equalTo(lblPassengerDetails.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(16)
            make.bottom.right.equalToSuperview().offset(-16)
            make.height.equalTo(((48 + 8) * FlightModel.shared().passengerList.count) - 8)
        }
        
        //Flight Details
        flightDetailsView.snp.makeConstraints { (make) in
            make.top.equalTo(passengerDetailsTableView.snp.bottom).offset(0)
            make.left.right.equalToSuperview()
            make.height.equalTo(571 - 213)
        }
        
        flightDetailsViewPod.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        btnContinue.snp.makeConstraints { (make) in
            make.top.equalTo(flightDetailsView.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(48)
            make.bottom.equalTo(contentView.snp.bottom).offset(-16)
        }
        
        //Passenger Edit
        coverView.snp.makeConstraints { (make) in
            make.left.right.width.bottom.equalToSuperview()
            make.top.equalTo(navBarView.snp.bottom)
        }
        
        passengerEditViewPod.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(575)
        }
    }
    
    @objc private func didTapBtnCountryCode() {
        
        let config = DefaultConfig()
        Columbus.config = config
        
        let countryPicker = CountryPickerViewController(initialRegionCode: selectedCountry, didSelectClosure: { [weak self] (country) in
            print(country)
            self?.selectedCountry = country.isoCountryCode
            self?.dismiss(animated: true, completion: {
                self!.setCountryCode(for: country)
            })
        })
        present(countryPicker, animated: true)
    }
    
    private func setCountryCode(for country: Country) {
        self.btnCountryCode.setTitle(country.dialingCodeWithPlusPrefix, for: .normal)
        self.btnCountryCode.setImage(country.flagIcon.resizeImage(20, opaque: true), for: .normal)
    }
    
    private func showPassengerEdit() {
        self.coverView.isHidden = false
        self.passengerEditViewPod.isHidden = false
        
        let fromAnimation = AnimationType.from(direction: .bottom, offset: 300.0)
        UIView.animate(views: [self.passengerEditViewPod], animations: [fromAnimation], duration: 0.5)
    }
    
    private func hidePassengerEdit() {
        self.passengerEditViewPod.tfFirstName.text = ""
        self.passengerEditViewPod.tfLastName.text = ""
        self.passengerEditViewPod.tfNrcExp.text = ""
        self.passengerEditViewPod.tfNrcCountry.text = ""
        self.passengerEditViewPod.tfPassportNrc.text = ""
        self.passengerEditViewPod.tfDOB.text = ""
        self.passengerEditViewPod.sameAsContactSwitch.isOn = false
        self.passengerEditViewPod.tfFirstName.isUserInteractionEnabled = true
        self.passengerEditViewPod.tfLastName.isUserInteractionEnabled = true
        
        self.view.endEditing(true)
        self.coverView.isHidden = true
        self.passengerEditViewPod.isHidden = true
    }
    
    private func validateCheck() -> Bool {
        var errorMessage = ""
        
        errorMessage = tfFirstName.text!.isEmpty ? "First Name is required" : ""
        tfFirstName.errorMessage = errorMessage
        
        errorMessage = tfLastName.text!.isEmpty ? "Last Name is required" : ""
        tfLastName.errorMessage = errorMessage
        
        errorMessage = tfMobileNumber.text!.isEmpty ? "Mobile Number is required" : ""
        tfMobileNumber.errorMessage = errorMessage
        
        errorMessage = tfEmail.text!.isEmpty ? "Email is required" : ""
        tfEmail.errorMessage = errorMessage
        
        errorMessage = tfPaymentMethod.text!.isEmpty ? "Payment is required" : ""
        if !errorMessage.isEmpty {
            lblPaymentMethod.text = "Payment Method *"
            lblPaymentMethod.textColor = UIColor.red
        }
        
        emptyPasseger.removeAll()
        for (index, passenger) in FlightModel.shared().passengerList.enumerated() {
            if passenger.firstName.isEmpty || passenger.lastName.isEmpty {
                emptyPasseger.append(index)
                errorMessage += "\(index)"
            }
        }
        passengerDetailsTableView.reloadData()
        
        if tfFirstName.text!.isEmpty || tfLastName.text!.isEmpty || tfMobileNumber.text!.isEmpty || tfEmail.text!.isEmpty || !emptyPasseger.isEmpty {
            return false
        } else {
            return true
        }
    }
    
    private func getContactDetail() -> ContactDetail {
        var contact = ContactDetail()
        contact.firstName = tfFirstName.text ?? ""
        contact.lastName = tfLastName.text ?? ""
        contact.mobileNumber = "\(btnCountryCode.titleLabel?.text)\(tfMobileNumber.text)"
        contact.email = tfEmail.text ?? ""
        return contact
    }
    
    @objc private func expandViewController() {
        UIView.animate(withDuration: 0.3, animations: {
            if self.isFlightDetailsExpended {
                self.flightDetailsView.snp.updateConstraints { (make) in
                    make.height.equalTo(571)  
                }
            } else {
                self.flightDetailsView.snp.updateConstraints { (make) in
                    make.height.equalTo(571 - 213)
                }
                
            }
            self.view.layoutIfNeeded()
        }) { complete in
            self.scrollview.scrollToBottom()
        }
    }
    
    @objc private func didTapBtnContinue() {
        if validateCheck() {
//            self.showConfirmAlert()
            self.bookingIntegrate()
        } else {
            self.scrollview.scrollToTop()
        }
    }
    
    @objc private func didTapBtnClose() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func didTapCoverView() {
        hidePassengerEdit()
    }
    
    private func showConfirmAlert() {
        
        let titleFont = [NSAttributedString.Key.font: UIFont(name: "Roboto-Bold", size: 18.0)!]
        let titleAttrString = NSMutableAttributedString(string: "Are your booking details correct?", attributes: titleFont)

        let messageFont = [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 15.0)!]
        let messageAttrString = NSMutableAttributedString(string: "We will send you an e-ticket by email. So please make sure your contant details are correct.", attributes: messageFont)
        
        let cancelButtonText = NSLocalizedString("Check Again", comment: "")
        let signupButtonText = NSLocalizedString("Yes, Confirm Booking", comment: "")

        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        
        let cancelAction = UIAlertAction(title: cancelButtonText, style: UIAlertAction.Style.cancel, handler: nil)
        let confirmAction = UIAlertAction(title: signupButtonText, style: UIAlertAction.Style.default, handler: { action in
            self.bookingIntegrate()
        })
        alert.addAction(cancelAction)
        alert.addAction(confirmAction)
        present(alert, animated: true, completion: nil)
    }
    
    //integrate with booking api
    func bookingIntegrate() {
        self.showLoadingIndicator(message: "Processing your booking...")
        
        var adultPassenger : [[String: Any]] = [[String: Any]]()
        var childPassenger: [[String:Any]] = [[String: Any]]()
        var infantPassenger: [[String:Any]] = [[String: Any]]()
        
        for i in 0..<FlightModel.shared().passengerList.count {
            
            let passenger = FlightModel.shared().passengerList[i]
            
            switch passenger.type {
            case .Adult:
                let adult : [String: Any] = [
                    "gender": passenger.title,
                    "first_name": passenger.firstName,
                    "last_name": passenger.lastName,
                    "passport_nrc": passenger.nrc,
                    "country_id": passenger.country.countryId ?? 0,
                    "phone_number": btnCountryCode.titleLabel!.text! + tfMobileNumber.text!,
                    "email": self.tfEmail.text ?? "",
                    "passport_expiry": passenger.exp.apiDate,
                    "dob": passenger.dob.apiDate
                ]
                adultPassenger.append(adult)
                
            case .Child:
                let child : [String: Any] = [
                    "gender": passenger.title,
                    "first_name": passenger.firstName,
                    "last_name": passenger.lastName,
                    "passport_nrc": passenger.nrc,
                    "country_id": passenger.country.countryId ?? 0,
                    "phone_number": btnCountryCode.titleLabel!.text! + tfMobileNumber.text!,
                    "email": self.tfEmail.text ?? "",
                    "passport_expiry": passenger.exp.apiDate,
                    "dob": passenger.dob.apiDate
                ]
                childPassenger.append(child)
            case .Infant:
                let infant : [String: Any] = [
                    "gender": passenger.title,
                    "first_name": passenger.firstName,
                    "last_name": passenger.lastName,
                    "passport_nrc": passenger.nrc,
                    "country_id": passenger.country.countryId ?? 0,
                    "phone_number": btnCountryCode.titleLabel!.text! + tfMobileNumber.text!,
                    "email": self.tfEmail.text ?? "",
                    "passport_expiry": passenger.exp.apiDate,
                    "dob": passenger.dob.apiDate
                ]
                infantPassenger.append(infant)
            }
            
        }
        let custname = tfFirstName.text! + " " + tfLastName.text!
        
        let parameters: Parameters = [
            "data": [
                "customer_email": tfEmail.text ?? "",
                "customer_name": custname,
                "subscribe_news": false,
                "referral": "B2C",
                "fare_id": FlightModel.shared().chooseFare.fareId ?? "",
                "connecting_flight": "",
                "adults": FlightModel.shared().getAdultPassengerCount(),
                "children": FlightModel.shared().getChildPassengerCount(),
                "infants": FlightModel.shared().getInfantPassengerCount(),
                "adult_passengers": adultPassenger,
                "infant_passengers": infantPassenger,
                "child_passengers": childPassenger
            ]
        ]
        
        print(parameters)
        FlightModel.shared().postFlightBooking(parameters: parameters, success: {
            self.hideLoadingIndicator()
            ///Go to next page after success booking
            self.goToPayment()
        }) { (error) in
            self.hideLoadingIndicator()
            print("ERROR => \(error)")
            self.bookingFailAlert()
        }
    }
    
    private func goToPayment() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.StoryboardID.PayementVC) as! PaymentViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func goToFlightInformation() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.StoryboardID.ConfirmVC) as! FlightConfirmationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func checkFlight() {
        showLoadingIndicator(message: "Checking Flight Availability...")
        FlightModel.shared().domesticFlightChecker(success: { (data) in
            print(data)
            self.flightListReloadHandler(false)
            self.hideLoadingIndicator()
        }) { (error) in
            self.hideLoadingIndicator()
            let alert = UIAlertController(title: "Sorry!", message: error, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) -> Void in
                self.flightListReloadHandler(true)
                self.navigationController?.popViewController(animated: true)
            }
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func bookingFailAlert() {
        let alert = UIAlertController(title: "Sorry", message: "Your reservation has failed.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    //TextField ValueChnage
    @objc func textFieldValueChange(_ textField: UITextField) {
        switch textField {
        case tfFirstName: tfFirstName.errorMessage = tfFirstName.text!.isEmpty ? "First Name is required" : ""
        case tfLastName: tfLastName.errorMessage = tfLastName.text!.isEmpty ? "Last Name is required" : ""
        case tfMobileNumber: tfMobileNumber.errorMessage = tfMobileNumber.text!.isEmpty ? "Mobile Number is required" : ""
        case tfEmail:
            tfEmail.errorMessage = tfEmail.text!.isEmpty ? "Email is required" : ""
            tfEmail.errorMessage = tfEmail.text!.isValidEmail() ? "" : "Incorrect Email"
            if tfEmail.text!.isEmpty {
                tfEmail.errorMessage = "Email is required"
            } else {
                if !tfEmail.text!.isValidEmail() {
                    tfEmail.errorMessage = "Incorrect Email"
                } else {
                    tfEmail.errorMessage = ""
                }
            }
        default: break
        }
    }
}

extension CustomerViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return FlightModel.shared().passengerList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PassengerDetailsTableViewCell", for: indexPath) as? PassengerDetailsTableViewCell {
            cell.backgroundColor = UIColor.clear
            cell.setData(FlightModel.shared().passengerList[indexPath.section])
            cell.contentView.layer.borderColor = UIColor.gray.cgColor
            emptyPasseger.forEach { (i) in
                if i == indexPath.section {
                    cell.contentView.layer.borderColor = UIColor.red.cgColor
                }
            }
            
            FlightModel.shared().currentPassenger = indexPath.section
            
            return cell
        }
        return UITableViewCell()
    }
}

extension CustomerViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == (tableView.numberOfSections - 1) {
            return 0
        }
        return 8
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if passengerEditViewPod.isHidden {
            let passenger = FlightModel.shared().passengerList[indexPath.section]
            passengerEditViewPod.passengerDetail = passenger
            passengerEditViewPod.showAll()
            
            if passenger.type == .Infant {
                //Hiding NRC view for Infant passenger
                passengerEditViewPod.hideNrcProperty()
                passengerEditViewPod.hideNrc()
            } else {
//                passengerEditViewPod.showAll()
//                if FlightModel.shared().flightReq.nationality == "Foreigner" {
//                    passengerEditViewPod.showNrcProperty()
//                }
//                else {
//                    passengerEditViewPod.hideNrcProperty()
//                }
                
                passengerEditViewPod.showNrcProperty()
            }
            if indexPath.section == 0 {
                //Showing switch and bind data
                passengerEditViewPod.sameAsContactView.isHidden = false
                if passengerEditViewPod.sameAsContactSwitch.isOn {
                    passengerEditViewPod.tfFirstName.text = tfFirstName.text ?? ""
                    passengerEditViewPod.tfFirstName.isUserInteractionEnabled = false
                    passengerEditViewPod.tfFirstName.textColor = UIColor.gray
                    
                    passengerEditViewPod.tfLastName.text = tfFirstName.text ?? ""
                    passengerEditViewPod.tfLastName.isUserInteractionEnabled = false
                    passengerEditViewPod.tfLastName.textColor = UIColor.gray
                }
            } else {
                //Removing previous data and hide switch
                passengerEditViewPod.sameAsContactView.isHidden = true
                
                passengerEditViewPod.tfFirstName.text = passenger.firstName
                passengerEditViewPod.tfFirstName.isUserInteractionEnabled = true
                passengerEditViewPod.tfFirstName.textColor = UIColor.textBlack
                
                passengerEditViewPod.tfLastName.text = passenger.lastName
                passengerEditViewPod.tfLastName.isUserInteractionEnabled = true
                passengerEditViewPod.tfLastName.textColor = UIColor.textBlack
            }
            
            passengerEditViewPod.tfDOB.text = passenger.dob.showDate
            passengerEditViewPod.tfTitle.text = passenger.title
            passengerEditViewPod.tfPassportNrc.text = passenger.nrc
            passengerEditViewPod.tfNrcCountry.text = passenger.country.countryName
            
            let calendar = Calendar(identifier: .gregorian)
            var components = DateComponents()
            components.calendar = calendar
            components.month = 6
            let minDate = calendar.date(byAdding: components, to: passenger.exp) ?? Date()
            passengerEditViewPod.tfNrcExp.text = minDate.passportExpire
            
            
            //To know the index of passenger in list
            passengerEditViewPod.index = indexPath.section
            passengerEditViewPod.passengerDetail = passenger
            passengerEditViewPod.countryPro = self
            passengerEditViewPod.bindData()
            passengerEditViewPod.hideErrorMessage()
            
            showPassengerEdit()
        } else {
            hidePassengerEdit()
        }
    }
}

extension CustomerViewController: CountryProtocol {
    func chooseCountry() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "DestinationSearchViewController") as! DestinationSearchViewController
        vc.setDestinationType(type: .Country)
        vc.setDelegate(country: self)
        self.present(vc, animated: true)
    }
    
    func chooseCountry(country: CountryResponse) {
        passengerEditViewPod.country = country
    }
}
