//
//  FlightConfirmationViewController.swift
//  Flymya
//
//  Created by Kay Kay on 6/27/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import Kingfisher

class FlightConfirmationViewController: UIViewController {
    
    lazy var naviBgView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.primaryBlue
        return view
    }()
    
    lazy private var btnBack: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Left Arrow 1158"), for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnClose), for: .touchUpInside)
        btn.isHidden = true
        return btn
    }()
    
    lazy private var btnDone: UIButton = {
        let btn = UIButton()
        btn.setTitle("Done", for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnDone), for: .touchUpInside)
        return btn
    }()
    
    lazy var lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white
        lbl.text = "Confirmation"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var ivIcon: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "checklist-switchcm")
        return imageview
    }()
    
    lazy var lblTxt1: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = "Your reservation has been received."
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        lbl.sizeToFit()
        lbl.textAlignment = .center
        return lbl
    }()
    
    lazy var lblTxt2: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textColorGray
        lbl.text = "We are currently processing your reservation. You will receive the invoice by email within 30 minutes of the reservations confirmation."
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        lbl.numberOfLines = 0
        lbl.sizeToFit()
        lbl.textAlignment = .center
        return lbl
    }()
    
    lazy var lblTxt3: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = "Contact Email"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 13)
        lbl.sizeToFit()
        lbl.textAlignment = .center
        lbl.isHidden = true
        return lbl
    }()
    
    lazy var lblTxt4: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = "booking@flymya.co"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        lbl.sizeToFit()
        lbl.textAlignment = .center
        lbl.isHidden = true
        return lbl
    }()
    
    lazy var priceView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        return view
    }()
    
    lazy var gradient1 : CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor().HexToColor(hexString: "#46C4F4").cgColor, UIColor().HexToColor(hexString: "#0E99EB").cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.locations = [0.0, 1.0]
        gradient.cornerRadius = 5
        gradient.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        gradient.masksToBounds = true
        self.priceView.layer.insertSublayer(gradient, at: 0)
        return gradient
    }()
    
    lazy var lblTotal: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white
        lbl.text = "Total Price"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 11)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblPrice: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.white
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 23)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblRes: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textColorGray
        lbl.text = "Reservation ID"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblID: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.primaryGreen
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 20)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var cityView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().HexToColor(hexString: "#FAFAFA")
        return view
    }()
    
    lazy var lblDep: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = ""
        lbl.textAlignment = .right
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 25)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblDepCity: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textColorGray
        lbl.text = ""
        lbl.textAlignment = .right
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var ivArrow: UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "Right Arrow Gray")
        return img
    }()
    
    lazy var lblRet: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = ""
        lbl.textAlignment = .left
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 25)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblRetCity: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textColorGray
        lbl.text = ""
        lbl.textAlignment = .left
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblAirline: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = ""
        lbl.textAlignment = .left
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblClass: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textColorGray
        lbl.text = ""
        lbl.textAlignment = .left
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var ivAirline: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "Myanmar Flag")
        imageview.contentMode = UIView.ContentMode.scaleAspectFit
        return imageview
    }()
    
    lazy var bottomView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor().HexToColor(hexString: "#FAFAFA")
        return view
    }()
    
    lazy var ivUp: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "Departure 1")
        return imageview
    }()
    
    lazy var lblDepTime: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = ""
        lbl.textAlignment = .left
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 16)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var ivArr: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "play-arrow")
        return imageview
    }()
    
    lazy var lblTimeTaken: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textColorGray
        lbl.text = ""
        lbl.textAlignment = .left
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var ivDown: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "Arrival")
        return imageview
    }()
    
    lazy var lblRetTime: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = ""
        lbl.textAlignment = .right
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 16)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblNoSeat: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = "No. Of Seat"
        lbl.textAlignment = .left
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblSeat: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textColorGray
        lbl.text = ""
        lbl.textAlignment = .center
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 13)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblTimes: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = "Date"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblDate: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textColorGray
        lbl.text = ""
        lbl.textAlignment = .center
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblFlight: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textBlack
        lbl.text = "Flight Name"
        lbl.textAlignment = .right
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    lazy var lblFName: UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.textColorGray
        lbl.text = ""
        lbl.textAlignment = .right
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
//    fileprivate let formatter: DateFormatter = {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "EE, dd MMM yyyy"
//        return formatter
//    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        setupUI()
        
        addListener()
        
        bindData()
    }
    
    private func bindData() {
        let flight = FlightModel.shared().chooseFlight
        let flightRequest = FlightModel.shared().flightReq
        let fare = FlightModel.shared().chooseFare
        let flightResponse = FlightModel.shared().flightBookingResponse
        
        lblAirline.text = flight.carrierName
        lblClass.text = fare.fareClass
        ivAirline.kf.setImage(with: URL(string: "\(AppConstants.BaseURL)\(flight.carrierLogoPath ?? "")"))
        lblPrice.text = "\(flightResponse?.currency ?? "") \(flightResponse?.total?.description ?? "")"
        lblID.text = flightResponse?.fM_BookingId ?? ""
        
        lblDep.text = flightRequest.depCity?.iATA
        lblDepCity.text = flightRequest.depCity?.cityName
        lblRet.text = flightRequest.retCity?.iATA
        lblRetCity.text = flightRequest.retCity?.cityName
        
        lblDepTime.text = flight.d_Time
        lblRetTime.text = flight.a_Time
        lblTimeTaken.text = flight.duration
        
        lblSeat.text = FlightModel.shared().passengerList.count.description
        lblDate.text = flight.d_Date
        lblFName.text = flight.flightNo
    }
    
    func addListener() {
        
    }
    
    private func setupUI() {
        
        view.addSubview(naviBgView)
        naviBgView.addSubview(btnBack)
        naviBgView.addSubview(btnDone)
        naviBgView.addSubview(lblTitle)
        
        view.addSubview(ivIcon)
        view.addSubview(lblTxt1)
        view.addSubview(lblTxt2)
        view.addSubview(lblTxt3)
        view.addSubview(lblTxt4)
        
        view.addSubview(priceView)
        priceView.addSubview(lblTotal)
        priceView.addSubview(lblPrice)
        
        view.addSubview(lblRes)
        view.addSubview(lblID)
        
        view.addSubview(cityView)
        cityView.addSubview(lblDep)
        cityView.addSubview(lblDepCity)
        cityView.addSubview(ivArrow)
        cityView.addSubview(lblRet)
        cityView.addSubview(lblRetCity)
        
        view.addSubview(lblAirline)
        view.addSubview(lblClass)
        view.addSubview(ivAirline)
        
        view.addSubview(bottomView)
        bottomView.addSubview(ivUp)
        bottomView.addSubview(lblDepTime)
        bottomView.addSubview(ivArr)
        bottomView.addSubview(lblTimeTaken)
        bottomView.addSubview(ivDown)
        bottomView.addSubview(lblRetTime)
        bottomView.addSubview(lblNoSeat)
        bottomView.addSubview(lblSeat)
        bottomView.addSubview(lblTimes)
        bottomView.addSubview(lblDate)
        bottomView.addSubview(lblFlight)
        bottomView.addSubview(lblFName)
        
        addConstraints()
        
    }
    
    func addConstraints() {
        
        naviBgView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(statusBarHeight + 44)
        }
        
        btnBack.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.topMargin).offset(8)
            make.left.equalToSuperview().offset(8)
            make.width.height.equalTo(32)
        }
        
        btnDone.snp.makeConstraints { (make) in
            make.centerY.equalTo(lblTitle.snp.centerY).offset(0)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(32)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(btnBack.snp.centerY)
            make.height.equalTo(20)
        }
        
        ivIcon.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(naviBgView.snp.bottom).offset(16)
            make.width.height.equalTo(35)
        }
        
        lblTxt1.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(ivIcon.snp.bottom).offset(16)
        }
        
        lblTxt2.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(lblTxt1.snp.bottom).offset(8)
        }
        
        lblTxt3.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(lblTxt2.snp.bottom).offset(16)
        }
        
        lblTxt4.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(lblTxt3.snp.bottom)
        }
        
        priceView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalTo(lblTxt4.snp.bottom).offset(16)
            make.width.equalToSuperview().multipliedBy(0.5)
            make.height.equalTo(50)
        }
        
        lblTotal.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(5)
            make.right.equalToSuperview().offset(-16)
        }
        
        lblPrice.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(lblTotal.snp.bottom)
        }
        
        lblRes.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.centerY.equalTo(lblTotal.snp.centerY)
        }
        
        lblID.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(lblRes.snp.bottom)
        }
        
        cityView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(lblID.snp.bottom).offset(16)
            make.height.equalTo(66)
        }
        
        lblDep.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.4)
            make.top.equalToSuperview().offset(10)
        }
        
        lblDepCity.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.4)
            make.top.equalTo(lblDep.snp.bottom)
        }
        
        ivArrow.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        lblRet.snp.makeConstraints { (make) in
            make.right.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.4)
            make.top.equalToSuperview().offset(10)
        }
        
        lblRetCity.snp.makeConstraints { (make) in
            make.right.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.4)
            make.top.equalTo(lblRet.snp.bottom)
        }
        
        lblAirline.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.top.equalTo(cityView.snp.bottom).offset(16)
        }
        
        lblClass.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.top.equalTo(lblAirline.snp.bottom).offset(0)
        }
        
        ivAirline.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(cityView.snp.bottom).offset(8)
            make.height.equalTo(48)
        }
        
        bottomView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(ivAirline.snp.bottom).offset(16)
            make.height.equalTo(120)
        }
        
        ivUp.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.centerX.equalTo(lblDepTime.snp.centerX)
            make.width.equalTo(20)
            make.height.equalTo(12)
        }
        
        lblDepTime.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.top.equalTo(ivUp.snp.bottom).offset(5)
        }
        
        ivArr.snp.makeConstraints { (make) in
            make.centerY.equalTo(ivUp.snp.centerY)
            make.centerX.equalToSuperview()
            make.width.equalTo(10)
            make.height.equalTo(10)
        }
        
        lblTimeTaken.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(lblDepTime.snp.centerY)
        }
        
        ivDown.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.centerX.equalTo(lblRetTime.snp.centerX)
            make.width.equalTo(20)
            make.height.equalTo(12)
        }
        
        lblRetTime.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(ivDown.snp.bottom).offset(5)
        }
        
        lblNoSeat.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.top.equalTo(lblDepTime.snp.bottom).offset(16)
        }
        
        lblSeat.snp.makeConstraints { (make) in
            make.centerX.equalTo(lblNoSeat.snp.centerX)
            make.top.equalTo(lblNoSeat.snp.bottom).offset(5)
        }
        
        lblTimes.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalTo(lblNoSeat.snp.centerY)
        }
        
        lblDate.snp.makeConstraints { (make) in
            make.top.equalTo(lblTimes.snp.bottom).offset(5)
            make.centerX.equalToSuperview()
        }
        
        lblFlight.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(lblRetTime.snp.bottom).offset(16)
        }
        
        lblFName.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(lblFlight.snp.bottom).offset(5)
        }
    }
    
    override func viewDidLayoutSubviews() {
        gradient1.frame.size = priceView.frame.size
    }
    
    @objc private func didTapBtnClose() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func didTapBtnDone() {
        navigationController?.popToRootViewController(animated: true)
    }
}
