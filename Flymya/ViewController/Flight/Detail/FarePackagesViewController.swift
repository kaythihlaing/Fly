//
//  FarePackagesViewController.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/19/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit

protocol FarePackageProtocol {
    func didTapBtnChoosePackage(for index: IndexPath)
}

class FarePackagesViewController: UIViewController {
    
    //MARK: - UI Components
    lazy var farePackagesCollectionView : UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 1, height: 1)
        flowLayout.sectionInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumInteritemSpacing = 8.0
        
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.dataSource = self
        cv.register(FarePackageCollectionViewCell.self, forCellWithReuseIdentifier: "FarePackageCollectionViewCell")
        cv.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cv.clipsToBounds = false
        cv.showsVerticalScrollIndicator = true
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    var selectedCell = 0
    
    var farePackageList : [Fare] = []
    var detailChooseProtocol: DetailChooseProtocol?
    
    //MARK: - Initializer
    init(with data: [Fare], detailProtocol: DetailChooseProtocol) {
        super.init(nibName: nil, bundle: nil)
        self.farePackageList = data.sorted (by: {Double($0.totalPrice!)! < Double($1.totalPrice!)!})
        self.detailChooseProtocol = detailProtocol
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 0..<farePackageList.count {
            if(farePackageList[i].fareId == FlightModel.shared().chooseFare.fareId) {
                selectedCell = i
            }
        }

        view.addSubview(farePackagesCollectionView)
        
        farePackagesCollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.topMargin)
            make.left.right.bottom.equalToSuperview()
        }
    }
    
}

extension FarePackagesViewController: FarePackageProtocol {
    func didTapBtnChoosePackage(for index: IndexPath) {
        print("=> selected row \(index.row)")
        self.selectedCell = index.row
        FlightModel.shared().chooseFare = farePackageList[index.row]
        farePackagesCollectionView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.dismiss(animated: true, completion: nil)
            self.detailChooseProtocol?.chooseClick()
        })
        
    }
}

extension FarePackagesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return farePackageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FarePackageCollectionViewCell", for: indexPath) as? FarePackageCollectionViewCell {
            cell.isSelectedPackage = (indexPath.row == selectedCell) ? true : false
            cell.cellIndex = indexPath
            cell.delegate = self
            cell.layoutIfNeeded()
            cell.setData(for: farePackageList[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }
}

extension FarePackagesViewController: UICollectionViewDelegate {
    
}

extension FarePackagesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width / 2) - 24
        return CGSize(width: width, height: width)
    }
}

