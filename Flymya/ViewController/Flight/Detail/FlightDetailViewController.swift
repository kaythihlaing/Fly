//
//  FlightDetailsViewController.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/19/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import Kingfisher
import MaterialComponents

class FlightDetailViewController: UIViewController {
    
    //MARK: - UI Components
    lazy private var lblAirLineName: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 18)
        lbl.textColor = UIColor.textColorGray
        lbl.numberOfLines = 0
        return lbl
    }()
    
    lazy private var lblFlightNumber: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 17)
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    lazy private var lblSeatClass: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 13)
        lbl.textColor = UIColor.textColorGray
        return lbl
    }()
    
    lazy private var ivAirLineLogo: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy private var departureCityIndicatorView: UIView = {
        let view = UIView()
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 2.0
        view.layer.borderColor = UIColor.primaryBlue.cgColor
        return view
    }()
    
    lazy private var arrivalCityIndicatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.primaryBlue
        view.layer.cornerRadius = 5
        return view
    }()
    
    lazy private var verticalLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.groupTableViewBackground
        return view
    }()
    
    lazy private var horizontalLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.groupTableViewBackground
        return view
    }()
    
    // Departure
    lazy private var lblDepartureTime: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 15)
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    lazy private var lblDepartureDate: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 15)
        lbl.textColor = UIColor.textColorGray
        return lbl
    }()
    
    lazy private var lblDepartureCity: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 15)
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    lazy private var lblDepartureIata: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 15)
        lbl.textColor = UIColor.textColorGray
        return lbl
    }()
    
    lazy private var lblDepartureAirport: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 15)
        lbl.textColor = UIColor.textColorGray
        lbl.numberOfLines = 0
        return lbl
    }()
    
    // Arrival
    lazy private var lblArrivalTime: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 15)
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    lazy private var lblArrivalDate: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 15)
        lbl.textColor = UIColor.textColorGray
        return lbl
    }()
    
    lazy private var lblArrivalCity: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 15)
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    lazy private var lblArrivalIata: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 15)
        lbl.textColor = UIColor.textColorGray
        return lbl
    }()
    
    lazy private var lblArrivalAirport: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 15)
        lbl.textColor = UIColor.textColorGray
        lbl.numberOfLines = 0
        return lbl
    }()
    
    lazy private var lblViaAirport: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 15)
        lbl.textColor = UIColor.textColorGray
        lbl.numberOfLines = 1
        return lbl
    }()
    
    lazy private var ivBaggage: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "baggage-blank")
        return iv
    }()
    
    lazy private var lblBaggageAmount: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 15)
        lbl.textColor = UIColor.primaryBlue
        return lbl
    }()
    
    lazy private var lblBaggageAllowance: UILabel = {
        let lbl = UILabel()
        lbl.text = "Baggage Allowance"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 15)
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    lazy private var lblBaggageAllowanceText: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 15)
        lbl.textColor = UIColor.textColorGray
        return lbl
    }()
    
    lazy private var lblTotalPrice: UILabel = {
        let lbl = UILabel()
        lbl.text = "Total Price"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    lazy private var lblTotalPriceText: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        lbl.textColor = #colorLiteral(red: 0, green: 0.6509803922, blue: 0.3176470588, alpha: 1)
        return lbl
    }()
    
    lazy private var btnChoose: MDCButton = {
        let btn = MDCButton()
        btn.setTitle("Choose", for: .normal)
        btn.backgroundColor = UIColor.primaryBlue
        btn.setTitleFont(UIFont(name: AppConstants.Font.Roboto.Bold, size: 17), for: .normal)
        btn.isUppercaseTitle = false
        btn.layer.cornerRadius = 4
        btn.layer.masksToBounds = true
        btn.addTarget(self, action: #selector(didTapBtnChoose), for: .touchUpInside)
        return btn
    }()
    
    //MARK: - Attributes
    private var flightDetail: Flights?
    var detailChooseProtocol: DetailChooseProtocol?
    
    //MARK: - Initializer
    init(for detail: Flights, detailProtocol: DetailChooseProtocol) {
        self.flightDetail = detail
        self.detailChooseProtocol = detailProtocol
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        bindData()
    }
    
    //MARK: - Private Functions
    private func setupUI() {
        view.addSubview(lblAirLineName)
        view.addSubview(lblFlightNumber)
        view.addSubview(lblSeatClass)
        view.addSubview(ivAirLineLogo)
        
        view.addSubview(departureCityIndicatorView)
        view.addSubview(verticalLineView)
        view.addSubview(arrivalCityIndicatorView)
        
        view.addSubview(lblDepartureTime)
        view.addSubview(lblDepartureDate)
        view.addSubview(lblDepartureCity)
        view.addSubview(lblDepartureIata)
        view.addSubview(lblDepartureAirport)
        
        view.addSubview(lblArrivalTime)
        view.addSubview(lblArrivalDate)
        view.addSubview(lblArrivalCity)
        view.addSubview(lblArrivalIata)
        view.addSubview(lblArrivalAirport)
        
        view.addSubview(lblViaAirport)
        
        view.addSubview(ivBaggage)
        ivBaggage.addSubview(lblBaggageAmount)
        view.addSubview(lblBaggageAllowance)
        view.addSubview(lblBaggageAllowanceText)
        view.addSubview(horizontalLineView)
        
        view.addSubview(lblTotalPrice)
        view.addSubview(lblTotalPriceText)
        view.addSubview(btnChoose)
        
        setConstraints()
    }
    
    private func setConstraints() {
        lblAirLineName.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp_topMargin).offset(16)
            make.left.equalToSuperview().offset(20)
            make.right.equalTo(ivAirLineLogo.snp.left).offset(-8)
        }
        
        lblFlightNumber.snp.makeConstraints { (make) in
            make.top.equalTo(lblAirLineName.snp.bottom).offset(8)
            make.left.equalTo(lblAirLineName.snp.left)
        }
        
        lblSeatClass.snp.makeConstraints { (make) in
            make.top.equalTo(lblFlightNumber.snp.bottom).offset(0)
            make.left.equalTo(lblAirLineName.snp.left)
        }
        
        ivAirLineLogo.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp_topMargin).offset(16)
            make.right.equalToSuperview().offset(-20)
            make.width.equalTo(120)
            make.height.equalTo(46)
        }
        
        departureCityIndicatorView.snp.makeConstraints { (make) in
            make.top.equalTo(lblSeatClass.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(20)
            make.width.height.equalTo(10)
        }
        
        verticalLineView.snp.makeConstraints { (make) in
            make.top.equalTo(departureCityIndicatorView.snp.bottom).offset(8)
            make.centerX.equalTo(departureCityIndicatorView.snp.centerX)
            make.width.equalTo(3)
            make.height.equalTo(50)
        }
        
        arrivalCityIndicatorView.snp.makeConstraints { (make) in
            make.top.equalTo(verticalLineView.snp.bottom).offset(8)
            make.centerX.equalTo(verticalLineView.snp.centerX)
            make.width.height.equalTo(10)
        }
        
        // Departure
        lblDepartureTime.snp.makeConstraints { (make) in
            make.centerY.equalTo(departureCityIndicatorView.snp.centerY)
            make.left.equalTo(departureCityIndicatorView.snp.right).offset(8)
        }
        
        lblDepartureDate.snp.makeConstraints { (make) in
            make.top.equalTo(lblDepartureTime.snp.bottom).offset(8)
            make.left.equalTo(lblDepartureTime)
        }
        
        lblDepartureCity.snp.makeConstraints { (make) in
            make.top.equalTo(lblDepartureTime)
            make.left.equalTo(lblDepartureDate.snp.right).offset(16)
        }
        
        lblDepartureIata.snp.makeConstraints { (make) in
            make.top.equalTo(lblDepartureCity)
            make.left.equalTo(lblDepartureCity.snp.right).offset(8)
        }
        
        lblDepartureAirport.snp.makeConstraints { (make) in
            make.top.equalTo(lblDepartureCity.snp.bottom).offset(8)
            make.left.equalTo(lblDepartureCity)
            make.right.equalToSuperview().offset(-20)
        }
        
        // Arrival
        lblArrivalTime.snp.makeConstraints { (make) in
            make.centerY.equalTo(arrivalCityIndicatorView.snp.centerY)
            make.left.equalTo(arrivalCityIndicatorView.snp.right).offset(8)
        }
        
        lblArrivalDate.snp.makeConstraints { (make) in
            make.top.equalTo(lblArrivalTime.snp.bottom).offset(8)
            make.left.equalTo(lblArrivalTime)
        }
        
        lblArrivalCity.snp.makeConstraints { (make) in
            make.top.equalTo(lblArrivalTime)
            make.left.equalTo(lblArrivalDate.snp.right).offset(16)
        }
        
        lblArrivalIata.snp.makeConstraints { (make) in
            make.top.equalTo(lblArrivalCity)
            make.left.equalTo(lblArrivalCity.snp.right).offset(8)
        }
        
        lblArrivalAirport.snp.makeConstraints { (make) in
            make.top.equalTo(lblArrivalCity.snp.bottom).offset(8)
            make.left.equalTo(lblArrivalCity)
            make.right.equalToSuperview().offset(-20)
        }
        
        //Via Airport
        lblViaAirport.snp.makeConstraints { (make) in
            make.top.equalTo(lblArrivalDate.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(20)
        }
        
        //Baggage
        ivBaggage.snp.makeConstraints { (make) in
            make.top.equalTo(lblViaAirport.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(20)
            make.width.height.equalTo(24)
        }
        
        lblBaggageAmount.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        lblBaggageAllowance.snp.makeConstraints { (make) in
            make.left.equalTo(ivBaggage.snp.right).offset(8)
            make.centerY.equalTo(ivBaggage)
        }
        
        lblBaggageAllowanceText.snp.makeConstraints { (make) in
            make.centerY.equalTo(lblBaggageAllowance)
            make.left.equalTo(lblBaggageAllowance.snp.right).offset(8)
        }
        
        horizontalLineView.snp.makeConstraints { (make) in
            make.top.equalTo(lblBaggageAllowance.snp.bottom).offset(16)
            make.centerX.equalToSuperview()
            make.height.equalTo(3)
            make.width.equalToSuperview().multipliedBy(0.9)
        }
        
        lblTotalPrice.snp.makeConstraints { (make) in
            make.top.equalTo(horizontalLineView.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(20)
        }
        
        lblTotalPriceText.snp.makeConstraints { (make) in
            make.top.equalTo(lblTotalPrice.snp.bottom).offset(4)
            make.left.equalTo(lblTotalPrice)
        }
        
        btnChoose.snp.makeConstraints { (make) in
            make.top.equalTo(lblTotalPrice)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(48)
            make.width.equalTo(120)
        }
    }
    
    private func bindData() {
        guard let detail = flightDetail else {
            return
        }
        
        lblAirLineName.text = detail.carrierName
        lblFlightNumber.text = detail.flightNo
        
        ivAirLineLogo.kf.setImage(with: URL(string: "\(AppConstants.BaseURL)\(detail.carrierLogoPath ?? "")"))
        lblDepartureCity.text = detail.d_Airport
        lblDepartureIata.text = detail.d_IATA
        lblDepartureTime.text = detail.d_Time
        lblDepartureDate.text = detail.d_Date
//        lblDepartureAirport.text = "need this from api"
        lblArrivalCity.text = detail.a_Airport
        lblArrivalIata.text = detail.a_IATA
        lblArrivalTime.text = detail.a_Time
        lblArrivalDate.text = detail.a_Date
        
        var viaText = detail.via ?? ""
        if let range = viaText.lowercased().range(of: "via".lowercased()) {
            viaText.removeSubrange(range)
        }
        lblViaAirport.text = detail.via?.isEmpty ?? false ? "Direct" : "Via \(viaText)"
        
        let chooseFare = FlightModel.shared().chooseFare
        lblSeatClass.text = chooseFare.fareClass
        lblTotalPriceText.text = "\(chooseFare.currency ?? "") \(chooseFare.totalPrice ?? "")"
        lblBaggageAllowance.text = chooseFare.baggagePolicy?.html2String
    }
    
    @objc private func didTapBtnChoose() {
        self.dismiss(animated: true, completion: nil)
        detailChooseProtocol?.chooseClick()
    }
    
}
