//
//  FlightDetailsTabViewController.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/18/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import BmoViewPager

class FlightDetailsTabViewController: UIViewController {

    lazy private var navBarView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.primaryBlue
        return view
    }()
    
    lazy private var btnClose: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Cancel"), for: .normal)
        btn.addTarget(self, action: #selector(didTapBtnClose), for: .touchUpInside)
        return btn
    }()
    
    lazy private var lblAirlineName: UILabel = {
        let lbl = UILabel()
        lbl.text = "Airline Name"
        lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 18)
        return lbl
    }()
    
    lazy private var lblFlightInformation: UILabel = {
        let lbl = UILabel()
        lbl.text = "RGN - NYU, 2hr 5min, 1 Adult, direct"
        lbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 15)
        return lbl
    }()
    
    lazy private var tabBarView: BmoViewPagerNavigationBar = {
        let view = BmoViewPagerNavigationBar()
        view.autoFocus = false
        view.viewPager = self.pagerView
        return view
    }()
    
    lazy private var pagerView: BmoViewPager = {
        let view = BmoViewPager()
        view.dataSource = self
        return view
    }()
    
    private let childViewList : [String] = ["Flight Details", "Fare Packages"]
    
    private var cachedPageViewControllers : [Int: UIViewController] = [:]
    
    var selectedFlightIndex: Int? {
        didSet {
            if let index = selectedFlightIndex {
                self.selectedFlight = FlightModel.shared().flightList[index]
            } else {
                self.selectedFlight = nil
            }
        }
    }
    
    private var selectedFlight: Flights?
    
    var detailChooseProtocol: DetailChooseProtocol?
    
    //MARK: - Life Cycle
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNeedsStatusBarAppearanceUpdate()
        
        setupUI()
        
        guard let flight = self.selectedFlight else { return }
        
       
        cachedPageViewControllers = [
            0 : FlightDetailViewController(for: flight, detailProtocol: detailChooseProtocol!),
            1 : FarePackagesViewController(with: flight.fare ?? [], detailProtocol: detailChooseProtocol!)
        ]
        
        bindData()
    }
    
    //MARK: - Private Functions
    private func setupUI() {
        //Navigation Bar
        view.addSubview(navBarView)
        navBarView.addSubview(lblAirlineName)
        navBarView.addSubview(lblFlightInformation)
        navBarView.addSubview(btnClose)
        
        view.addSubview(tabBarView)
        view.addSubview(pagerView)
        
        setConstraints()
    }
    
    private func setConstraints() {
        //Navigation Bar
        navBarView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(statusBarHeight + 44)
        }
        
        btnClose.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.topMargin).offset(8)
            make.right.equalToSuperview().offset(-8)
            make.width.height.equalTo(32)
        }
        
        lblAirlineName.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.topMargin)
            make.left.equalToSuperview().offset(16)
        }
        
        lblFlightInformation.snp.makeConstraints { (make) in
            make.top.equalTo(lblAirlineName.snp.bottom)
            make.left.equalTo(lblAirlineName)
            make.bottom.equalToSuperview()
        }
        
        tabBarView.snp.makeConstraints { (make) in
            make.top.equalTo(navBarView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(44)
        }
        
        pagerView.snp.makeConstraints { (make) in
            make.top.equalTo(tabBarView.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        }
    }
    
    private func bindData() {
        guard let _flightDetail = selectedFlight else {
            return
        }
        
        lblAirlineName.text = _flightDetail.carrierName
        lblFlightInformation.text = "\(_flightDetail.d_IATA ?? "") - \(_flightDetail.a_IATA ?? ""), \(_flightDetail.duration ?? ""), 1 Adult"
    }
    
    @objc private func didTapBtnClose() {
        dismiss(animated: true, completion: nil)
    }
    
}

extension FlightDetailsTabViewController: BmoViewPagerDataSource {
    func bmoViewPagerDataSourceNumberOfPage(in viewPager: BmoViewPager) -> Int {
        return childViewList.count
    }
    
    func bmoViewPagerDataSource(_ viewPager: BmoViewPager, viewControllerForPageAt page: Int) -> UIViewController {
        switch page {
        case 0:
            return cachedPageViewControllers[0]!
            
        case 1:
            return cachedPageViewControllers[1]!
            
        default:
            return UIViewController()
        }
    }
    
    //NavBar
    func bmoViewPagerDataSourceNaviagtionBarItemNormalAttributed(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> [NSAttributedString.Key : Any]? {
        return [
            NSAttributedString.Key.font : UIFont.init(name: AppConstants.Font.Roboto.Regular, size: 17)!,
            NSAttributedString.Key.foregroundColor : UIColor.gray
        ]
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemHighlightedAttributed(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> [NSAttributedString.Key : Any]? {
        return [
            NSAttributedString.Key.font : UIFont.init(name: AppConstants.Font.Roboto.Bold, size: 17)!,
            NSAttributedString.Key.foregroundColor : UIColor.primaryBlue
        ]
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemHighlightedBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {
        let view = UnderLineView()
        view.marginX = 0.0
        view.lineWidth = 5.0
        view.strokeColor = UIColor.primaryBlue
        return view
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemNormalBackgroundView(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> UIView? {
        let view = UnderLineView()
        view.marginX = 0.0
        view.lineWidth = 5.0
        view.strokeColor = UIColor.groupTableViewBackground
        return view
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemSize(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> CGSize {
        return CGSize(width: navigationBar.bounds.width / 2, height: navigationBar.bounds.height)
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemSpace(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> CGFloat {
        return 0
    }
    
    func bmoViewPagerDataSourceNaviagtionBarItemTitle(_ viewPager: BmoViewPager, navigationBar: BmoViewPagerNavigationBar, forPageListAt page: Int) -> String? {
        return childViewList[page]
    }
}
