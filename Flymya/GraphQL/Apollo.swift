//
//  ApolloManager.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 8/21/19.
//  Copyright © 2019 Flymya.com. All rights reserved.
//

import Foundation
import Apollo

class ApolloManager {
    
    static let shared = ApolloManager()
    
    let client: ApolloClient
    
    init() {
        client = ApolloClient(url: URL(string: AppConstants.GraphQLUrl)!)
    }
    
    func samplePayment() {
        let meta = formDataInput(productType: "domestic", transactionId: "22979")
        let input = ominipayGatewayInput(gateway: "stripe", amount: "1", currency: "USD", token: "tk24214", metadata: meta, description: "hello")
        ApolloManager.shared.client.perform(mutation: PayGatewayMutationMutation(input: input)) { result in
            print("GraphQL =>")
            print(result)
        }
    }
}
