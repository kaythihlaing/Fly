//  This file was automatically generated and should not be edited.

import Apollo

public struct ominipayGatewayInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(gateway: String, amount: Swift.Optional<String?> = nil, currency: Swift.Optional<String?> = nil, token: Swift.Optional<String?> = nil, metadata: formDataInput, description: Swift.Optional<String?> = nil) {
    graphQLMap = ["gateway": gateway, "amount": amount, "currency": currency, "token": token, "metadata": metadata, "description": description]
  }

  public var gateway: String {
    get {
      return graphQLMap["gateway"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gateway")
    }
  }

  public var amount: Swift.Optional<String?> {
    get {
      return graphQLMap["amount"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "amount")
    }
  }

  public var currency: Swift.Optional<String?> {
    get {
      return graphQLMap["currency"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "currency")
    }
  }

  public var token: Swift.Optional<String?> {
    get {
      return graphQLMap["token"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "token")
    }
  }

  public var metadata: formDataInput {
    get {
      return graphQLMap["metadata"] as! formDataInput
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "metadata")
    }
  }

  public var description: Swift.Optional<String?> {
    get {
      return graphQLMap["description"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "description")
    }
  }
}

public struct formDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(productType: String, transactionId: String, otherId: Swift.Optional<String?> = nil, legacyId: Swift.Optional<String?> = nil, card: Swift.Optional<cardDataInput?> = nil, encryptedCardInfo: Swift.Optional<String?> = nil, maskedCardInfo: Swift.Optional<String?> = nil, expMonthCardInfo: Swift.Optional<String?> = nil, expYearCardInfo: Swift.Optional<String?> = nil, cardholderName: Swift.Optional<String?> = nil, panCountry: Swift.Optional<String?> = nil, timeStamp: Swift.Optional<String?> = nil, expiry: Swift.Optional<CcppExpiry?> = nil, storeCardUniqueId: Swift.Optional<String?> = nil, clientIp: Swift.Optional<String?> = nil, panBank: Swift.Optional<String?> = nil, cardholderEmail: Swift.Optional<String?> = nil, payCategoryId: Swift.Optional<String?> = nil, userDefined1: Swift.Optional<String?> = nil, userDefined2: Swift.Optional<String?> = nil, userDefined3: Swift.Optional<String?> = nil, userDefined4: Swift.Optional<String?> = nil, userDefined5: Swift.Optional<String?> = nil, storeCard: Swift.Optional<String?> = nil, ippTransaction: Swift.Optional<String?> = nil, installmentPeriod: Swift.Optional<String?> = nil, interestType: Swift.Optional<String?> = nil, recurring: Swift.Optional<String?> = nil, invoicePrefix: Swift.Optional<String?> = nil, recurringAmount: Swift.Optional<String?> = nil, allowAccumulate: Swift.Optional<String?> = nil, maxAccumulateAmt: Swift.Optional<String?> = nil, recurringInterval: Swift.Optional<String?> = nil, recurringCount: Swift.Optional<String?> = nil, chargeNextDate: Swift.Optional<String?> = nil, promotion: Swift.Optional<String?> = nil, request3Ds: Swift.Optional<String?> = nil, statementDescriptor: Swift.Optional<String?> = nil, agentCode: Swift.Optional<String?> = nil, channelCode: Swift.Optional<String?> = nil, paymentExpiry: Swift.Optional<String?> = nil, mobileNo: Swift.Optional<String?> = nil, subMerchantList: Swift.Optional<String?> = nil, qrData: Swift.Optional<String?> = nil, qrOption: Swift.Optional<String?> = nil, tokenizeWithoutAuthorization: Swift.Optional<String?> = nil, rateQuoteId: Swift.Optional<String?> = nil, originalAmount: Swift.Optional<String?> = nil, customRouteId: Swift.Optional<String?> = nil, airlineTransaction: Swift.Optional<String?> = nil, airlinePassengers: Swift.Optional<String?> = nil, addresses: Swift.Optional<CcppAddress?> = nil, purchaserMsisdn: Swift.Optional<String?> = nil, tradeType: Swift.Optional<String?> = nil) {
    graphQLMap = ["productType": productType, "transactionId": transactionId, "otherId": otherId, "legacyId": legacyId, "card": card, "encryptedCardInfo": encryptedCardInfo, "maskedCardInfo": maskedCardInfo, "expMonthCardInfo": expMonthCardInfo, "expYearCardInfo": expYearCardInfo, "cardholderName": cardholderName, "panCountry": panCountry, "timeStamp": timeStamp, "expiry": expiry, "storeCardUniqueID": storeCardUniqueId, "clientIP": clientIp, "panBank": panBank, "cardholderEmail": cardholderEmail, "payCategoryID": payCategoryId, "userDefined1": userDefined1, "userDefined2": userDefined2, "userDefined3": userDefined3, "userDefined4": userDefined4, "userDefined5": userDefined5, "storeCard": storeCard, "ippTransaction": ippTransaction, "installmentPeriod": installmentPeriod, "interestType": interestType, "recurring": recurring, "invoicePrefix": invoicePrefix, "recurringAmount": recurringAmount, "allowAccumulate": allowAccumulate, "maxAccumulateAmt": maxAccumulateAmt, "recurringInterval": recurringInterval, "recurringCount": recurringCount, "chargeNextDate": chargeNextDate, "promotion": promotion, "request3DS": request3Ds, "statementDescriptor": statementDescriptor, "agentCode": agentCode, "channelCode": channelCode, "paymentExpiry": paymentExpiry, "mobileNo": mobileNo, "subMerchantList": subMerchantList, "qrData": qrData, "qrOption": qrOption, "tokenizeWithoutAuthorization": tokenizeWithoutAuthorization, "rateQuoteID": rateQuoteId, "originalAmount": originalAmount, "customRouteID": customRouteId, "airlineTransaction": airlineTransaction, "airlinePassengers": airlinePassengers, "addresses": addresses, "purchaserMsisdn": purchaserMsisdn, "tradeType": tradeType]
  }

  public var productType: String {
    get {
      return graphQLMap["productType"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "productType")
    }
  }

  public var transactionId: String {
    get {
      return graphQLMap["transactionId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "transactionId")
    }
  }

  public var otherId: Swift.Optional<String?> {
    get {
      return graphQLMap["otherId"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "otherId")
    }
  }

  public var legacyId: Swift.Optional<String?> {
    get {
      return graphQLMap["legacyId"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "legacyId")
    }
  }

  public var card: Swift.Optional<cardDataInput?> {
    get {
      return graphQLMap["card"] as? Swift.Optional<cardDataInput?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "card")
    }
  }

  public var encryptedCardInfo: Swift.Optional<String?> {
    get {
      return graphQLMap["encryptedCardInfo"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "encryptedCardInfo")
    }
  }

  public var maskedCardInfo: Swift.Optional<String?> {
    get {
      return graphQLMap["maskedCardInfo"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "maskedCardInfo")
    }
  }

  public var expMonthCardInfo: Swift.Optional<String?> {
    get {
      return graphQLMap["expMonthCardInfo"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "expMonthCardInfo")
    }
  }

  public var expYearCardInfo: Swift.Optional<String?> {
    get {
      return graphQLMap["expYearCardInfo"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "expYearCardInfo")
    }
  }

  public var cardholderName: Swift.Optional<String?> {
    get {
      return graphQLMap["cardholderName"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "cardholderName")
    }
  }

  public var panCountry: Swift.Optional<String?> {
    get {
      return graphQLMap["panCountry"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "panCountry")
    }
  }

  public var timeStamp: Swift.Optional<String?> {
    get {
      return graphQLMap["timeStamp"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "timeStamp")
    }
  }

  public var expiry: Swift.Optional<CcppExpiry?> {
    get {
      return graphQLMap["expiry"] as? Swift.Optional<CcppExpiry?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "expiry")
    }
  }

  public var storeCardUniqueId: Swift.Optional<String?> {
    get {
      return graphQLMap["storeCardUniqueID"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "storeCardUniqueID")
    }
  }

  public var clientIp: Swift.Optional<String?> {
    get {
      return graphQLMap["clientIP"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "clientIP")
    }
  }

  public var panBank: Swift.Optional<String?> {
    get {
      return graphQLMap["panBank"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "panBank")
    }
  }

  public var cardholderEmail: Swift.Optional<String?> {
    get {
      return graphQLMap["cardholderEmail"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "cardholderEmail")
    }
  }

  public var payCategoryId: Swift.Optional<String?> {
    get {
      return graphQLMap["payCategoryID"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "payCategoryID")
    }
  }

  public var userDefined1: Swift.Optional<String?> {
    get {
      return graphQLMap["userDefined1"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userDefined1")
    }
  }

  public var userDefined2: Swift.Optional<String?> {
    get {
      return graphQLMap["userDefined2"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userDefined2")
    }
  }

  public var userDefined3: Swift.Optional<String?> {
    get {
      return graphQLMap["userDefined3"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userDefined3")
    }
  }

  public var userDefined4: Swift.Optional<String?> {
    get {
      return graphQLMap["userDefined4"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userDefined4")
    }
  }

  public var userDefined5: Swift.Optional<String?> {
    get {
      return graphQLMap["userDefined5"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userDefined5")
    }
  }

  public var storeCard: Swift.Optional<String?> {
    get {
      return graphQLMap["storeCard"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "storeCard")
    }
  }

  public var ippTransaction: Swift.Optional<String?> {
    get {
      return graphQLMap["ippTransaction"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ippTransaction")
    }
  }

  public var installmentPeriod: Swift.Optional<String?> {
    get {
      return graphQLMap["installmentPeriod"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "installmentPeriod")
    }
  }

  public var interestType: Swift.Optional<String?> {
    get {
      return graphQLMap["interestType"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "interestType")
    }
  }

  public var recurring: Swift.Optional<String?> {
    get {
      return graphQLMap["recurring"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "recurring")
    }
  }

  public var invoicePrefix: Swift.Optional<String?> {
    get {
      return graphQLMap["invoicePrefix"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "invoicePrefix")
    }
  }

  public var recurringAmount: Swift.Optional<String?> {
    get {
      return graphQLMap["recurringAmount"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "recurringAmount")
    }
  }

  public var allowAccumulate: Swift.Optional<String?> {
    get {
      return graphQLMap["allowAccumulate"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "allowAccumulate")
    }
  }

  public var maxAccumulateAmt: Swift.Optional<String?> {
    get {
      return graphQLMap["maxAccumulateAmt"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "maxAccumulateAmt")
    }
  }

  public var recurringInterval: Swift.Optional<String?> {
    get {
      return graphQLMap["recurringInterval"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "recurringInterval")
    }
  }

  public var recurringCount: Swift.Optional<String?> {
    get {
      return graphQLMap["recurringCount"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "recurringCount")
    }
  }

  public var chargeNextDate: Swift.Optional<String?> {
    get {
      return graphQLMap["chargeNextDate"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "chargeNextDate")
    }
  }

  public var promotion: Swift.Optional<String?> {
    get {
      return graphQLMap["promotion"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "promotion")
    }
  }

  public var request3Ds: Swift.Optional<String?> {
    get {
      return graphQLMap["request3DS"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "request3DS")
    }
  }

  public var statementDescriptor: Swift.Optional<String?> {
    get {
      return graphQLMap["statementDescriptor"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "statementDescriptor")
    }
  }

  public var agentCode: Swift.Optional<String?> {
    get {
      return graphQLMap["agentCode"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "agentCode")
    }
  }

  public var channelCode: Swift.Optional<String?> {
    get {
      return graphQLMap["channelCode"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "channelCode")
    }
  }

  public var paymentExpiry: Swift.Optional<String?> {
    get {
      return graphQLMap["paymentExpiry"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "paymentExpiry")
    }
  }

  public var mobileNo: Swift.Optional<String?> {
    get {
      return graphQLMap["mobileNo"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "mobileNo")
    }
  }

  public var subMerchantList: Swift.Optional<String?> {
    get {
      return graphQLMap["subMerchantList"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "subMerchantList")
    }
  }

  public var qrData: Swift.Optional<String?> {
    get {
      return graphQLMap["qrData"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "qrData")
    }
  }

  public var qrOption: Swift.Optional<String?> {
    get {
      return graphQLMap["qrOption"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "qrOption")
    }
  }

  public var tokenizeWithoutAuthorization: Swift.Optional<String?> {
    get {
      return graphQLMap["tokenizeWithoutAuthorization"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "tokenizeWithoutAuthorization")
    }
  }

  public var rateQuoteId: Swift.Optional<String?> {
    get {
      return graphQLMap["rateQuoteID"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "rateQuoteID")
    }
  }

  public var originalAmount: Swift.Optional<String?> {
    get {
      return graphQLMap["originalAmount"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "originalAmount")
    }
  }

  public var customRouteId: Swift.Optional<String?> {
    get {
      return graphQLMap["customRouteID"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "customRouteID")
    }
  }

  public var airlineTransaction: Swift.Optional<String?> {
    get {
      return graphQLMap["airlineTransaction"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "airlineTransaction")
    }
  }

  public var airlinePassengers: Swift.Optional<String?> {
    get {
      return graphQLMap["airlinePassengers"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "airlinePassengers")
    }
  }

  public var addresses: Swift.Optional<CcppAddress?> {
    get {
      return graphQLMap["addresses"] as? Swift.Optional<CcppAddress?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "addresses")
    }
  }

  public var purchaserMsisdn: Swift.Optional<String?> {
    get {
      return graphQLMap["purchaserMsisdn"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "purchaserMsisdn")
    }
  }

  public var tradeType: Swift.Optional<String?> {
    get {
      return graphQLMap["tradeType"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "tradeType")
    }
  }
}

public struct cardDataInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(number: String, expiryMonth: String, expiryYear: String, cvv: String, title: Swift.Optional<String?> = nil, firstName: Swift.Optional<String?> = nil, lastName: Swift.Optional<String?> = nil, name: Swift.Optional<String?> = nil, company: Swift.Optional<String?> = nil, address1: Swift.Optional<String?> = nil, address2: Swift.Optional<String?> = nil, city: Swift.Optional<String?> = nil, postcode: Swift.Optional<String?> = nil, state: Swift.Optional<String?> = nil, country: Swift.Optional<String?> = nil, phone: Swift.Optional<String?> = nil, phoneExtension: Swift.Optional<String?> = nil, fax: Swift.Optional<String?> = nil, startMonth: Swift.Optional<String?> = nil, startYear: Swift.Optional<String?> = nil, tracks: Swift.Optional<String?> = nil, issueNumber: Swift.Optional<String?> = nil, billingTitle: Swift.Optional<String?> = nil, billingName: Swift.Optional<String?> = nil, billingFirstName: Swift.Optional<String?> = nil, billingLastName: Swift.Optional<String?> = nil, billingCompany: Swift.Optional<String?> = nil, billingAddress1: Swift.Optional<String?> = nil, billingAddress2: Swift.Optional<String?> = nil, billingCity: Swift.Optional<String?> = nil, billingPostcode: Swift.Optional<String?> = nil, billingState: Swift.Optional<String?> = nil, billingCountry: Swift.Optional<String?> = nil, billingPhone: Swift.Optional<String?> = nil, billingFax: Swift.Optional<String?> = nil, shippingTitle: Swift.Optional<String?> = nil, shippingName: Swift.Optional<String?> = nil, shippingFirstName: Swift.Optional<String?> = nil, shippingLastName: Swift.Optional<String?> = nil, shippingCompany: Swift.Optional<String?> = nil, shippingAddress1: Swift.Optional<String?> = nil, shippingAddress2: Swift.Optional<String?> = nil, shippingCity: Swift.Optional<String?> = nil, shippingPostcode: Swift.Optional<String?> = nil, shippingState: Swift.Optional<String?> = nil, shippingCountry: Swift.Optional<String?> = nil, shippingPhone: Swift.Optional<String?> = nil, shippingFax: Swift.Optional<String?> = nil, email: Swift.Optional<String?> = nil, birthday: Swift.Optional<String?> = nil, gender: Swift.Optional<String?> = nil) {
    graphQLMap = ["number": number, "expiryMonth": expiryMonth, "expiryYear": expiryYear, "cvv": cvv, "title": title, "firstName": firstName, "lastName": lastName, "name": name, "company": company, "address1": address1, "address2": address2, "city": city, "postcode": postcode, "state": state, "country": country, "phone": phone, "phoneExtension": phoneExtension, "fax": fax, "startMonth": startMonth, "startYear": startYear, "tracks": tracks, "issueNumber": issueNumber, "billingTitle": billingTitle, "billingName": billingName, "billingFirstName": billingFirstName, "billingLastName": billingLastName, "billingCompany": billingCompany, "billingAddress1": billingAddress1, "billingAddress2": billingAddress2, "billingCity": billingCity, "billingPostcode": billingPostcode, "billingState": billingState, "billingCountry": billingCountry, "billingPhone": billingPhone, "billingFax": billingFax, "shippingTitle": shippingTitle, "shippingName": shippingName, "shippingFirstName": shippingFirstName, "shippingLastName": shippingLastName, "shippingCompany": shippingCompany, "shippingAddress1": shippingAddress1, "shippingAddress2": shippingAddress2, "shippingCity": shippingCity, "shippingPostcode": shippingPostcode, "shippingState": shippingState, "shippingCountry": shippingCountry, "shippingPhone": shippingPhone, "shippingFax": shippingFax, "email": email, "birthday": birthday, "gender": gender]
  }

  public var number: String {
    get {
      return graphQLMap["number"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "number")
    }
  }

  public var expiryMonth: String {
    get {
      return graphQLMap["expiryMonth"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "expiryMonth")
    }
  }

  public var expiryYear: String {
    get {
      return graphQLMap["expiryYear"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "expiryYear")
    }
  }

  public var cvv: String {
    get {
      return graphQLMap["cvv"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "cvv")
    }
  }

  public var title: Swift.Optional<String?> {
    get {
      return graphQLMap["title"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "title")
    }
  }

  public var firstName: Swift.Optional<String?> {
    get {
      return graphQLMap["firstName"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "firstName")
    }
  }

  public var lastName: Swift.Optional<String?> {
    get {
      return graphQLMap["lastName"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lastName")
    }
  }

  public var name: Swift.Optional<String?> {
    get {
      return graphQLMap["name"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var company: Swift.Optional<String?> {
    get {
      return graphQLMap["company"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "company")
    }
  }

  public var address1: Swift.Optional<String?> {
    get {
      return graphQLMap["address1"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "address1")
    }
  }

  public var address2: Swift.Optional<String?> {
    get {
      return graphQLMap["address2"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "address2")
    }
  }

  public var city: Swift.Optional<String?> {
    get {
      return graphQLMap["city"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "city")
    }
  }

  public var postcode: Swift.Optional<String?> {
    get {
      return graphQLMap["postcode"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "postcode")
    }
  }

  public var state: Swift.Optional<String?> {
    get {
      return graphQLMap["state"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "state")
    }
  }

  public var country: Swift.Optional<String?> {
    get {
      return graphQLMap["country"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "country")
    }
  }

  public var phone: Swift.Optional<String?> {
    get {
      return graphQLMap["phone"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "phone")
    }
  }

  public var phoneExtension: Swift.Optional<String?> {
    get {
      return graphQLMap["phoneExtension"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "phoneExtension")
    }
  }

  public var fax: Swift.Optional<String?> {
    get {
      return graphQLMap["fax"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "fax")
    }
  }

  public var startMonth: Swift.Optional<String?> {
    get {
      return graphQLMap["startMonth"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "startMonth")
    }
  }

  public var startYear: Swift.Optional<String?> {
    get {
      return graphQLMap["startYear"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "startYear")
    }
  }

  public var tracks: Swift.Optional<String?> {
    get {
      return graphQLMap["tracks"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "tracks")
    }
  }

  public var issueNumber: Swift.Optional<String?> {
    get {
      return graphQLMap["issueNumber"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "issueNumber")
    }
  }

  public var billingTitle: Swift.Optional<String?> {
    get {
      return graphQLMap["billingTitle"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingTitle")
    }
  }

  public var billingName: Swift.Optional<String?> {
    get {
      return graphQLMap["billingName"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingName")
    }
  }

  public var billingFirstName: Swift.Optional<String?> {
    get {
      return graphQLMap["billingFirstName"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingFirstName")
    }
  }

  public var billingLastName: Swift.Optional<String?> {
    get {
      return graphQLMap["billingLastName"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingLastName")
    }
  }

  public var billingCompany: Swift.Optional<String?> {
    get {
      return graphQLMap["billingCompany"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingCompany")
    }
  }

  public var billingAddress1: Swift.Optional<String?> {
    get {
      return graphQLMap["billingAddress1"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingAddress1")
    }
  }

  public var billingAddress2: Swift.Optional<String?> {
    get {
      return graphQLMap["billingAddress2"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingAddress2")
    }
  }

  public var billingCity: Swift.Optional<String?> {
    get {
      return graphQLMap["billingCity"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingCity")
    }
  }

  public var billingPostcode: Swift.Optional<String?> {
    get {
      return graphQLMap["billingPostcode"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingPostcode")
    }
  }

  public var billingState: Swift.Optional<String?> {
    get {
      return graphQLMap["billingState"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingState")
    }
  }

  public var billingCountry: Swift.Optional<String?> {
    get {
      return graphQLMap["billingCountry"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingCountry")
    }
  }

  public var billingPhone: Swift.Optional<String?> {
    get {
      return graphQLMap["billingPhone"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingPhone")
    }
  }

  public var billingFax: Swift.Optional<String?> {
    get {
      return graphQLMap["billingFax"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billingFax")
    }
  }

  public var shippingTitle: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingTitle"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingTitle")
    }
  }

  public var shippingName: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingName"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingName")
    }
  }

  public var shippingFirstName: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingFirstName"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingFirstName")
    }
  }

  public var shippingLastName: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingLastName"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingLastName")
    }
  }

  public var shippingCompany: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingCompany"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingCompany")
    }
  }

  public var shippingAddress1: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingAddress1"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingAddress1")
    }
  }

  public var shippingAddress2: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingAddress2"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingAddress2")
    }
  }

  public var shippingCity: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingCity"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingCity")
    }
  }

  public var shippingPostcode: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingPostcode"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingPostcode")
    }
  }

  public var shippingState: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingState"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingState")
    }
  }

  public var shippingCountry: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingCountry"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingCountry")
    }
  }

  public var shippingPhone: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingPhone"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingPhone")
    }
  }

  public var shippingFax: Swift.Optional<String?> {
    get {
      return graphQLMap["shippingFax"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "shippingFax")
    }
  }

  public var email: Swift.Optional<String?> {
    get {
      return graphQLMap["email"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "email")
    }
  }

  public var birthday: Swift.Optional<String?> {
    get {
      return graphQLMap["birthday"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "birthday")
    }
  }

  public var gender: Swift.Optional<String?> {
    get {
      return graphQLMap["gender"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gender")
    }
  }
}

public struct CcppExpiry: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(month: Swift.Optional<Int?> = nil, year: Swift.Optional<Int?> = nil) {
    graphQLMap = ["month": month, "Year": year]
  }

  public var month: Swift.Optional<Int?> {
    get {
      return graphQLMap["month"] as? Swift.Optional<Int?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "month")
    }
  }

  public var year: Swift.Optional<Int?> {
    get {
      return graphQLMap["Year"] as? Swift.Optional<Int?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "Year")
    }
  }
}

public struct CcppAddress: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(billing: Swift.Optional<CcppAddressBilling?> = nil) {
    graphQLMap = ["billing": billing]
  }

  public var billing: Swift.Optional<CcppAddressBilling?> {
    get {
      return graphQLMap["billing"] as? Swift.Optional<CcppAddressBilling?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "billing")
    }
  }
}

public struct CcppAddressBilling: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(address1: Swift.Optional<String?> = nil, address2: Swift.Optional<String?> = nil, address3: Swift.Optional<String?> = nil, postalCode: Swift.Optional<String?> = nil, city: Swift.Optional<String?> = nil, state: Swift.Optional<String?> = nil, countryCode: Swift.Optional<String?> = nil) {
    graphQLMap = ["address1": address1, "address2": address2, "address3": address3, "postalCode": postalCode, "city": city, "state": state, "countryCode": countryCode]
  }

  public var address1: Swift.Optional<String?> {
    get {
      return graphQLMap["address1"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "address1")
    }
  }

  public var address2: Swift.Optional<String?> {
    get {
      return graphQLMap["address2"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "address2")
    }
  }

  public var address3: Swift.Optional<String?> {
    get {
      return graphQLMap["address3"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "address3")
    }
  }

  public var postalCode: Swift.Optional<String?> {
    get {
      return graphQLMap["postalCode"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "postalCode")
    }
  }

  public var city: Swift.Optional<String?> {
    get {
      return graphQLMap["city"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "city")
    }
  }

  public var state: Swift.Optional<String?> {
    get {
      return graphQLMap["state"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "state")
    }
  }

  public var countryCode: Swift.Optional<String?> {
    get {
      return graphQLMap["countryCode"] as? Swift.Optional<String?> ?? .none
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "countryCode")
    }
  }
}

public final class PayGatewayMutationMutation: GraphQLMutation {
  /// mutation payGatewayMutation($input: ominipayGatewayInput!) {
  ///   createPayment(input: $input) {
  ///     __typename
  ///     status
  ///     message
  ///     qrCode
  ///     redirectUrl
  ///   }
  /// }
  public let operationDefinition =
    "mutation payGatewayMutation($input: ominipayGatewayInput!) { createPayment(input: $input) { __typename status message qrCode redirectUrl } }"

  public let operationName = "payGatewayMutation"

  public var input: ominipayGatewayInput

  public init(input: ominipayGatewayInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createPayment", arguments: ["input": GraphQLVariable("input")], type: .object(CreatePayment.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createPayment: CreatePayment? = nil) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createPayment": createPayment.flatMap { (value: CreatePayment) -> ResultMap in value.resultMap }])
    }

    public var createPayment: CreatePayment? {
      get {
        return (resultMap["createPayment"] as? ResultMap).flatMap { CreatePayment(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "createPayment")
      }
    }

    public struct CreatePayment: GraphQLSelectionSet {
      public static let possibleTypes = ["OminipayGateway"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("status", type: .nonNull(.scalar(Bool.self))),
        GraphQLField("message", type: .scalar(String.self)),
        GraphQLField("qrCode", type: .scalar(String.self)),
        GraphQLField("redirectUrl", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(status: Bool, message: String? = nil, qrCode: String? = nil, redirectUrl: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "OminipayGateway", "status": status, "message": message, "qrCode": qrCode, "redirectUrl": redirectUrl])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var status: Bool {
        get {
          return resultMap["status"]! as! Bool
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var message: String? {
        get {
          return resultMap["message"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "message")
        }
      }

      public var qrCode: String? {
        get {
          return resultMap["qrCode"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "qrCode")
        }
      }

      public var redirectUrl: String? {
        get {
          return resultMap["redirectUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "redirectUrl")
        }
      }
    }
  }
}
