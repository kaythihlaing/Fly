//
//  ApolloManager.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 8/21/19.
//  Copyright © 2019 Flymya.com. All rights reserved.
//

import Foundation
import Apollo

class ApolloManager {
    
    static let shared = ApolloManager()
    
    let client: ApolloClient
    
    init() {
        client = ApolloClient(url: URL(string: AppConstants.GraphQLUrl)!)
    }
    
    func samplePayment() {
        let meta = formDataInput(productType: "domestic", transactionId: "12345")
        let input = ominipayGatewayInput(gateway: "stripe", amount: "0", currency: "USD", token: "tk123456", metadata: meta, description: "hello testing")
        ApolloManager.shared.client.perform(mutation: PayGatewayMutationMutation(input: input)) { result in
            print("GraphQL =>")
            print(result)
        }
    }
}
