//
//  NetworkManager.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/21/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import SwiftyJSON

class NetworkManager {
    
    private let baseURL: String = AppConstants.BaseURL
    
    //MARK: - Singleton
    private static var sharedNetworkManager: NetworkManager = {
        return NetworkManager()
    }()
    
    private init() {}
    
    class func shared() -> NetworkManager {
        return sharedNetworkManager
    }
    
    //MARK: - Cancel all previous requests
    public func cancelAllRequest() {
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            tasks.forEach({$0.cancel()})
        }
    }
    
    //MARK: - Network Request
    public func getDataArray<T: Mappable>(route: String,
                        headers : HTTPHeaders,
                        parameters : Parameters,
                        type: T.Type,
                        success : @escaping ([T]) -> Void,
                        failure : @escaping (String) -> Void) {
        
        Alamofire.request(baseURL + route, method: .get, parameters: parameters, headers: headers).responseJSON { (response) in
            
            print("=> request url '\(String(describing: response.request?.url))'")
            switch response.result {
                case .success:
                    guard let result = response.result.value else {
                        failure("Response Error")
                        return
                    }
                    
                    let resultArray : [T] = Mapper<T>().mapArray(JSONObject: result) ?? []
                    
                    success(resultArray)
                
                case .failure(let error):
                    failure(error.localizedDescription)
            }
        }
    }
    
    public func getDataObject<T: Mappable>(route: String,
                        headers : HTTPHeaders,
                        parameters : Parameters,
                        type: T.Type,
                        success : @escaping (T) -> Void,
                        failure : @escaping (String) -> Void) {
        
        Alamofire.request(baseURL + route, method: .get, parameters: parameters, headers: headers).responseObject { (response: DataResponse<T>) in
            
            print("=> request url '\(String(describing: response.request?.url))'")
            switch response.result {
            case .success:
                guard let result = response.result.value else {
                    failure("Response Error")
                    return
                }
                success(result)
                
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
    
    public func postDataObject<T: Mappable>(route: String,
                                           headers : HTTPHeaders,
                                           parameters : Parameters,
                                           type: T.Type,
                                           success : @escaping (T) -> Void,
                                           failure : @escaping (String) -> Void) {
        
        print("=> request url '\(baseURL + route)'")
        Alamofire.request(baseURL + route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<T>) in

            print("=> response  '\(String(describing: response))'")
            switch response.result {
            case .success:
                guard let result = response.result.value else {
                    failure("\(response)")
                    return
                }
                success(result)

            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
    
    public func postDataJSON(route: String,
                                            headers : HTTPHeaders,
                                            parameters : Parameters,
                                            success : @escaping (JSON) -> Void,
                                            failure : @escaping (String) -> Void) {
        
        print("=> request url '\(baseURL + route)'")
        Alamofire.request(baseURL + route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            print("=> response  '\(String(describing: response))'")
            switch response.result {
            case .success:
                let api = response.result.value
                let json = JSON(api!)
                if json["Status"].bool ?? false {
                    success(json)
                } else {
                    failure("Response Error!")
                }
                
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
    
    public func postQueryString(route: String,
                             headers : HTTPHeaders,
                             parameters : Parameters,
                             success : @escaping (JSON) -> Void,
                             failure : @escaping (String) -> Void) {
        
        print("=> request url '\(baseURL + route)'")
        Alamofire.request(baseURL + route, method: .post, parameters: parameters, encoding: URLEncoding.queryString, headers: headers).responseJSON { (response) in
            
            print("=> response  '\(String(describing: response))'")
            switch response.result {
            case .success:
                let api = response.result.value
                let json = JSON(api!)
                success(json)
                
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
    
}
