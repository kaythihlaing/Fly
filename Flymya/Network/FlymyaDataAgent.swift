//
//  FlymyaDataAgent.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/21/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import SwiftyJSON

class FlymyaDataAgent {
    
    var parameters: Parameters!
    
    //MARK: - Singleton
    private static var sharedNetworkManager: FlymyaDataAgent = {
        return FlymyaDataAgent()
    }()
    
    class func shared() -> FlymyaDataAgent {
        return sharedNetworkManager
    }
    
//    func getAirportList(success : @escaping ([CityResponse]) -> Void, failure : @escaping (String) -> Void) {
//        
//        let headers: HTTPHeaders = [:]
//        
//        let parameters: [String : Any] = [:]
//        
//        NetworkManager.shared().getDataArray(route: "mobile-api/v1/city-list", headers: headers, parameters: parameters, type: CityResponse.self, success: { (response) in
//            success(response)
//        }) { (error) in
//            failure(error)
//        }
//    }
    
    func getFlightMNAList(parameters: Parameters, success : @escaping ([Flights]) -> Void, failure : @escaping (String) -> Void) {
        
        let headers: HTTPHeaders = [:]
        
        NetworkManager.shared().getDataObject(route: "search-flight/mna", headers: headers, parameters: parameters, type: FlightListResponse.self, success: { (response) in
            
            guard let responeData = response.flights else {
                failure("no data")
                return
            }
            
            let result: [Flights] = responeData
            success(result)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func getFlightKBZList(parameters: Parameters, success : @escaping ([Flights]) -> Void, failure : @escaping (String) -> Void) {
        
        let headers: HTTPHeaders = [:]
        
        NetworkManager.shared().getDataObject(route: "search-flight/airkbz", headers: headers, parameters: parameters, type: FlightListResponse.self, success: { (response) in
            
            guard let responeData = response.flights else {
                failure("no data")
                return
            }
            
            let result: [Flights] = responeData
            success(result)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func getFlightYGNList(parameters: Parameters, success : @escaping ([Flights]) -> Void, failure : @escaping (String) -> Void) {
        
        let headers: HTTPHeaders = [:]
        
        NetworkManager.shared().getDataObject(route: "search-flight/yangon_airways", headers: headers, parameters: parameters, type: FlightListResponse.self, success: { (response) in
            
            guard let responeData = response.flights else {
                failure("no data")
                return
            }
            
            let result: [Flights] = responeData
            success(result)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func getFlightGMAList(parameters: Parameters, success : @escaping ([Flights]) -> Void, failure : @escaping (String) -> Void) {
        
        let headers: HTTPHeaders = [:]
        
        NetworkManager.shared().getDataObject(route: "search-flight/airmypgma", headers: headers, parameters: parameters, type: FlightListResponse.self, success: { (response) in
            
            guard let responeData = response.flights else {
                failure("no data")
                return
            }
            
            let result: [Flights] = responeData
            success(result)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func postFlightBooking(parameters: Parameters, success : @escaping (FlightBookingData) -> Void, failure : @escaping (String) -> Void) {
        
        let headers: HTTPHeaders = [:]
        
        NetworkManager.shared().postDataObject(route: "mobile-api/v1/flight-booking", headers: headers, parameters: parameters, type: FlightBookingResponse.self, success: { (response) in

            if response.status ?? false {
                if let data = response.departureBooking {
                    success(data)
                } else {
                    failure("\(response)")
                }
            } else {
                failure("\(response)")
            }

        }) { (error) in
            failure(error)
        }
    }
    
    func getCountryList(success : @escaping ([CountryResponse]) -> Void, failure : @escaping (String) -> Void) {
        
        let headers: HTTPHeaders = [:]
        
        let parameters: [String : Any] = [:]
        
        NetworkManager.shared().getDataArray(route: "mobile-api/v1/country-list", headers: headers, parameters: parameters, type: CountryResponse.self, success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
    
    func domesticFlightCheck(requestParam: DomesticFlightCheckerRequest, success : @escaping (String) -> Void, failure : @escaping (String) -> Void) {
        
        let headers: HTTPHeaders = [:]
        
        let parameters: [String : Any] = [
            "Adults" : requestParam.adults,
            "ArrivalCityCode" : requestParam.arrivalCityCode,
            "ArrivalTime" : requestParam.arrivalTime,
            "Children" : requestParam.children,
            "DepartureCityCode" : requestParam.departureCityCode,
            "DepartureDate" : requestParam.departureDate.apiDate,
            "DepartureTime" : requestParam.departureTime,
            "FareId" : requestParam.fareId,
            "FlightName" : requestParam.flightName,
            "Infants" : requestParam.infants,
            "Nationality" : requestParam.nationality,
            "airline" : requestParam.airline,
            "class" : requestParam.fareClass,
            "currency" : requestParam.currency,
            "total_price" : requestParam.totalPrice
        ]
        
        NetworkManager.shared().postQueryString(route: "Myanmar-domestic-flight-check", headers: headers, parameters: parameters, success: { (response) in
            
            let status = response["success"].bool
            
            guard let result = status else { return failure("Server Error.") }
            
            if result {
                success("Your flight is ready to book.")
            } else {
                failure("This flight is not available.")
            }
        }) { (error) in
            failure(error)
        }
    }
    
}
