//
//  AppConstants.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/10/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation

struct AppConstants {
    
    static let BaseURL = Bundle.main.infoDictionary?["BASEURL"] as! String
    static let GraphQLUrl = Bundle.main.infoDictionary?["GRAPHQLURL"] as! String
    static let StripeKey = Bundle.main.infoDictionary?["STRIPEKEY"] as! String
    
    enum StoryboardID {
        static let MAIN = "Main"
        static let SearchVC = "FlightSearchViewController"
        static let DestinationSearchVC = "DestinationSearchViewController"
        static let CalendarVC = "CalendarViewController"
        static let FlightListVC = "FlightListViewController"
        static let EditSearchFlightVC = "EditFlightSearchViewController"
        static let EditPassFlightVC = "EditCustomerViewController"
        static let FlightDetailsVC = "FlightDetailsTabViewController"
        static let PayementVC = "PaymentViewController"
        static let PersonalInfoVC = "CustomerViewController"
        static let ConfirmVC = "FlightConfirmationViewController"
    }
    
    enum Font {
        enum Poppins {
            static let Regular = "Poppins-Regular"
            static let Bold = "Poppins-Bold"
            static let ExtraBold = "Poppins-ExtraBold"
            static let SemiBold = "Poppins-SemiBold"
            static let Medium = "Poppins-Medium"
        }
        enum AvenirNext {
            static let Regular = "AvenirNext-Regular"
            static let Medium = "AvenirNext-Medium"
            static let DemiBold = "AvenirNext-DemiBold"
            static let Bold = "AvenirNext-Bold"
        }
        enum Roboto {
            static let Regular = "Roboto-Regular"
            static let Bold = "Roboto-Bold"
            static let BoldCondensed = "Roboto-BoldCondensed"
            static let Medium = "Roboto-Medium"
            static let Black = "Roboto-Black"
        }
    }
    
    enum Color {
        static let btnColor = "#FF8F00"
        static let secondaryBlue = "#00ACF6"
    }
    
}
