//
//  EmptyViewHelper.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 7/2/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import UIKit

class EmptyViewHelper {
    
    class func TableViewEmptyMessage(message: String, viewController: UIViewController, tableview: UITableView) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: viewController.view.bounds.size.width, height: viewController.view.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        //messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        tableview.backgroundView = messageLabel;
        tableview.separatorStyle = .none;
    }
    
    class func CollectionViewEmptyMessage(message: String, viewController: UIViewController, collectionview: UICollectionView) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: viewController.view.bounds.size.width, height: viewController.view.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        //messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        collectionview.backgroundView = messageLabel;
    }
}

