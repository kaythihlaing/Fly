//
//  UIViewController+Extension.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/24/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

extension UIViewController {
    
    public var safeAreaWidth : CGFloat {
        if #available(iOS 11.0, *) {
            return view.safeAreaLayoutGuide.layoutFrame.size.width
        } else {
            return 0
        }
    }
    
    @available(iOS 11.0, *)
    public var safeAreaHeight : CGFloat {
        return view.safeAreaLayoutGuide.layoutFrame.size.height
    }
    
    public var statusBarHeight : CGFloat {
        return UIApplication.shared.statusBarFrame.height
    }
    
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    @available(iOS 11.0, *)
    public var topSafeArea: CGFloat {
        guard let window = UIApplication.shared.keyWindow else { return 0 }
        let topPadding = window.safeAreaInsets.top
        return topPadding
    }
    
    @available(iOS 11.0, *)
    public var bottomSafeArea: CGFloat {
        guard let window = UIApplication.shared.keyWindow else { return 0 }
        let bottomPadding = window.safeAreaInsets.bottom
        return bottomPadding
    }
    
    public func showLoadingIndicator(message: String = "Loading...") {
        let progressView = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressView.mode = MBProgressHUDMode.indeterminate
        progressView.label.text = message
    }
    
    public func hideLoadingIndicator() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    public func showAlertDialog(inputTitle: String, inputMessage: String, alertActionTitle: String = "OK!") {
        let alert = UIAlertController(title: inputTitle, message: inputMessage, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: alertActionTitle, style: .default)
        {
            (UIAlertAction) -> Void in
        }
        alert.addAction(alertAction)
        present(alert, animated: true)
        {
            () -> Void in
        }
    }
    
    public func showToast(message : String = "Success") {
        let hub = MBProgressHUD.showAdded(to: self.view, animated: true)
        hub.mode = MBProgressHUDMode.text
        hub.label.text = message
        hub.margin = 10.0
        hub.offset.y = 0.0
        hub.removeFromSuperViewOnHide = true
        hub.hide(animated: true, afterDelay: 3)
    }
    
    public func dismissKeyboardOnTapView() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
}
