//
//  UIColor+Extension.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/17/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    public class var primaryBlue: UIColor {
        return UIColor(named: "Primary-Blue-Color") ?? #colorLiteral(red: 0, green: 0.6823529412, blue: 0.937254902, alpha: 1)
    }
    
    public class var primaryOrange: UIColor {
        return UIColor(named: "Primary-Orange-Color") ?? #colorLiteral(red: 0.968627451, green: 0.568627451, blue: 0.1176470588, alpha: 1)
    }
    
    public class var textColorGray: UIColor {
        return UIColor(named: "Text-Color-Gray")!
    }

    public class var primaryGreen: UIColor {
        return UIColor(named: "Primary-Green-Color") ?? #colorLiteral(red: 0, green: 0.6509803922, blue: 0.3176470588, alpha: 1)
    }
    
    public class var textBlack: UIColor {
        return UIColor(named: "Text-Black-Color") ?? #colorLiteral(red: 0.137254902, green: 0.1215686275, blue: 0.1254901961, alpha: 1)
    }
    
}
