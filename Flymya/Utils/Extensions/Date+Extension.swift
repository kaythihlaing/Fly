//
//  Date+Extension.swift
//  Flymya
//
//  Created by Kay Kay on 7/2/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit

extension Date {
    static let formatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EE, dd MMM yyyy"
        return formatter
    }()
    
    var showDate: String {
        return Date.formatter1.string(from: self)
    }
    
    var passportExpire: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/ MM/ yyyy"
        return formatter.string(from: self)
    }
    
    static let formatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MMM/yyyy"
        return formatter
    }()
    var apiDate: String {
        return Date.formatter2.string(from: self)
    }
    
    static let formatter3: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter
    }()
    var day: String {
        return Date.formatter3.string(from: self)
    }
    
    static let formatter4: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter
    }()
    var simpleDate: String {
        return Date.formatter4.string(from: self)
    }
}


extension String {
    static let formatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    static let formatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MMM/yyyy"
        return formatter
    }()
    
    
    var apiDate: String {
        let date = String.formatter1.date(from: self)
        return Date.formatter2.string(from: date!)
    }
    
}
