//
//  FarePackageViewPod.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/20/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents

class FarePackageViewPod: UIView {
    
    lazy var containerView : UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.borderWidth = 0.5
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy private var lblFareTitle: UILabel = {
        let lbl = UILabel()
        lbl.text = "SAVER"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 17)
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    lazy private var lblFareDescription: UILabel = {
        let lbl = UILabel()
        lbl.text = "50% Refund Policy"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 15)
        lbl.textColor = UIColor.gray
        lbl.isHidden = true
        return lbl
    }()
    
    lazy private var lblFarePrice: UILabel = {
        let lbl = UILabel()
        lbl.text = "+ MMK 10,000"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        lbl.textColor = UIColor.primaryOrange
        return lbl
    }()
    
    lazy private var btnChoosePackage: UIButton = {
        let btn = UIButton()
        btn.setTitle("Choose", for: .normal)
        btn.backgroundColor = UIColor.white
        btn.setTitleColor(UIColor.primaryBlue, for: .normal)
        btn.titleLabel?.font = UIFont(name: AppConstants.Font.Roboto.Medium, size: 16)
        btn.layer.cornerRadius = 20
        btn.layer.masksToBounds = false
        btn.layer.borderWidth = 2.0
        btn.layer.borderColor = UIColor.primaryBlue.cgColor
        btn.addTarget(self, action: #selector(didTapBtnChoosePackage), for: .touchUpInside)
        return btn
    }()
    
    var handlerToCell: () -> (Void) = {}
    
    var fare: Fare? {
        didSet {
            if let fare = fare {
                lblFareTitle.text = fare.fareClass
                lblFareDescription.text = fare.mainClass
                lblFarePrice.text = "\(fare.currency ?? "") \(fare.totalPrice ?? "")"
            }
        }
    }
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.addSubview(containerView)
        
        containerView.addSubview(lblFareTitle)
        containerView.addSubview(lblFareDescription)
        containerView.addSubview(lblFarePrice)
        containerView.addSubview(btnChoosePackage)
    }
    
    override func layoutSubviews() {
        containerView.frame = self.frame
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        lblFareTitle.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
        lblFareDescription.snp.makeConstraints { (make) in
            make.top.equalTo(lblFareTitle.snp.bottom).offset(8)
            make.left.right.equalTo(lblFareTitle)
        }
        
        lblFarePrice.snp.makeConstraints { (make) in
//            make.bottom.equalTo(btnChoosePackage.snp.top).offset(-8)
            make.top.equalTo(lblFareTitle.snp.bottom).offset(8)
            make.left.right.equalTo(lblFareTitle)
        }
        
        btnChoosePackage.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-16)
            make.left.right.equalTo(lblFareTitle)
            make.height.equalTo(40)
        }
    }
    
    @objc private func didTapBtnChoosePackage() {
        handlerToCell()
    }
}
