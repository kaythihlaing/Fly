//
//  FlightDetailsExpandableViewPod.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/28/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit

class FlightDetailsExpandableViewPod: UIView {

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var flightDetailsBgView: UIView!
    
    @IBOutlet weak var destinationView: UIView!
    
    @IBOutlet weak var flightInfoView: UIView!
    
    @IBOutlet weak var feeBreakdownView: UIView!
    
    @IBOutlet weak var totalPriceView: UIView!
    
    @IBOutlet weak var ivUpDownArrow: UIImageView!
    
    @IBOutlet weak var feeBreakdownViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblDepCity: UILabel!
    @IBOutlet weak var lblRetCity: UILabel!
    @IBOutlet weak var lblDepDate: UILabel!
    @IBOutlet weak var lblFlightName: UILabel!
    @IBOutlet weak var lblFareName: UILabel!
    @IBOutlet weak var lblDepTime: UILabel!
    @IBOutlet weak var lblRetTime: UILabel!
    @IBOutlet weak var lblTimeTaken: UILabel!
    @IBOutlet weak var lblDepIATA: UILabel!
    @IBOutlet weak var lblRetIATA: UILabel!
    @IBOutlet weak var lblWayType: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    private var isExpanded = false
    
    var expandableHandler: (_ isExpanded: Bool) -> () = {_ in }
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("FlightDetailsExpandableViewPod", owner: self, options: nil)
        
        setupUI()
    }
    
    private func setupUI() {
        self.addSubview(containerView)
        
        flightDetailsBgView.layer.cornerRadius = 5
        flightDetailsBgView.layer.borderColor = UIColor().HexToColor(hexString: "#C7C7C7").cgColor
        flightDetailsBgView.layer.borderWidth = 0.5
        
        destinationView.layer.borderColor = UIColor().HexToColor(hexString: "#C7C7C7").cgColor
        destinationView.layer.borderWidth = 0.5
        
        flightInfoView.layer.borderColor = UIColor().HexToColor(hexString: "#C7C7C7").cgColor
        flightInfoView.layer.borderWidth = 0.5
        
        feeBreakdownView.layer.borderColor = UIColor().HexToColor(hexString: "#C7C7C7").cgColor
        feeBreakdownView.layer.borderWidth = 0.5
        
        totalPriceView.layer.borderColor = UIColor().HexToColor(hexString: "#C7C7C7").cgColor
        totalPriceView.layer.borderWidth = 0.5
        
        //temporary hide fare price
//        let tap = UITapGestureRecognizer(target: self, action: #selector(expandView))
//        totalPriceView.addGestureRecognizer(tap)
        
        initializeData()
    }
    
    func initializeData() {
        let flight = FlightModel.shared().chooseFlight
        lblDepCity.text = FlightModel.shared().flightReq.depCity?.cityName
        lblRetCity.text = FlightModel.shared().flightReq.retCity?.cityName
        lblDepDate.text = "Departure . \(FlightModel.shared().flightReq.depDate!.day), \(FlightModel.shared().flightReq.depDate!.simpleDate)"
        lblFlightName.text = flight.carrierName
        lblFareName.text = FlightModel.shared().chooseFare.mainClass
        lblDepTime.text = flight.d_Time
        lblRetTime.text = flight.a_Time
        lblTimeTaken.text = flight.duration
        lblDepIATA.text = flight.d_IATA
        lblRetIATA.text = flight.a_IATA
        //lblWayType.text = flight.
        lblPrice.text = "\(FlightModel.shared().chooseFare.currency ?? "")  \(FlightModel.shared().chooseFare.totalPrice ?? "")"
    }
    
    override func layoutSubviews() {
        
        containerView.frame = self.bounds
        containerView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
        if isExpanded {
            self.feeBreakdownViewHeight.constant = 213
            self.ivUpDownArrow.image = UIImage(named: "Updown")
        }
        else {
            self.feeBreakdownViewHeight.constant = 0
            self.ivUpDownArrow.image = UIImage(named: "Down")
        }
    }
    
    @objc private func expandView() {
        isExpanded.toggle()
        expandableHandler(isExpanded)
    }

}
