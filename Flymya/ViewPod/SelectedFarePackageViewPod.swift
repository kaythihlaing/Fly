//
//  SelectedFarePackageViewPod.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/20/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents

class SelectedFarePackageViewPod : UIView {
    
    lazy var containerView : MDCCard = {
        let view = MDCCard()
        view.layer.cornerRadius = 5
        return view
    }()
    
    lazy var gradient1 : CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.primaryBlue.cgColor, UIColor().HexToColor(hexString: "7DEDFE").cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.locations = [0.5, 1.0]
        gradient.cornerRadius = 5
        gradient.masksToBounds = true
        self.containerView.layer.insertSublayer(gradient, at: 0)
        return gradient
    }()
    
    lazy private var lblFareTitle: UILabel = {
        let lbl = UILabel()
        lbl.text = "PROMO"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 17)
        lbl.textColor = UIColor.white
        return lbl
    }()
    
    lazy private var lblFareDescription: UILabel = {
        let lbl = UILabel()
        lbl.text = "A basic fare"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 15)
        lbl.textColor = UIColor.white
        return lbl
    }()
    
    lazy private var lblFareNote: UILabel = {
        let lbl = UILabel()
        lbl.text = "Non Refundable"
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 17)
        lbl.textColor = UIColor.white
        lbl.numberOfLines = 3
        return lbl
    }()
    
    var fare: Fare? {
        didSet {
            if let fare = fare {
                lblFareTitle.text = fare.fareClass
                lblFareDescription.text = fare.mainClass
                lblFareNote.text = "\(fare.currency ?? "") \(fare.totalPrice ?? "")"
            }
        }
    }
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.addSubview(containerView)
        
        containerView.addSubview(lblFareTitle)
//        containerView.addSubview(lblFareDescription)
        containerView.addSubview(lblFareNote)
    }
    
    override func layoutSubviews() {
        containerView.frame = self.frame
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        gradient1.frame.size = containerView.frame.size
        
        lblFareTitle.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
        
//        lblFareDescription.snp.makeConstraints { (make) in
//            make.top.equalTo(lblFareTitle.snp.bottom).offset(8)
//            make.left.right.equalTo(lblFareTitle)
//        }
        
        lblFareNote.snp.makeConstraints { (make) in
            make.top.equalTo(lblFareTitle.snp.bottom).offset(8)
            make.left.right.equalTo(lblFareTitle)
        }
    }
    
    
}
