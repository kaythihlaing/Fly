//
//  PaymentListViewPod.swift
//  Flymya
//
//  Created by Kay Kay on 6/27/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit

class PaymentListViewPod: UIView {
    
    lazy var containerView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    lazy var stackView: UIStackView = {
        let sView = UIStackView()
        sView.translatesAutoresizingMaskIntoConstraints = false
        sView.axis = .vertical
        sView.alignment = .fill
        sView.distribution = .fillEqually
        sView.spacing = 0.5
        return sView
    }()
    
    lazy var lblMB: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.white
        lbl.text = "Mobile Banking"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14.5)
        return lbl
    }()
    
    lazy var lblMPU: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.white
        lbl.text = "MPU Payment"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14.5)
        return lbl
    }()
    
    lazy var lbl123: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.white
        lbl.text = "123 Myanmar Payment"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14.5)
        return lbl
    }()
    
    lazy var lblKBZ: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.white
        lbl.text = "KBZ Quick Pay"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14.5)
        return lbl
    }()
    
    lazy var lblCC: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.white
        lbl.text = "Credit Card"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14.5)
        return lbl
    }()
    
    lazy var lblCOD: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.white
        lbl.text = "Cash On Delivery"
        lbl.textAlignment = .center
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 14.5)
        return lbl
    }()
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.addSubview(containerView)
        
        containerView.addSubview(stackView)
        stackView.addArrangedSubview(lblMB)
        stackView.addArrangedSubview(lblMPU)
        stackView.addArrangedSubview(lbl123)
        stackView.addArrangedSubview(lblKBZ)
        stackView.addArrangedSubview(lblCC)
        stackView.addArrangedSubview(lblCOD)
    }
    
    override func layoutSubviews() {
        containerView.frame = self.frame
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        stackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        lblMB.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
        }
        
        lblMPU.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
        }
        
        lbl123.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
        }
        
        lblKBZ.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
        }
        
        lblCC.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
        }
        
        lblCOD.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
        }
    }
    
}
