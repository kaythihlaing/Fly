//
//  PassengerInfoEditViewPod.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 7/1/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField

class PassengerInfoEditViewPod : UIView {
    
    //MARK: - UI Components
    
    @IBOutlet var containerView: UIView!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var lblPassengerInfo: UILabel!
    
    @IBOutlet weak var tfTitle: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var tfPassportNrc: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tfNrcCountry: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tfNrcExp: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tfFirstName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tfLastName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tfDOB: SkyFloatingLabelTextField!
        
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var sameAsContactView: UIView!
    
    @IBOutlet weak var sameAsContactSwitch: UISwitch!
    
    @IBOutlet weak var nrcView: UIView!
    
    @IBOutlet weak var nrcViewHeight: NSLayoutConstraint!
    
    //MARK: - Attributes
    var btnSaveTapHandler: () -> () = {}
    
    var getContactDetailHandler: () -> (ContactDetail) = { () -> (ContactDetail) in
        return ContactDetail()
    }
    
    let titlePicker = UIPickerView()
    let nrcPrefixPicker = UIPickerView()
    let nrcTypePicker = UIPickerView()
    let dobPicker = UIDatePicker()
    let expPicker = UIDatePicker()
    
    var selectedTitle = PassengerTitle.Mr
    
    var titleList = ["Mr.", "Ms.", "Mrs.", "Miss."]
    let babyTitleList = ["Mr.", "Ms."]
    let adultTitleList = ["Mr.", "Ms.", "Mrs.", "Miss."]
    let nrcTypeList = ["(N)", "(E)", "(P)", "(A)", "(F)", "TH", "G"]
    
    let formatter = DateFormatter()
    
    var index: Int = 0
    
    var customer = CustomerViewController()
    
    var passengerDetail: FlightPassenger? {
        didSet {
            if let info = passengerDetail {
                var text = "Passenger \(info.id): "
                text += info.firstName.isEmpty ? "\(info.type)" : "\(info.firstName)"
                lblPassengerInfo.text = text
                tfDOB.text = formatter.string(from: dobPicker.date)
                
                let calendar = Calendar(identifier: .gregorian)
                
                let currentDate = Date()
                var components = DateComponents()
                components.calendar = calendar
                
                components.year = 10
                let maxDate = calendar.date(byAdding: components, to: currentDate)!
                
                components.year = 0
                let minDate = calendar.date(byAdding: components, to: currentDate)!
                
                var monthcomponents = DateComponents()
                monthcomponents.month = 6
                let current = calendar.date(byAdding: monthcomponents, to: currentDate)!
                
                expPicker.minimumDate = minDate
                expPicker.maximumDate = maxDate
                expPicker.setDate(current, animated: true)
                
                switch info.type {
                case .Adult:
                    components.year = -12
                    let maxDate = calendar.date(byAdding: components, to: currentDate)!
                    
                    components.year = -100
                    let minDate = calendar.date(byAdding: components, to: currentDate)!
                    
                    dobPicker.minimumDate = minDate
                    dobPicker.maximumDate = maxDate
                    
                case .Child:
                    components.year = -2
                    let maxDate = calendar.date(byAdding: components, to: currentDate)!
                    
                    components.year = -12
                    let minDate = calendar.date(byAdding: components, to: currentDate)!
                    
                    dobPicker.minimumDate = minDate
                    dobPicker.maximumDate = maxDate
                    
                case .Infant:
                    components.year = 0
                    let maxDate = calendar.date(byAdding: components, to: currentDate)!
                    
                    components.year = -2
                    let minDate = calendar.date(byAdding: components, to: currentDate)!
                    
                    dobPicker.minimumDate = minDate
                    dobPicker.maximumDate = maxDate
                    
                default: break
                }
            }
        }
    }
    
    var countryPro: CountryProtocol?
    
    var country = CountryResponse() {
        didSet {
            tfNrcCountry.text = country.countryName
        }
    }
    
    //MARK: -
    override init(frame : CGRect) {
        super.init(frame : frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("PassengerInfoEditViewPod", owner: self, options: nil)
        setupUI()
    }
    
    private func setupUI() {
        self.addSubview(containerView)
        
        backgroundView.layer.cornerRadius = 10
        backgroundView.layer.masksToBounds = true
        backgroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        //createPicker()
        
        tfTitle.titleFormatter = { $0 }
        tfFirstName.titleFormatter = { $0 }
        tfLastName.titleFormatter = { $0 }
        tfDOB.titleFormatter = { $0 }
        tfNrcExp.titleFormatter = { $0 }
        
        formatter.dateFormat = "dd/ MM/ yyyy"
    }
    
    override func layoutSubviews() {
        containerView.frame = self.frame
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    
    
    func hideNrcProperty() {
//        tfNrcExp.isHidden = true
//        tfNrcCountry.isHidden = true
        nrcView.isHidden = true
        nrcViewHeight.constant = 0
        viewHeight.constant = 575 - 181
        layoutIfNeeded()
    }
    
    func showNrcProperty() {
        nrcView.isHidden = false
        nrcViewHeight.constant = 181
        viewHeight.constant = 575
        layoutIfNeeded()
    }
    
    func hideNrc() {
        tfPassportNrc.isHidden = true
        viewHeight.constant = 440
        layoutIfNeeded()
    }
    
    func showAll() {
        tfPassportNrc.isHidden = false
        tfNrcExp.isHidden = false
        tfNrcCountry.isHidden = false
        viewHeight.constant = 575
        layoutIfNeeded()
        createPicker()
    }
    
    private func createPicker() {
        
        if passengerDetail?.type != PassengerType.Adult {
            titleList.removeAll()
            titleList = babyTitleList
        }
        else {
            titleList.removeAll()
            titleList = adultTitleList
        }
        
        //Title
        titlePicker.delegate = self
        tfTitle.inputView = titlePicker
        
//        //NRC Prefix Number
//        nrcPrefixPicker.delegate = self
//        tfNrcPrefix.inputView = nrcPrefixPicker
//
//        //NRC Type
//        nrcTypePicker.delegate = self
//        tfNrcType.inputView = nrcTypePicker
        
        //DOB
        dobPicker.datePickerMode = .date
        dobPicker.addTarget(self, action: #selector(didSelectDatePicker), for: .valueChanged)
        tfDOB.inputView = dobPicker
        
        //Expire
        expPicker.datePickerMode = .date
        expPicker.addTarget(self, action: #selector(didSelectExpiryPicker), for: .valueChanged)
        tfNrcExp.inputView = expPicker
        
        let countryTap = UITapGestureRecognizer(target: self, action: #selector(self.didTapBtnCountryCode))
        tfNrcCountry.isUserInteractionEnabled = true
        tfNrcCountry.addGestureRecognizer(countryTap)
    }
    
    @objc private func didTapBtnCountryCode(_ sender: Any) {
        countryPro!.chooseCountry()
    }
    
    func hideErrorMessage() {
//        tfNrcTownship.errorMessage = ""
        tfPassportNrc.errorMessage = ""
        tfFirstName.errorMessage = ""
        tfLastName.errorMessage = ""
    }
    
    private func isValidate() -> Bool {
        var isValidate = true
        
        if passengerDetail?.type != PassengerType.Infant {
            if tfPassportNrc.text!.isEmpty {
                tfPassportNrc.errorMessage = "*"
                isValidate = false
            }
        
            if FlightModel.shared().flightReq.nationality == "Foreigner" {
            
                if tfNrcCountry.text!.isEmpty {
                    tfNrcCountry.errorMessage = "*"
                    isValidate = false
                }
                
                if tfNrcExp.text!.isEmpty {
                    tfNrcExp.errorMessage = "*"
                    isValidate = false
                }
            }
        }
        
        if tfFirstName.text!.isEmpty {
            tfFirstName.errorMessage = "First Name is required"
            isValidate = false
        }
        
        if tfLastName.text!.isEmpty {
            tfLastName.errorMessage = "Last Name is required"
            isValidate = false
        }
        
        return isValidate
    }
    
    func bindData() {
        if let passenger = passengerDetail {
            tfFirstName.text = passenger.firstName
            tfLastName.text = passenger.lastName
            tfPassportNrc.text = passenger.nrc
        }
    }
    
    private func savePassengerData() {
        passengerDetail = FlightPassenger()
        passengerDetail?.title = tfTitle.text ?? "Mr."
        passengerDetail?.firstName = tfFirstName.text ?? ""
        passengerDetail?.lastName = tfLastName.text ?? ""
        
        passengerDetail?.nrc = tfPassportNrc.text ?? ""
        passengerDetail?.dob = dobPicker.date
        passengerDetail?.type = FlightModel.shared().passengerList[index].type
        if passengerDetail?.type == PassengerType.Infant {
            if let passenger = FlightModel.shared().passengerList.first {
                //TODO: Need to fix
                passengerDetail?.country = passenger.country
                passengerDetail?.nrc = passenger.nrc
            }
        } else {
            passengerDetail?.country = country
        }
        passengerDetail?.exp = expPicker.date
        
        FlightModel.shared().passengerList[index] = passengerDetail!
    }
    
    //MARK: - User Action
    @IBAction func didTapBtnSave(_ sender: Any) {
        if isValidate() {
            savePassengerData()
            btnSaveTapHandler()
        }
    }
    
    @objc private func didSelectDatePicker() {
        tfDOB.text = formatter.string(from: dobPicker.date)
    }
    
    @objc private func didSelectExpiryPicker() {
        tfNrcExp.text = formatter.string(from: expPicker.date)
    }
    
    @IBAction func didTapSwitch(_ sender: UISwitch) {
        if sender.isOn {
            let data = getContactDetailHandler()
            self.tfFirstName.text = data.firstName
            self.tfLastName.text = data.lastName
            
            self.tfFirstName.isUserInteractionEnabled = false
            self.tfFirstName.textColor = UIColor.gray
            
            self.tfLastName.isUserInteractionEnabled = false
            self.tfLastName.textColor = UIColor.gray
        } else {
            self.tfFirstName.isUserInteractionEnabled = true
            self.tfFirstName.textColor = UIColor.textBlack
            
            self.tfLastName.isUserInteractionEnabled = true
            self.tfLastName.textColor = UIColor.textBlack
        }
    }
}

extension PassengerInfoEditViewPod: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case titlePicker: return titleList.count
        case nrcPrefixPicker: return 14
        case nrcTypePicker: return nrcTypeList.count
        default: break
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case titlePicker: return titleList[row]
        case nrcPrefixPicker: return "\(row + 1)"
        case nrcTypePicker: return nrcTypeList[row]
        default: break
        }
        return ""
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case titlePicker: self.tfTitle.text = titleList[row]
//        case nrcPrefixPicker:
//            self.tfNrcPrefix.text = "\(row + 1)"
//        case nrcTypePicker:
//            self.tfNrcType.text = nrcTypeList[row]
        default: break
        }
    }
    
}


