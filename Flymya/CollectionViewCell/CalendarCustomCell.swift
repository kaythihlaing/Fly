//
//  CalendarCustomCell.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/11/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import UIKit
import FSCalendar

enum SelectionType : Int {
    case none
    case single
    case leftBorder
    case middle
    case rightBorder
}

class CalendarCustomCell: FSCalendarCell {
    
    weak var selectionLayer: CAShapeLayer!
    
    var selectionType: SelectionType = .none {
        didSet {
            setNeedsLayout()
        }
    }
    
    required init!(coder aDecoder: NSCoder!) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let selectionLayer = CAShapeLayer()
        selectionLayer.fillColor = UIColor().HexToColor(hexString: AppConstants.Color.secondaryBlue).cgColor
        selectionLayer.actions = ["hidden": NSNull()]
        self.contentView.layer.insertSublayer(selectionLayer, below: self.titleLabel!.layer)
        self.selectionLayer = selectionLayer
        
        self.shapeLayer.isHidden = true
        
        let view = UIView(frame: self.bounds)
        view.backgroundColor = UIColor.clear
        self.backgroundView = view;
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundView?.frame = self.bounds.insetBy(dx: 1, dy: 1)
        self.selectionLayer.frame = self.contentView.bounds
        
        // Title lable color & selection layer design for cell
        if selectionType == .middle {
            self.titleLabel.textColor = UIColor.black
            self.selectionLayer.fillColor = UIColor().HexToColor(hexString: AppConstants.Color.secondaryBlue, alpha: 0.15).cgColor
            self.selectionLayer.path = UIBezierPath(rect: self.selectionLayer.bounds).cgPath
        }
        else if selectionType == .leftBorder {
            self.titleLabel.textColor = UIColor.white
            self.selectionLayer.fillColor = UIColor().HexToColor(hexString: AppConstants.Color.secondaryBlue, alpha: 1.0).cgColor
            self.selectionLayer.path = UIBezierPath(roundedRect: self.selectionLayer.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: 8, height: 8)).cgPath
        }
        else if selectionType == .rightBorder {
            self.titleLabel.textColor = UIColor.white
            self.selectionLayer.fillColor = UIColor().HexToColor(hexString: AppConstants.Color.secondaryBlue, alpha: 1.0).cgColor
            self.selectionLayer.path = UIBezierPath(roundedRect: self.selectionLayer.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: 8, height: 8)).cgPath
        }
        else if selectionType == .single {
            self.titleLabel.textColor = UIColor.white
            self.selectionLayer.fillColor = UIColor().HexToColor(hexString: AppConstants.Color.secondaryBlue, alpha: 1.0).cgColor
            self.selectionLayer.path = UIBezierPath(roundedRect: self.selectionLayer.bounds, byRoundingCorners: [.topRight, .bottomRight, .topLeft, .bottomLeft], cornerRadii: CGSize(width: 8, height: 8)).cgPath
        }
        
        // [Fixed] ramdom white title issue
        if self.isPlaceholder {
            self.titleLabel.textColor = UIColor.lightGray
        } else {
            if self.selectionLayer.isHidden {
                self.titleLabel.textColor = UIColor.black
            }
        }
    }
    
    override func configureAppearance() {
        super.configureAppearance()
        // Override the build-in appearance configuration
        if self.isPlaceholder {
            self.eventIndicator.isHidden = true
            self.titleLabel.textColor = UIColor.lightGray
        }
    }
    
}
