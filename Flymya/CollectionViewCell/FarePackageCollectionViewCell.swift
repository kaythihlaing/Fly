//
//  FarePackageCollectionViewCell.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/20/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import MaterialComponents

class FarePackageCollectionViewCell: UICollectionViewCell {
    
    //MARK: - UIComponents
    lazy private var vpSelected : SelectedFarePackageViewPod = {
        let vp = SelectedFarePackageViewPod()
        vp.isHidden = true
        return vp
    }()
    
    lazy private var vp: FarePackageViewPod = {
        let vp = FarePackageViewPod()
        return vp
    }()
    
    var isSelectedPackage: Bool = false
    
    var cellIndex = IndexPath()
    
    var delegate: FarePackageProtocol?
    
    private var farePackage: Fare?
    
    private var handlerInCell: () -> (Void) = {}
    
    //MARK: - Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.addSubview(vp)
        self.addSubview(vpSelected)
        
        vpSelected.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        vp.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.handlerInCell = {
            self.delegate?.didTapBtnChoosePackage(for: self.cellIndex)
        }
        
        vp.handlerToCell = self.handlerInCell
    }
    
    func setData(for fare: Fare) {
        self.farePackage = fare
        vpSelected.fare = farePackage
        vp.fare = farePackage
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        if isSelectedPackage {
            vpSelected.isHidden = false
        } else {
            vpSelected.isHidden = true
        }
    }
    
}
