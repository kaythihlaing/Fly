//
//  FlightModel.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/21/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

enum PassengerType : String {
    case Adult = "Adult"
    case Child = "Child"
    case Infant = "Infant"
}

enum PassengerTitle : String {
    case Mr = "Mr."
    case Ms = "Ms."
    case Mrs = "Mrs."
    case Miss = "Miss."
}

enum PaymentGateWay : String {
    case stripe = "stripe"
    case paypal = "paypal"
    case wavemoney = "wavemoney"
    case kbz_pay_qrcode = "kbz-pay-qrcode"
    case kbz_quick_pay = "kbz-quick-pay"
    case kbz_pay_pwaapp = "kbz-pay-pwaapp"
    case mobile_banking = "mobile-banking"
    case myanmar123 = "2c2p123"
    case mpu = "2c2pmpu"
    case cod = "cod"
}

struct NRC {
    var prefixNumber: String = ""
    var prefixTownship: String = ""
    var nrcType: String = ""
    var nrcNumber: String = ""
    
    func getFullString() -> String {
        return "\(prefixNumber)/\(prefixTownship)\(nrcType)\(nrcNumber)"
    }
}

struct FlightPassenger {
    var id: Int = FlightModel.shared().passengerList.count + 1
    var title: String = "Mr."
    var firstName: String = ""
    var lastName: String = ""
    var nrc: String = ""//: NRC = NRC()
    var dob: Date = Date()
    var type: PassengerType = .Adult
    var exp: Date = Date()
    var country: CountryResponse = CountryResponse()
}

struct ContactDetail {
    var firstName: String = ""
    var lastName: String = ""
    var mobileNumber: String = ""
    var email: String = ""
}

typealias paymentMethod = (gatewayName: PaymentGateWay, displayName: String)

class FlightModel {
    
    var airportList : [CityResponse] = []
    var countryList : [CountryResponse] = []
    var flightList : [Flights] = []
    var passengerList : [FlightPassenger] = [] {
        didSet {
            for (index, _) in passengerList.enumerated() {
                passengerList[index].id = index + 1
            }
            print("=> passenger count - \(FlightModel.shared().passengerList.count)")
        }
    }
    var currentPassenger = 0
    
    let paymentMethodList: [paymentMethod] = [
        (PaymentGateWay.stripe, "Credit Card"),
        (PaymentGateWay.paypal, "PayPal"),
        (PaymentGateWay.kbz_quick_pay, "KBZ Quick Pay"),
        (PaymentGateWay.mobile_banking, "Mobile Banking"),
        (PaymentGateWay.myanmar123, "123 Myanmar"),
        (PaymentGateWay.mpu, "MPU"),
        (PaymentGateWay.cod, "Cash On Delivery")
    ]
    
    let local48HourPaymentList: [String: String] = [
        "mobile-banking" : "Mobile Banking Transfer",
        "2C2P-CreditCard" : "Credit Card(Visa/Master)"
    ]
    
    let localPaymentList: [String: String] = [
        "mobile-banking" : "Mobile Banking Transfer",
        "2C2P-123" : "123 Payment Myanmar",
        "2C2P-MPU" : "MPU",
        "2C2P-CreditCard" : "Credit Card(Visa/Master)",
        "KBZ-quick-pay" : "KBZ Quick Pay",
        "COD" : "Cash On Delivery"
    ]
    
    let foreignPaymentList: [String: String] = [
        "paypal" : "PayPal",
        "2C2P-CreditCard" : "Credit Card(Visa/Master)",
        "2C2P-Alipay" : "Alipay",
        "2C2P-Unionpay" : "Union Pay"
    ]
    
    let foreignMNAPaymentList: [String: String] = [
        "paypal" : "PayPal"
    ]
    
    enum PaymentMethod : String {
        case Mr = "Mr."
        case Ms = "Ms."
        case Mrs = "Mrs."
        case Miss = "Miss."
    }

    var flightReq: FlightListReq = FlightListReq()
    var chooseFlight: Flights = Flights()
    var chooseFare: Fare = Fare()
    var selectedPaymentMethod: paymentMethod?
    var flightBookingResponse: FlightBookingData?
    
    //MARK: - Singleton
    private init() {}
    
    class func shared() -> FlightModel {
        return sharedDataModel
    }
    
    private static var sharedDataModel: FlightModel = {
        let dataModel = FlightModel()
        return dataModel
    }()
    
    func getAdultPassengerCount() -> Int {
        return self.passengerList.filter { $0.type == .Adult }.count
    }
    
    func getChildPassengerCount() -> Int {
        return self.passengerList.filter { $0.type == .Child }.count
    }
    
    func getInfantPassengerCount() -> Int {
        return self.passengerList.filter { $0.type == .Infant }.count
    }
    
    func getPassengerCountText() -> String {
        let adultCount = getAdultPassengerCount()
        let childCount = getChildPassengerCount()
        let infantCount = getInfantPassengerCount()
        
        var text = "\(adultCount) Adult"
        if childCount > 0 {
            text += ", \(childCount) Child"
        }
        if infantCount > 0 {
            text += ", \(infantCount) Infant"
        }
        return text
    }
    
    //MARK: - Network Request
//    func getAirportList(success : @escaping () -> Void, failure : @escaping (String) -> Void) {
//        FlymyaDataAgent.shared().getAirportList(success: { (data) in
//            self.airportList = data
//            success()
//        }) { (error) in
//            failure(error)
//        }
//    }
    
    func getFlightMNAList(parameters: Parameters, success : @escaping () -> Void, failure : @escaping (String) -> Void) {
        FlymyaDataAgent.shared().getFlightMNAList(parameters: parameters, success: { (data) in
            self.flightList.append(contentsOf: data)
            print(self.flightList.count)
            success()
        }) { (error) in
            failure(error)
        }
    }
    
    func getFlightKBZList(parameters: Parameters, success : @escaping () -> Void, failure : @escaping (String) -> Void) {
        FlymyaDataAgent.shared().getFlightKBZList(parameters: parameters, success: { (data) in
            self.flightList.append(contentsOf: data)
            print(self.flightList.count)
            success()
        }) { (error) in
            failure(error)
        }
    }
    
    func getFlightYGNList(parameters: Parameters, success : @escaping () -> Void, failure : @escaping (String) -> Void) {
        FlymyaDataAgent.shared().getFlightYGNList(parameters: parameters, success: { (data) in
            self.flightList.append(contentsOf: data)
            print(self.flightList.count)
            success()
        }) { (error) in
            failure(error)
        }
    }
    
    func getFlightGMAList(parameters: Parameters, success : @escaping () -> Void, failure : @escaping (String) -> Void) {
        FlymyaDataAgent.shared().getFlightGMAList(parameters: parameters, success: { (data) in
            self.flightList.append(contentsOf: data)
            print(self.flightList.count)
            success()
        }) { (error) in
            failure(error)
        }
    }
    
    func postFlightBooking(parameters: Parameters, success : @escaping () -> Void, failure : @escaping (String) -> Void) {
        FlymyaDataAgent.shared().postFlightBooking(parameters: parameters, success: { (data) in
            self.flightBookingResponse = data
            success()
        }) { (error) in
            failure(error)
        }
    }
    
    func getCountryList(success : @escaping () -> Void, failure : @escaping (String) -> Void) {
        FlymyaDataAgent.shared().getCountryList(success: { (data) in
            self.countryList = data
            success()
        }) { (error) in
            failure(error)
        }
    }
    
    func domesticFlightChecker(success : @escaping (String) -> Void, failure : @escaping (String) -> Void) {
        
        var airline = ""
        if chooseFlight.carrierName == "Myanmar National Airline" {
            airline = "mna"
        }
        
        let param = DomesticFlightCheckerRequest(
            adults: getAdultPassengerCount(),
            arrivalCityCode: chooseFlight.a_IATA ?? "",
            arrivalTime: chooseFlight.a_Time ?? "",
            children: getChildPassengerCount(),
            departureCityCode: chooseFlight.d_IATA ?? "",
            departureDate: chooseFlight.d_Date ?? "",
            departureTime: chooseFlight.d_Time ?? "",
            fareId: chooseFare.fareId ?? "",
            flightName: chooseFlight.flightNo ?? "",
            infants: getInfantPassengerCount(),
            nationality: flightReq.nationality,
            airline: airline,
            fareClass: chooseFare.fareClass ?? "",
            currency: chooseFare.currency ?? "",
            totalPrice: chooseFare.totalPrice ?? "")
        
        FlymyaDataAgent.shared().domesticFlightCheck(requestParam: param, success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
}

