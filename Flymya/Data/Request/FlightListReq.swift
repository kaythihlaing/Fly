//
//  FlightListReq.swift
//  Flymya
//
//  Created by Kay Kay on 7/11/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation

class FlightListReq {
    var depDate: Date? = Date()
    var retDate: Date? = Date()
    var depCity: CityResponse? = CityResponse()
    var retCity: CityResponse? = CityResponse()
    var nationality = "Foreigner"
    var tripType = "oneway"
    
    init() {
        
    }
    
    init(depCity: CityResponse, retCity: CityResponse,depDate: Date, retDate: Date, nationality: String, tripType: String) {
        self.depDate = depDate
        self.retDate = retDate
        self.depCity = depCity
        self.retCity = retCity
        self.nationality = nationality
        self.tripType = tripType
    }
}
