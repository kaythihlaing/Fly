//
//  DomesticFlightCheckerRequest.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 8/9/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation

struct DomesticFlightCheckerRequest {
    var adults: Int = 0
    var arrivalCityCode: String = ""
    var arrivalTime: String = ""
    var children: Int = 0
    var departureCityCode: String = ""
    var departureDate: String = ""
    var departureTime: String = ""
    var fareId: String = ""
    var flightName: String = ""
    var infants: Int = 0
    var nationality: String = ""
    var airline: String = ""
    var fareClass: String = ""
    var currency: String = ""
    var totalPrice: String = ""
}
