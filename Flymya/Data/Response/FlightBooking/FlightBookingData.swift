//
//  FlightBookingData.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 7/31/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlightBookingData : Mappable {
    var status : String?
    var total : Int?
    var bookingId : Int?
    var paymentStatus : String?
    var currency : String?
    var bookingDate : String?
    var fM_BookingId : String?
    var issuedPNR_No : String?
    var subTotal : Int?
    var paymentMethod : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        status <- map["Status"]
        total <- map["Total"]
        bookingId <- map["BookingId"]
        paymentStatus <- map["PaymentStatus"]
        currency <- map["Currency"]
        bookingDate <- map["BookingDate"]
        fM_BookingId <- map["FM_BookingId"]
        issuedPNR_No <- map["IssuedPNR_No"]
        subTotal <- map["SubTotal"]
        paymentMethod <- map["PaymentMethod"]
    }
    
}
