//
//  FlightBookingResponse.swift
//  Flymya
//
//  Created by Kay Kay on 7/12/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlightBookingResponse : Mappable {
    var status : Bool?
    var total : Int?
    var departureBooking : FlightBookingData?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        status <- map["Status"]
        total <- map["Total"]
        departureBooking <- map["DepartureBooking"]
    }
    
}
