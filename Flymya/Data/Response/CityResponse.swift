//
//  CityResponse.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/21/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import ObjectMapper

//struct CityResponse : Mappable {
//    var cityId : Int?
//    var cityName : String?
//    var iATA : String?
//    
//    init() {
//
//    }
//
//    init?(map: Map) {
//
//    }
//
//    mutating func mapping(map: Map) {
//
//        cityId <- map["CityId"]
//        cityName <- map["CityName"]
//        iATA <- map["IATA"]
//    }
//
//}

struct CityResponse: Codable {
    var cityId : Int?
    var cityName : String?
    var iATA : String?
    
}
