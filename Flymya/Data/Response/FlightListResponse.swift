//
//  MNAFlightListResponse.swift
//  Flymya
//
//  Created by Kay Kay on 6/21/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlightListResponse : Mappable {
    var flights : [Flights]?
    
    init() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        flights <- map["Flights"]
    }
}

struct Flights : Mappable {
    var flightId : Int?
    var carrierCode : String?
    var carrierName : String?
    var carrierLogoPath : String?
    var flightNo : String?
    var d_IATA : String?
    var d_Airport : String?
    var d_Date : String?
    var d_Time : String?
    var a_IATA : String?
    var a_Airport : String?
    var a_Date : String?
    var a_Time : String?
    var duration : String?
    var via : String?
    var fare : [Fare]?
    
    init() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        flightId <- map["FlightId"]
        carrierCode <- map["CarrierCode"]
        carrierName <- map["CarrierName"]
        carrierLogoPath <- map["CarrierLogoPath"]
        flightNo <- map["FlightNo"]
        d_IATA <- map["D_IATA"]
        d_Airport <- map["D_Airport"]
        d_Date <- map["D_Date"]
        d_Time <- map["D_Time"]
        a_IATA <- map["A_IATA"]
        a_Airport <- map["A_Airport"]
        a_Date <- map["A_Date"]
        a_Time <- map["A_Time"]
        duration <- map["Duration"]
        via <- map["Via"]
        fare <- map["Fare"]
    }
    
}

struct Fare : Mappable {
    var fareId : String?
    var bookable : Bool?
    var totalPrice : String?
    var currency : String?
    var mainClass : String?
    var originFareClass : String?
    var fareClass : String?
    var passengerPrice : PassengerPrice?
    var ticketPolicy : String?
    var baggagePolicy : String?
    
    init() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        fareId <- map["FareId"]
        bookable <- map["Bookable"]
        totalPrice <- map["TotalPrice"]
        currency <- map["Currency"]
        mainClass <- map["MainClass"]
        originFareClass <- map["OriginFareClass"]
        fareClass <- map["FareClass"]
        passengerPrice <- map["PassengerPrice"]
        ticketPolicy <- map["TicketPolicy"]
        baggagePolicy <- map["BaggagePolicy"]
    }
    
}

struct PassengerPrice : Mappable {
    var adultPrice : String?
    var childPrice : String?
    var infantPrice : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        adultPrice <- map["AdultPrice"]
        childPrice <- map["ChildPrice"]
        infantPrice <- map["InfantPrice"]
    }
    
}
