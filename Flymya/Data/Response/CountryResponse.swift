//
//  CountryResponse.swift
//  Flymya
//
//  Created by Kay Kay on 8/1/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation
import ObjectMapper

struct CountryResponse : Mappable {
    var countryId : Int?
    var countryName : String?
    
    init() {
        
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        countryId <- map["CountryId"]
        countryName <- map["CountryName"]
    }
    
}
