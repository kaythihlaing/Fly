//
//  DestinationSearchTableViewCell.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/10/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit

class DestinationSearchResultTableViewCell: UITableViewCell {
    
    lazy private var lblDestinationName : UILabel = {
        let lbl = UILabel()
        lbl.text = "Name"
        lbl.font = UIFont(name: AppConstants.Font.AvenirNext.Medium, size: 17)
        lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return lbl
    }()
    
    lazy private var lblDestinationIata: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: AppConstants.Font.AvenirNext.Medium, size: 17)
        lbl.textColor = UIColor.lightGray
        return lbl
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.contentView.addSubview(lblDestinationName)
        self.contentView.addSubview(lblDestinationIata)
    }
    
    override func layoutSubviews() {
        lblDestinationName.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.centerY.equalToSuperview()
        }
        
        lblDestinationIata.snp.makeConstraints { (make) in
            make.left.equalTo(lblDestinationName.snp.right).offset(8)
            make.centerY.equalTo(lblDestinationName.snp.centerY)
        }
    }
    
    func setData(_ data: CityResponse) {
        self.lblDestinationName.text = data.cityName
        self.lblDestinationIata.text = data.iATA
    }
    
    func setAlreadySelected() {
        lblDestinationName.textColor = UIColor.gray
        self.isUserInteractionEnabled = false
    }
    
    func setData(_ data: CountryResponse) {
        self.lblDestinationName.text = data.countryName
    }
    
    func changeTextColorBlue() {
        self.lblDestinationName.textColor = #colorLiteral(red: 0, green: 0.7335222363, blue: 0.973000586, alpha: 1)
    }
    
    func changeTextColorBlack() {
        self.lblDestinationName.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
}
