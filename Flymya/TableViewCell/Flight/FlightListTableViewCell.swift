//
//  FlightListTableViewCell.swift
//  Flymya
//
//  Created by Kay Kay on 6/18/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit
import MaterialComponents

class FlightListTableViewCell: UITableViewCell {
    
    let bgView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor().HexToColor(hexString: "#E4E4E4").cgColor
        return view
    }()
    
    let lblFlightName: UILabel = {
        let label = UILabel()
        label.text = "Myanmar Airways Intl."
        label.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 16)
        label.textColor = UIColor().HexToColor(hexString: "#7F91A8")
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.numberOfLines = 2
        return label
    }()
    
    let lblFromTime: UILabel = {
        let label = UILabel()
        label.text = "15:00"
        label.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        label.textColor = UIColor().HexToColor(hexString: "#231F20")
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.sizeToFit()
        return label
    }()
    
    lazy var ivArrow: UIImageView = {
        let imageview = UIImageView()
        imageview.translatesAutoresizingMaskIntoConstraints = false
        imageview.image = #imageLiteral(resourceName: "right-arrow 2")
        return imageview
    }()
    
    let lblToTime: UILabel = {
        let label = UILabel()
        label.text = "18:00"
        label.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 16)
        label.textColor = UIColor().HexToColor(hexString: "#231F20")
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.sizeToFit()
        return label
    }()
    
    lazy var btnDetail: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Detail  ", for: .normal)
        button.setTitleColor(UIColor.primaryBlue, for: .normal)
        button.titleLabel?.font = UIFont(name: AppConstants.Font.Roboto.Regular, size: 16)
        button.setImage(#imageLiteral(resourceName: "Dropdown"), for: .normal)
        button.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        button.addTarget(self, action: #selector(didTapBtnDetail), for: .touchUpInside)
        return button
    }()
    
    let lblPrice: UILabel = {
        let label = UILabel()
        label.text = "MMK 500,000"
        label.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 18)
        label.textColor = UIColor.primaryGreen
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.sizeToFit()
        return label
    }()
    
    lazy var btnChoose: MDCButton = {
        let button = MDCButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Choose", for: .normal)
        button.backgroundColor = UIColor.primaryBlue
        button.setTitleFont(UIFont(name: AppConstants.Font.Roboto.Medium, size: 16), for: .normal)
        button.isUppercaseTitle = false
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(didTapBtnChoose), for: .touchUpInside)
        return button
    }()
    
    var didTapBtnDetailHandler: () -> (Void) = {}
    
    var didTapBtnChooseHandler: () -> (Void) = {}
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.contentView.addSubview(bgView)
        
        //left
        bgView.addSubview(lblFlightName)
        bgView.addSubview(lblFromTime)
        bgView.addSubview(ivArrow)
        bgView.addSubview(lblToTime)
        bgView.addSubview(btnDetail)
        
        //right
        bgView.addSubview(lblPrice)
        bgView.addSubview(btnChoose)
    }
    
    override func layoutSubviews() {
        bgView.snp.makeConstraints { (make) in
            make.leftMargin.rightMargin.equalToSuperview()
            make.height.equalTo(130)
            make.centerY.equalToSuperview()
        }
        
        lblFlightName.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview().offset(10)
            make.width.equalToSuperview().multipliedBy(0.6)
        }
        
        lblFromTime.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(10)
            make.top.equalTo(lblFlightName.snp.bottom).offset(10)
        }
        
        ivArrow.snp.makeConstraints { (make) in
            make.left.equalTo(lblFromTime.snp.right).offset(3)
            make.centerY.equalTo(lblFromTime)
        }
        
        lblToTime.snp.makeConstraints { (make) in
            make.left.equalTo(ivArrow.snp.right).offset(3)
            make.centerY.equalTo(lblFromTime)
        }
        
        btnDetail.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(8)
            make.bottom.equalToSuperview()
            make.width.equalTo(70)
            make.height.equalTo(50)
            
        }
        
        lblPrice.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-10)
            make.top.equalToSuperview().offset(10)
        }
        
        btnChoose.snp.makeConstraints { (make) in
            make.trailing.bottom.equalToSuperview().offset(-10)
            make.width.equalTo(120)
            make.height.equalTo(40)
        }
        
        
        
    }
    
    @objc private func didTapBtnDetail() {
        didTapBtnDetailHandler()
    }
    
    @objc private func didTapBtnChoose() {
        didTapBtnChooseHandler()
    }
    
}

