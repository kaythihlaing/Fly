//
//  PassengerDetailsTableViewCell.swift
//  Flymya
//
//  Created by Zin Lin Phyo on 6/27/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import UIKit

class PassengerDetailsTableViewCell: UITableViewCell {
    
    lazy private var lblPassengerInfo : UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont(name: AppConstants.Font.Roboto.Bold, size: 14)
        lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        return lbl
    }()
    
    lazy private var ivEdit: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Edit")
        return iv
    }()
    
    private var passenger: FlightPassenger? {
        didSet {
            if let info = passenger {
                var text = "Passenger \(info.id): "
                text += info.firstName.isEmpty ? "\(info.type)" : "\(info.firstName)"
                lblPassengerInfo.text = text
            }
        }
    }
    
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.contentView.addSubview(lblPassengerInfo)
        self.contentView.addSubview(ivEdit)
        
        contentView.layer.cornerRadius = 5
//        contentView.layer.borderColor = UIColor.gray.cgColor
        contentView.layer.borderWidth = 1
        contentView.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        lblPassengerInfo.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.centerY.equalToSuperview()
        }
        
        ivEdit.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-16)
            make.centerY.equalToSuperview()
        }
    }

    func setData(_ passenger: FlightPassenger) {
        self.passenger = passenger
    }
}
