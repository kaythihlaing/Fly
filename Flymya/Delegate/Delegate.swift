//
//  Delegate.swift
//  Flymya
//
//  Created by Kay Kay on 6/12/19.
//  Copyright © 2019 Kay Kay. All rights reserved.
//

import Foundation

protocol calProtocol {
    func getTravelDate(dep: Date?, ret: Date?)
}

protocol DepCityProtocol {
    func getDepCity(for city: CityResponse)
}

protocol RetCityProtocol {
    func getRetCity(for city: CityResponse)
}

protocol PassengerCountProtocol {
    func getPassengerCount(adult: Int, child: Int, infant: Int)
}

protocol EditSearchProtocol {
    func editSearch()
}

protocol DetailChooseProtocol {
    func chooseClick()
}

protocol CountryProtocol {
    func chooseCountry()//call Country select page
    func chooseCountry(country: CountryResponse)//after select country
}


